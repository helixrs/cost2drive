<?php

include("app/classes/object-trip.php");

$json = new stdClass();
$json->email = $_POST["email"];
$start = $_POST["start"];
$end = $_POST["end"];
$query_string = $_POST['queryString'];


//create trip object and store it in session


// query string format:
// pagename=from&trip_info=..

// (I)
// trip_info=start-address/to/end-address/
// (II)
// trip_info=start-address/to/end-address/passengers/carId/~whatever or nothing~
// trip_info=start-address/to/end-address/2/15035/2008-Honda-Civic
// (III)
// trip_info=start-address/to/end-address/passengers/custom/mpg/gpt/fuel_type
// trip_info=start-address/to/end-address/passengers/custom/20/17/Regular

//  to the end of url goes: /via/Stop-Point/via/Stop-Point2/...


if (strpos($query_string, 'start=') === false) {

    //~$query_vars_initial = explode('/', $query_string);
    $query_vars_initial = explode('/', $query_string);


    // waypoints - Stop Points: Read them from the array and then remove them from an array
    $get_waypoints = '';
    $query_vars_bkp = $query_vars_initial;
    if (in_array('via', $query_vars_bkp)) {
        // url contains: /via/Stop-Point/via/Stop-Point2/...
        $get_waypoints = array();
        $next = false;
        foreach ($query_vars_bkp as $var_key => $var_value) {
            if ($next) {
                $get_waypoints[] = $var_value;
                unset($query_vars_initial[$var_key]);
            }
            if ($var_value == 'via') {
                $next = true;
                unset($query_vars_initial[$var_key]);
            } else {
                $next = false;
            }
        }
    }
    $waypoints_bkp = $get_waypoints;
    if ($get_waypoints == '') {
        unset ($get_waypoints);
    }

    // reset array indexes, make final array $query_vars without Stop Points
    $query_vars = array();
    foreach ($query_vars_initial as $var_value) {
        $query_vars[] = $var_value;
    }



    $get_start = str_replace('-', '%20', $query_vars[2]);
    $get_end = str_replace('-', '%20', $query_vars[4]);
    $get_start_readable = str_replace('-', ' ', $query_vars[2]);

    $get_end_readable = str_replace('-', ' ', $query_vars[4]);

    if (count($query_vars) <= 6) {

        // default car - url format: /start-address/to/end-address/
        $get_custom = true;
        $get_mpg = 25;
        $get_car_id = -1;
        $get_fuel_type = 'Regular';
        $get_passengers = 1;
        $get_gpt = 17;

    } else {

        if (in_array('custom', $query_vars)) {

            // custom car info: /start-address/to/end-address/passengers/custom/mpg/gpt/fuel_type
            $get_custom = true;
            $get_passengers = (is_numeric($query_vars[3])) ? $query_vars[5] : 1;
            $custom_var_index = array_search('custom', $query_vars);
            $custom_var_index++;
            $get_mpg = (is_numeric($query_vars[$custom_var_index])) ? $query_vars[$custom_var_index] : 25;
            $custom_var_index++;
            $get_gpt = (is_numeric($query_vars[$custom_var_index])) ? $query_vars[$custom_var_index] : 17;
            $custom_var_index++;
            $get_fuel_type = ($query_vars[$custom_var_index] != '') ? $query_vars[$custom_var_index] : 'Regular';

        } else {

            // carId from url: /start-address/to/end-address/passengers/carId/~whatever or nothing~
            $get_custom = false;
            $get_passengers = (is_numeric($query_vars[5])) ? $query_vars[5] : 1;
            $get_car_id = (is_numeric($query_vars[6])) ? $query_vars[6] : 15395;
        }
    }

} else {

    $get_start = $_GET["start"];
    $get_end = $_GET["end"];
    $get_start_readable = $_GET["start"];
    $get_end_readable = $_GET["end"];
    $get_custom = ($_GET["custom"] == 'true') ? true : false;
    $get_mpg = $_GET["mpg"];
    $get_car_id = $_GET["carId"];
    $get_fuel_type = $_GET["fuelType"];
    $get_waypoints = $_GET["waypoints"];
    $get_passengers = $_GET["passengers"];
    $get_gpt = $_GET["gpt"];
}


//seconds * minutes * hours * days + current time
$inTwoMonths = 60 * 60 * 24 * 60 + time();
setcookie('start', $get_start_readable, $inTwoMonths, "/", ".costtodrive.com");
setcookie('end', $get_end_readable, $inTwoMonths, "/", ".costtodrive.com");
setcookie('carId', $get_car_id, $inTwoMonths, "/", ".costtodrive.com");


if ($get_custom == false && isset($get_car_id) && $get_car_id != "") {
    $carId = $get_car_id;
} else {
    if (isset($get_mpg) && $get_mpg != "") {
        $carId = -1;
    } else {
        $carId = 15395;
    }
}


$car = new Car($carId);
if ($get_custom == true && isset($get_mpg) && $get_mpg != "") {
    $car->setCustom($get_fuel_type, $get_mpg, $get_gpt);
    $carName = "Custom Car";
} else {
    $carName = $car->year . " " . $car->mfr . " " . $car->make;
}


$trip = new Trip($car, $get_start, $get_end, $get_waypoints, $get_passengers);
$trip->calculateCost();

session_start();
$_SESSION["trip"] = json_encode($trip);


$stops[0] =$trip->startPoint->lng.",".$trip->startPoint->lat;
$stops[1] =$trip->endPoint->lng.",".$trip->endPoint->lat;




$airport = getAirport($stops);




if (!empty($airport)) {
    $json->originCode = $airport[0]['code'];
    $json->destinationCode = $airport[1]['code'];
}


$json->sourceId = 63809;
$json = json_encode($json);

$ch = curl_init();

//$url = "http://awdapi-stg.airfarewatchdog.com:8000/airfarewatchdog/users/subscriptions/coreg";
//production url
$url = "http://awdapi.airfarewatchdog.com:8000/airfarewatchdog/users/subscriptions/coreg";

//$apiKey ="e9a03f2a616c4dadcd6b52d04d74a13c";
//production apikey
$apiKey ="2a3a1ca444a8407fc0a8b6c6effb2103";

$headers = array();
$headers[] = "Content-Type:application/json";
$headers[] = "apikey:" . $apiKey;


curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch,CURLOPT_POSTFIELDS, $json);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$result = curl_exec($ch);
curl_close($ch);

$response = new stdClass();
$response->message =  "Email recorded successfuly";
$response->status = 1;

$result = json_decode($result);

if (!empty($result)) {
    if (empty($result->success)) {
        if (!empty($result->errors)) {
            $response->message = $result->errors[0]->message;

        }
        else {
            $response->message ="Internal Server Error. Please try later";
        }
        $response->status = 0;
    }
}
//seconds * minutes * hours * days + current time
$inThreeMonths = 60 * 60 * 24 * 90 + time();
setcookie('awd_subscribed', "subscribed", $inThreeMonths, "/", ".costtodrive.com");

echo json_encode($response);

exit();
?>