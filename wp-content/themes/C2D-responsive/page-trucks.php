<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage cost_to_drive
 * @since Cost to Drive 1.0
 */

header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
include("app/include/globals.php");
include("app/classes/object-car.php");

get_header();

?>


<h1 class="hidden">Cost2Drive</h1>

<script type="text/javascript">
    window._tpm = window._tpm || [];
    window._tpm['paywallID'] = ' 41415690';
    window._tpm['trackPageview'] = true;


</script>
<script type="text/javascript" src="//cdn.tinypass.com/tpl/d1/tpm.js"></script>
<script type="text/javascript" src="/wp-content/themes/C2D-responsive/main.js"></script>


<div class="row">
    <div id="container" class="col-md-8">
        <div id="content" role="main">

            <div class="wide-box page">
                <form class="form-horizontal" id="where_to_f"
                      action="http://<?php print $_SERVER['SERVER_NAME']; ?>/from" method="get" role="form">
                    <?php include("app/include/truck-trip-form.php"); ?>
                    <?php include("app/include/truck-form.php"); ?>
                    <input class="my_c2d_btn" type="image"
                           src="<?php echo get_bloginfo('template_url'); ?>/images/structural/c2d-btn.gif"
                           alt="Calculate my cost to drive"/>
                    <input class="phone-btn" type="image"
                           src="<?php echo get_bloginfo('template_url'); ?>/images/structural/c2d-btn-phone.png"
                           alt="Calculate my cost to drive"/>
                </form>
                <?php include("app/include/car-script.php"); ?>
            </div><!-- /wide-box -->

        </div><!-- #content -->
    </div><!-- #container -->

    <?php get_sidebar(); ?>
</div><!-- .row -->
<?php get_footer(); ?>
<div class="front-page-lightbox">
    <div class="modal fade in"
         tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1 col-xs-10 col-xs-offset-1">
                        <h1>Flying Might Even Be Cheaper!</h1>
                        <h3>Sign up and get free low fare alerts</h3>
                    </div>
                </div>
            </div>
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10 col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 col-lg-offset-1">
                            <div class="inputs-container">
                                <input id="email" placeholder="Your Email:">
                                <div class="input-placeholder">
                                    <input id = 'start-location' type="text" required>
                                    <div class="placeholder">
                                        Your City: <span>Boston, MA (BOS)</span>
                                    </div>
                                </div>
                                <button id = "submit-email" class ="btn btn-lg text-center">Sign up now <span class ='triangle-right'></span></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
        <img class="awd-airplane" src="<?php echo get_bloginfo('template_url'); ?>/images/awd_airplane.png">
    </div>
</div>
<!-- /.modal -->

