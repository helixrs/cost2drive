<?php
/*
Template Name: Recommended Destinations
*/
error_reporting(0);

	header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
	include("app/include/globals.php");
	include("app/classes/object-car.php");

	get_header();
?>

<?php
	//$end_location = $trip->end;
	$location_title = get_the_title();
?>

		<div id="container" class="popular-destinations recommended-destinations">
			<div id="content" role="main">
				<div class="wide-box page">
					<h1 class="entry-title">Cost to drive to <?php print $location_title; ?></h1>
					<div class="top-form-wrapper">
						<?php /* <p>A form to enter your "from", enter your car and destination is pre-populated and we<br /> just calculate it</p> */ ?>
						<div>
							<form id="where_to_f"  action="http://<?php print $_SERVER['SERVER_NAME']; ?>/from" method="get">
								<?php include("app/include/trip-form-popular-dest.php");?>
								<?php include("app/include/car-form.php");?>
								<input class="my_c2d_btn" type="image" src="<?php echo get_bloginfo('template_url'); ?>/images/structural/c2d-btn.gif" alt="Calculate my cost to drive" />
							</form>
						<?php include("app/include/car-script.php"); ?>
						</div>
					</div>

					<div class="entry-content">
						<?php print $post->post_content; ?>
					</div>

					<?php /* ?>
					<div id="complete_directions">
						<p>Get complete directions from:</p>
						<ul>
							<li><a target="_blank" href="http://maps.google.com/?q=<?php print $location_title ?>">Google Maps</a></li>
						</ul>
					</div>
					<?php */ ?>


				<?php
				if (!empty($location_title)) {

					$query = sprintf("SELECT geo_id FROM geos_id_cache WHERE geo_name='%s' AND geo_id<>0 LIMIT 1",
									mysql_real_escape_string($location_title));
					$row = $wpdb->get_row($query);
					$taid_is_ok = false;

					if ($row != null) {
						$taid = $row->geo_id;
						if (is_numeric($taid) && $taid > 0) {
							$taid_is_ok = true;
						}
						//print '<!-- #from cache: '.$taid.'# -->';
					} else {
						$contents = file_get_contents('https://www.google.com/search?q=tripadvisor+hotels+'.urlencode($location_title));
						$pattern = '/http:\/\/www\.tripadvisor(.*?)-g([0-9]+)/';
						preg_match($pattern, $contents, $matches);
						$taid = $matches[2];
						if (is_numeric($taid) && $taid > 0) {
							if ($wpdb->insert('geos_id_cache', array('geo_id' => $taid, 'geo_name' => $location_title), array('%d', '%s'))) {
								$taid_is_ok = true;
								//print '<p>#write into db#</p>';
							}
						}
						//print '<!-- #live: '.$taid.'# -->';
					}
				?>

					<?php if ($taid_is_ok) : ?>
					<div class="destination-recommendations">
						<input type="hidden" name="taid-value" value="<?php print $taid; ?>" id="taid-value" />
						<h2 id="dest-recommend">New! <span>Destination Recommendations for <?php echo $location_title; ?></span></h2>
						<div class="dest-recommend-content-wrap">
							<div class="dest-recommend-content">
								<img class="spiner" src="http://<?php print $_SERVER['SERVER_NAME']; ?>/wp-content/themes/C2D/images/ajax-loader.gif" alt="loading..." title="" />
							</div>
						</div>
					</div>
					<?php endif; ?>


					<?php /*
					require_once ("popular-destinations-lat-lng.php");
					if (isset($popular_destination_titles[$location_title])) {
					?>
					<script type="text/javascript">
						var end_location_name = "<?php print addslashes($location_title); ?>";
						var end_point_lat = <?php print $popular_destination_titles[$location_title][0]; ?>;
						var end_point_lng = <?php print $popular_destination_titles[$location_title][1]; ?>;
					</script>
					<div class="weather-box">
						<!-- ajax populated -->
					</div>
					<?php
					} */
					?>

				<?php
				}
				?>

				</div><!-- /wide-box -->
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
