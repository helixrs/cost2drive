jQuery(document).ready(function($){




	var where_initial = $('#start').val();
	$('#where_to_f input[type="text"]').focusin(function(){
		if ($(this).val() == where_initial) {
			$(this).val('').addClass('populated').removeClass('error');
		}
	});
	$('#where_to_f input[type="text"]').focusout(function(){
		if ($(this).val() == '') {
			$(this).val(where_initial).removeClass('populated');
		} else {
			$(this).val(where_initial).addClass('populated');
		}
	});

	$('select').focusin(function(){
		$(this).removeClass('error');
	});


  	$('#where_to_f').submit(function(){

		// validation
		var valid = true;
		$('input, select').removeClass('error');

		var value_placeholder = 'Enter Address, City, State or Zip';
		if ($('input#start').val() == '' || $('input#start').val() == value_placeholder) {
			$('#start').addClass('error');
			valid = false;
		}
		if ($('input#end').val() == '' || $('input#end').val() == value_placeholder) {
			$('#end').addClass('error');
			valid = false;
		}
		if ($('input#carId').val() == '') {
			valid = false;
		}
		if ($('select#year_c option:first').is(':selected')) {
			$('select#year_c').addClass('error');
			valid = false;
		}
		if ($('select#make option:first').is(':selected')) {
			$('select#make').addClass('error');
			valid = false;
		}
		if ($('select#model option:first').is(':selected')) {
			$('select#model').addClass('error');
			valid = false;
		}


		if (!valid) {
			return false;
		} else {
			// make clean url
			var clean_url = '/from';
			clean_url = clean_url + '/' + $('input#start').val();
			clean_url = clean_url + '/to/' + $('input#end').val();
			//clean_url = clean_url + '/' + $('select#passengers').val();
			clean_url = clean_url + '/1';
			clean_url = clean_url + '/' + $('input#carId').val();
			clean_url = clean_url + '/' + $('select#year_c').val();
			clean_url = clean_url + '-' + $('select#make').val();
			clean_url = clean_url + '-' + $('select#model').val();

			clean_url1 = (clean_url.replace(/ /g, '-')).replace(/,/g, '');

			window.parent.location = clean_url1;
			return false;
		}
	});

});
