<?php

function get_destionation_data($taid)
{
// SENDING REQUEST
    $result = array();
    $result['restaurants'] = array();
    $result['hotels'] = array();
    $result['attractions'] = array();

    ini_set('display_errors', "1");

    $url = "http://content.tripadvisor.com/ContentService";
    $key = "BBA2AF50459045D8AE416772964CB86F";
    $url = "http://api.tripadvisor.com/api/partner/2.0/location/" . $taid . "/restaurants/" . "?limit=5&key=" . $key;

    $data = getDecodedData($url);
    $dataNext = null;
    if (!empty($data->data)) {
        while (empty($result["restaurants"]) || $dataNext != null) {
            if ($dataNext != null) {
                $data = getDecodedData($dataNext);
            }
            foreach ($data->data as $restaurant) {
                $restaurantData = array();
                $restaurantData['ratingImage'] = $restaurant->rating_image_url;
                $restaurantData['restaurantDetailUrl'] = $restaurant->web_url;
                $restaurantData['name'] = $restaurant->name;
                $restaurantData['percentRecommended'] = $restaurant->percent_recommended;
                $result['restaurants'][] = $restaurantData;
            }
            if ($dataNext != null) {
                $dataNext = $data->pagination->next;
            }
        }
    }

    $url = "http://api.tripadvisor.com/api/partner/2.0/location/" . $taid . "/hotels/" . "?limit=5&key=" . $key;
    $data = getDecodedData($url);
    $dataNext = null;
    if (!empty($data->data)) {
        while (empty($result["hotels"]) || $dataNext != null) {
            if ($dataNext != null) {
                $data = getDecodedData($dataNext);
            }
            foreach ($data->data as $hotel) {

                $hotelData = array();
                $hotelData['TAID'] = $hotel->location_id;
                $hotelData['ratingImage'] = $hotel->rating_image_url;
                $hotelData['hotelDetailUrl'] = $hotel->web_url;
                $hotelData['name'] = $hotel->name;
                $hotelData['percentRecommended'] = $hotel->percent_recommended;
                $result['hotels'][] = $hotelData;
            }
            if ($dataNext != null) {
                $dataNext = $data->pagination->next;
            }
        }
    }


    $url = "http://api.tripadvisor.com/api/partner/2.0/location/" . $taid . "/attractions/" . "?limit=5&key=" . $key;
    $data = getDecodedData($url);
    $dataNext = null;
    if (!empty($data->data)) {
        while (empty($result["attractions"]) || $dataNext != null) {
            if ($dataNext != null) {
                $data = getDecodedData($dataNext);
            }
            foreach ($data->data as $attraction) {
                $attractionData = array();
                $attractionData['ratingImage'] = $attraction->rating_image_url;
                $attractionData['attractionDetailUrl'] = $attraction->web_url;
                $attractionData['name'] = $attraction->name;
                $attractionData['percentRecommended'] = $attraction->percent_recommended;
                $result['attractions'][] = $attractionData;
            }
            if ($dataNext != null) {
                $dataNext = $data->pagination->next;
            }
        }
    }

    return empty($result["restaurants"]) && empty($result["hotels"]) && empty($result["attractions"]) ? false : $result;
}

function getDecodedData($url)
{
    $yt = curl_init();
    if (empty($header)) {
        $header = "";
    }
    $header .= "Accept: */*";
    //curl_setopt($yt, CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($yt, CURLOPT_URL, $url);
    curl_setopt($yt, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($yt, CURLOPT_FOLLOWLOCATION, true);

    curl_setopt($yt, CURLOPT_MAXREDIRS, 10);
    $jsonData = curl_exec($yt);

    $data = json_decode($jsonData);
    curl_close($yt);

    return $data;
}

// #####################################################################
//$taid = 187073;


$taid = $_POST['taid'];
$current_time = time();

$query = sprintf("SELECT time, data FROM geo_result_cache WHERE geo_id='%d' LIMIT 1",
    mysql_real_escape_string($taid));

$row = $wpdb->get_row($query);

if ($row != null) {
    // there is cached data for the geo_id
    $cached_data_time = $row->time;
    $time_diff = $current_time - $cached_data_time;

    if ($time_diff < 86400) {

        // cached data is newer than 24h
        $rxml = json_decode($row->data, true);
        if (empty($rxml)) { //if database data is empty
            $rxml = get_destionation_data($taid);
            $wpdb->update(
                'geo_result_cache',
                array(
                    'time' => $current_time,
                    'data' => json_encode($rxml)
                ),
                array('geo_id' => $taid),
                array(
                    '%d',
                    '%s'
                ),
                array('%d')
            );
        }

        print '<!-- old -->';
    } else {
        // cached data is older than 24h
        $rxml = get_destionation_data($taid);
        // update cache
        if (!$rxml) { // if there's no data retrieved from api take data from db
            $rxml = json_decode($row->data, true);
        } else {
            $wpdb->update(
                'geo_result_cache',
                array(
                    'time' => $current_time,
                    'data' => json_encode($rxml)
                ),
                array('geo_id' => $taid),
                array(
                    '%d',
                    '%s'
                ),
                array('%d')
            );
        }
        print '<!-- update -->';
    }


} else {
    // no cached data
    $rxml = get_destionation_data($taid);
    // cache data
    if (rxml) {
        $wpdb->insert(
            'geo_result_cache',
            array(
                'geo_id' => $taid,
                'time' => $current_time,
                'data' => json_encode($rxml)
            ),
            array(
                '%d',
                '%d',
                '%s'
            )
        );
    }
    print '<!-- insert -->';
}
$hotels_data = $rxml["hotels"];
$restaurants_data = $rxml["restaurants"];
$attractions_data = $rxml['attractions'];
?>
<?php // if (count($hotels_data) <= 5 && count($restaurants_data) <= 5 && count($attractions_data) <= 5) : ?>
<div class="destinations">
    <div class="destinations-tabs">
        <span class="top-rated-tabs">Top Rated:</span>
        <ul>
            <?php if (is_array($hotels_data) && count($hotels_data) <= 5) : ?>
                <li><a class="active" href="#">Hotels</a></li>
            <?php endif; ?>
            <?php if (is_array($restaurants_data) && count($restaurants_data) <= 5) : ?>
                <li><a href="#">Restaurants</a></li>
            <?php endif; ?>
            <?php if (is_array($attractions_data) && count($attractions_data) <= 5) : ?>
                <li><a href="#">Attractions</a></li>
            <?php endif; ?>
        </ul>
        <div class="powered-by">
            <span>Powered by</span>
            <img
                src="https://developer-tripadvisor.s3.amazonaws.com/uploads/.thumbnails/220x50_ta_logo_color.png/220x50_ta_logo_color-220x50.png"
                alt="tripadvisor"/>
        </div>
    </div>
    <div class="destinations-tabs-content">
        <?php if (is_array($hotels_data) && count($hotels_data) <= 5) : ?>
            <ul class="first">
                <?php foreach ($hotels_data as $hotel) : ?>
                    <li>
                        <img src="<?php print $hotel['ratingImage']; ?>" alt="rating"/>
                        <a href="<?php print $hotel['hotelDetailUrl']; ?>"
                           target="_blank"><?php print $hotel['name']; ?></a>
                        <span><iframe width="149" height="29"
                                      src="http://www.tripadvisor.com/WidgetEmbed-cdscheckrates?partnerId=DFE65FB89BBD4BC2BFFA95A695240436&display=true&locationId=<?php print $hotel['TAID']; ?>"
                                      frameborder="0"></iframe></span>
                        <?php /*<span><?php print_r($hotel['percentRecommended']); ?>% recommend</span>*/ ?>
                    </li>
                <?php endforeach; ?>
                <li class="last">
                    <a target="_blank" href="http://www.tripadvisor.com/Hotels-g<?php print $taid; ?>">See more
                        hotels</a>
                </li>
            </ul>
        <?php endif; ?>
        <?php if (is_array($restaurants_data) && count($restaurants_data) <= 5) : ?>
            <ul>
                <?php foreach ($restaurants_data as $restaurant) : ?>
                    <li>
                        <img src="<?php print $restaurant['ratingImage']; ?>" alt="rating"/>
                        <a href="<?php print $restaurant['restaurantDetailUrl']; ?>"
                           target="_blank"><?php print $restaurant['name']; ?></a>
                        <?php if (isset($restaurant["percentRecommended"])) : ?>
                            <span><?php print (is_numeric($restaurant['percentRecommended'])) ? $restaurant['percentRecommended'] . '% recommend' : ''; ?></span> <?php endif ?>
                    </li>
                <?php endforeach; ?>
                <li class="last">
                    <a target="_blank" href="http://www.tripadvisor.in/Restaurants-g<?php print $taid; ?>">See more
                        restaurants</a>
                </li>
            </ul>
        <?php endif; ?>
        <?php if (is_array($attractions_data) && count($attractions_data) <= 5) : ?>
            <ul>
                <?php foreach ($attractions_data as $attraction) : ?>
                    <li>
                        <img src="<?php print $attraction['ratingImage']; ?>" alt="rating"/>
                        <a href="<?php print $attraction['attractionDetailUrl']; ?>"
                           target="_blank"><?php print $attraction['name']; ?></a>
                        <?php if (isset($restaurant["percentRecommended"])) : ?>
                            <span><?php print (is_numeric($attraction['percentRecommended'])) ? $restaurant['percentRecommended'] . '% recommend' : ''; ?></span> <?php endif ?>
                    </li>
                <?php endforeach; ?>
                <li class="last">
                    <a target="_blank" href="http://www.tripadvisor.com/Attractions-g<?php print $taid; ?>">See more
                        attractions</a>
                </li>
            </ul>
        <?php endif; ?>
    </div>
</div>
<?php // endif; ?>

