<?php

require_once ("../WeatherBash/WeatherBug.class.php");
require_once ("../WeatherBash/WeatherBugLiveWeather.class.php");
require_once ("../WeatherBash/WeatherBugForecast.class.php");

$WeatherBug = new WeatherBug;

$points = json_decode(str_replace('\"', '"', $_POST['points']), true);

$return = array();

$i = 0;
foreach ($points as $weather_point) {

	$WeatherResponse = $WeatherBug->GetLiveWeatherByLatLong ($weather_point['lat'], $weather_point['lng']);
	
	$subject = $WeatherResponse->CurrentConditionIcon;
	$pattern = '/^(.+?)cond([0-9]+?)\.gif/';
	preg_match($pattern, $subject, $matches);
	$weather_icon = $matches[2];
	
	
	$return[$i]['lat'] = $weather_point['lat'] + 1;
	$return[$i]['lng'] = $weather_point['lng'] - 1;
	$return[$i]['temp'] = $WeatherResponse->Temperature;
	$return[$i]['desc'] = $WeatherResponse->CurrentConditionDescription;
	$return[$i]['icon'] = 'http://img.weather.weatherbug.com/forecast/icons/localized/50x42/en/trans/cond'.$weather_icon.'.png';
	
	$i++;
}

print json_encode($return);


?>
