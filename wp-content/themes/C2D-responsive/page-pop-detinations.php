<?php
/*
Template Name: Popular Destinations
*/
error_reporting(0);

	header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
	include("app/include/globals.php");
	include("app/classes/object-car.php");

	get_header();
?>

<?php
	//$end_location = $trip->end;
	$location_title = get_the_title();
?>
	<div class="row">
		<div id="container" class="popular-destinations col-md-8">
			<div id="content" role="main">
				<div class="wide-box page">
					<h1 class="entry-title">Cost to drive to <?php print $location_title; ?></h1>
					<div class="top-form-wrapper">
						<?php /* <p>A form to enter your "from", enter your car and destination is pre-populated and we<br /> just calculate it</p> */ ?>
						<div>
							<form id="where_to_f" class="form-horizontal" action="http://<?php print $_SERVER['SERVER_NAME']; ?>/from" method="get">
							<?php include("app/include/trip-form-popular-dest.php");?>
							<?php include("app/include/car-form.php");?>
							<input class="my_c2d_btn" type="image" src="<?php echo get_bloginfo('template_url'); ?>/images/structural/c2d-btn.gif" alt="Calculate my cost to drive" />
							<input class="phone-btn" type="image" src="<?php echo get_bloginfo('template_url'); ?>/images/structural/c2d-btn-phone.png" alt="Calculate my cost to drive" />
						</form>
						<?php include("app/include/car-script.php"); ?>
						</div>
					</div>

					<?php
					/* Run the loop to output the page.
						* If you want to overload this in a child theme then include a file
						* called loop-page.php and that will be used instead.
						*/
					get_template_part( 'loop', 'page' );
					?>

					<div id="complete_directions">
						<p>Get complete directions from:</p>
						<ul>
							<li><a target="_blank" href="http://maps.google.com/?q=<?php print $location_title ?>">Google Maps</a></li>
						</ul>
					</div>


				<?php
				if (!empty($location_title)) {

					$query = sprintf("SELECT geo_id FROM geos_id_cache WHERE geo_name='%s' AND geo_id<>0 LIMIT 1",
									mysql_real_escape_string($location_title));
					$row = $wpdb->get_row($query);
					$taid_is_ok = false;

					if ($row != null) {
						$taid = $row->geo_id;
						if (is_numeric($taid) && $taid > 0) {
							$taid_is_ok = true;
						}
						//print '<!-- #from cache: '.$taid.'# -->';
					} else {
						$contents = file_get_contents('https://www.google.com/search?q=tripadvisor+hotels+'.urlencode($location_title));
						$pattern = '/http:\/\/www\.tripadvisor(.*?)-g([0-9]+)/';
						preg_match($pattern, $contents, $matches);
						$taid = $matches[2];
						if (is_numeric($taid) && $taid > 0) {
							if ($wpdb->insert('geos_id_cache', array('geo_id' => $taid, 'geo_name' => $location_title), array('%d', '%s'))) {
								$taid_is_ok = true;
								//print '<p>#write into db#</p>';
							}
						}
						//print '<!-- #live: '.$taid.'# -->';
					}
				?>

					<?php if ($taid_is_ok) : ?>
					<div class="destination-recommendations">
						<input type="hidden" name="taid-value" value="<?php print $taid; ?>" id="taid-value" />
						<h2 id="dest-recommend">New! <span>Destination Recommendations for <?php echo $location_title; ?></span></h2>
						<div class="dest-recommend-content-wrap">
							<div class="dest-recommend-content">
								<img class="spiner" src="http://<?php print $_SERVER['SERVER_NAME']; ?>/wp-content/themes/C2D/images/ajax-loader.gif" alt="loading..." title="" />
							</div>
						</div>
					</div>
					<?php endif; ?>


					<?php
					require_once ("popular-destinations-lat-lng.php");
					if (isset($popular_destination_titles[$location_title])) {
					?>
					<script type="text/javascript">
						var end_location_name = "<?php print addslashes($location_title); ?>";
						var end_point_lat = <?php print $popular_destination_titles[$location_title][0]; ?>;
						var end_point_lng = <?php print $popular_destination_titles[$location_title][1]; ?>;
					</script>
					<div class="weather-box">
						<!-- ajax populated -->
					</div>
					<?php
					}
					?>

				<?php
				}
				?>

				</div><!-- /wide-box -->
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
	</div><!-- row -->
<?php get_footer(); ?>

<div class="front-page-lightbox">
    <div class="modal fade in"
         tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1 col-xs-10 col-xs-offset-1 text-center">
                        <h1 class="find-hotel-deals">Find <span class="trip-end"></span>
                            Hotel Deals</h1>
                        <h4>(enter the dates of your trip)</h4>
                    </div>
                </div>
            </div>
            <div class="modal-content">
                <div class="modal-body">
                    <div class="col-md-8 col-md-push-2 col-xs-12 text-center datepicker-container">
                        <i class="fa fa-calendar"></i>
                        <input id="datepicker-start" placeholder="Start Trip" class="datepicker"/>
                        <input id="datepicker-end" placeholder="End Trip" class="datepicker"/>
                    </div>
                    <div class="col-md-8 col-md-push-2 col-xs-12 text-center button-container">
                        <a class="find-deals"
                           data-ga=""
                           target="_blank"
                           href="http://www.bookingbuddy.com/c/tab-browsing/db5.html?oppLanderId=466&departure_date={Check_Out_Date}&num_travelers={Trip_Passengers}&search_mode=hotel&arrival_date={Check_In_Date}&af=16114203&arrival_city={Trip_End}&source=66414"><span
                                class="btn find-deals-button text-capitalize">FIND HOTEL DEALS</span></a>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>