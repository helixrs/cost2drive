<?php
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
/**
 * The template for displaying results page.
 *
 * @package WordPress
 * @subpackage cost_to_drive
 * @since Cost to Drive 1.0
 */


$query_string = $_SERVER['QUERY_STRING'];
// query string format:
// pagename=from&trip_info=..

// (I)
// trip_info=start-address/to/end-address/
// (II)
// trip_info=start-address/to/end-address/passengers/carId/~whatever or nothing~
// trip_info=start-address/to/end-address/2/15035/2008-Honda-Civic
// (III)
// trip_info=start-address/to/end-address/passengers/custom/mpg/gpt/fuel_type
// trip_info=start-address/to/end-address/passengers/custom/20/17/Regular

//  to the end of url goes: /via/Stop-Point/via/Stop-Point2/...

if (strpos($query_string, 'start=') === false) {

	//~$query_vars_initial = explode('/', $query_string);
	$query_vars_initial = explode('/', $_GET['trip_info']);	


	// waypoints - Stop Points: Read them from the array and then remove them from an array
	$get_waypoints = '';
	$query_vars_bkp = $query_vars_initial;
	if (in_array('via', $query_vars_bkp)) {
		// url contains: /via/Stop-Point/via/Stop-Point2/...
		$get_waypoints = array();
		$next = false;
		foreach ($query_vars_bkp as $var_key=>$var_value) {
			if ($next) {
				$get_waypoints[] = $var_value;
				unset($query_vars_initial[$var_key]);
			}
			if ($var_value == 'via') {
				$next = true;
				unset($query_vars_initial[$var_key]);
			} else {
				$next = false;
			}
		}
	}
	$waypoints_bkp = $get_waypoints;
	if ($get_waypoints == '') {
		unset ($get_waypoints);
	}
	
	// reset array indexes, make final array $query_vars without Stop Points
	$query_vars = array();
	foreach ($query_vars_initial as $var_value) {
		$query_vars[] = $var_value;
	}



	$get_start = str_replace('-', '%20', $query_vars[0]);
	$get_end =   str_replace('-', '%20', $query_vars[2]);
	$get_start_readable = str_replace('-', ' ', $query_vars[0]);
	$get_end_readable =   str_replace('-', ' ', $query_vars[2]);

	if (count($query_vars) <= 5) {

		// default car - url format: /start-address/to/end-address/
		$get_custom = true;
		$get_mpg = 25;
		$get_car_id = -1;
		$get_fuel_type = 'Regular';
		$get_passengers = 1;
		$get_gpt = 17;

	} else {

		if (in_array('custom', $query_vars)) {

			// custom car info: /start-address/to/end-address/passengers/custom/mpg/gpt/fuel_type
			$get_custom = true;
			$get_passengers = (is_numeric($query_vars[3])) ? $query_vars[3] : 1;
			$custom_var_index = array_search('custom', $query_vars);
			$custom_var_index++;
			$get_mpg = (is_numeric($query_vars[$custom_var_index])) ? $query_vars[$custom_var_index] : 25;
			$custom_var_index++;
			$get_gpt = (is_numeric($query_vars[$custom_var_index])) ? $query_vars[$custom_var_index] : 17;
			$custom_var_index++;
			$get_fuel_type = ($query_vars[$custom_var_index] != '') ? $query_vars[$custom_var_index] : 'Regular';

		} else {

			// carId from url: /start-address/to/end-address/passengers/carId/~whatever or nothing~
			$get_custom = false;
			$get_passengers = (is_numeric($query_vars[3])) ? $query_vars[3] : 1;
			$get_car_id = (is_numeric($query_vars[4])) ? $query_vars[4] : 15395;
		}
	}

} else {
	$get_start = $_GET["start"];
	$get_end =   $_GET["end"];
	$get_start_readable = $_GET["start"];
	$get_end_readable =   $_GET["end"];
	$get_custom = ($_GET["custom"] == 'true') ? true : false;
	$get_mpg = $_GET["mpg"];
	$get_car_id = $_GET["carId"];
	$get_fuel_type = $_GET["fuelType"];
	$get_waypoints = $_GET["waypoints"];
	$get_passengers = $_GET["passengers"];
	$get_gpt = $_GET["gpt"];
}


	//seconds * minutes * hours * days + current time
	$inTwoMonths = 60 * 60 * 24 * 60 + time();
	setcookie('start', $get_start_readable, $inTwoMonths, "/", ".costtodrive.com");
	setcookie('end', $get_end_readable, $inTwoMonths, "/", ".costtodrive.com");
	setcookie('carId', $get_car_id, $inTwoMonths, "/", ".costtodrive.com");

	include("app/helpers/trip-advisor.php");
	include("app/include/globals.php");
	include("app/classes/object-trip.php");
	include("app/classes/object-google-maps.php");
	include("app/classes/object-tripadvisor-destination.php");
	

	if ($get_custom==false && isset($get_car_id) && $get_car_id!="")
	{
		$carId = $get_car_id;
	}
	else
	{
		if (isset($get_mpg) && $get_mpg!="")
		{
			$carId=-1;
		}
		else
		{
			$carId = 15395;
		}
	}
	$car = new Car($carId);
	if ($get_custom==true && isset($get_mpg) && $get_mpg!="")
	{
		$car->setCustom($get_fuel_type,$get_mpg,$get_gpt);
		$carName = "Custom Car";
	}
	else
	{
		$carName = $car->year." ".$car->mfr." ".$car->make;
	}

	$trip = new Trip($car,$get_start,$get_end,$get_waypoints,$get_passengers);
	$trip->calculateCost();
	$trip->getKayak();
	$map = new C2DMap($trip);


?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php bloginfo( 'name' ); ?> | From <?php echo $trip->start; ?> to <?php echo $trip->end; ?>, in <?php print ucwords($carName); ?></title>
<meta property="og:title" content="<?php bloginfo( 'name' ); ?> | From <?php echo $trip->start; ?> to <?php echo $trip->end; ?>, in <?php print ucwords($carName); ?>" />
<meta property="og:image" content="<?php print  bloginfo('template_url'); ?>/images/structural/og-logo.png" />
<meta property="og:url" content="http://<?php print  $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]; ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="image_src" href="/wp-content/themes/C2D/images/structural/logo.png" />
<?php
// load js files here
	if (!is_admin()) {
		wp_enqueue_script('jquery');
		wp_enqueue_script('js_fancybox', get_bloginfo('template_url') . '/jquery.fancybox-1.3.4.js', array('jquery'), '1.0');
		wp_enqueue_script('js_files', get_bloginfo('template_url') . '/main.js', array('jquery'), '1.0');
	}
?>
<?php
	wp_head();
?>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script src="http://widgets.twimg.com/j/2/widget.js"></script>
<script type="text/javascript">stLight.options({publisher:'242135f6-62bd-47d6-93f5-b37899a6956c'});</script>
<?php 
	$map->printGoogleJS();
	$map->addAddress($trip->start,$trip->startPoint);
	$map->addAddress($trip->end,$trip->endPoint);
	$map->polyline=$trip->polyline;
	
	$count_waypoints = count($trip->waypointsList);
	if ($count_waypoints > 0) {
		for ($i = 0; $i < $count_waypoints-1; $i++)	{
			$map->addAddress($trip->waypointsList[$i],$trip->waypointsListPoints[$i]);
		}
	}
	
	$map->showMap();
?>

<script type="text/javascript">
	function gas_points(map) {
		var markersArrayGas = new Array();
		<?php
		$i=0;
		while($i < count($map->trip->stops) ) {
		?> 
			var myLatlng = new google.maps.LatLng(<?php print $map->trip->stops[$i]["latitude"]; ?>, <?php print $map->trip->stops[$i]["longitude"]; ?>);
			var marker<?php print $i; ?> = new google.maps.Marker({
			position: myLatlng,
			map: map, 
			icon:"/wp-content/themes/C2D/images/gas.png",
			title:"<?php print $map->trip->stops[$i]["station"]; ?>"
			});

			var infoWindow = new google.maps.InfoWindow();

			google.maps.event.addListener(marker<?php print $i; ?>, "click", function () {
				infoWindow.setContent("<?php print $map->trip->stops[$i]["station"]; ?>");
				infoWindow.open(map,marker<?php print $i; ?>);
			});
			
			markersArrayGas.push(marker<?php print $i; ?>);
		
		<?php
			$i++;
		}
		?>
		
		for (var i = 0; i < markersArrayGas.length; i++ ) {
			markersArrayGas[i].setMap(map);
		}
	}	
	function toll_points(map){return false;}
	function weather_points(map){return false;}
</script>

</head>

<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
	<div id="header">
		<div id="masthead">
			<div id="branding" role="banner">
				<div id="site-title">
					<span>
						<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="/wp-content/themes/C2D/images/structural/logo.png" alt="Cost To Drive" /></a>
					</span>
				</div>
				<div id="site-description"><?php bloginfo( 'description' ); ?></div>				
			</div><!-- #branding -->
			<div id="login-register">
				<?php if ( is_user_logged_in() ) {
					 global $current_user;
					 wp_get_current_user(); ?>
					 <span>Welcome back, <?php echo $current_user->nickname; ?>!</span> <?php
				} ?>
				
			</div>
			<div id="access" role="navigation">
				<div class="skip-link screen-reader-text"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'costtodrive' ); ?>"><?php _e( 'Skip to content', 'costtodrive' ); ?></a></div>
				<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
			</div><!-- #access -->
		</div><!-- #masthead -->
	</div><!-- #header -->

	<div id="main">

		<div id="container">
			<div id="content" role="main">
				<div class="wide-box page">
				<?php if (!is_null($trip->start) && !is_null($trip->end)) : ?>

					<div id="your_trip_info">
						<h2 id="your_trip"><span>Your Trip</span></h2>
						<a class="expand-et" href="#inline">Edit Trip</a>
						<a class="collapse-et" href="#">Cancel</a>

						<div class="trip_info">
							<p><?php echo $trip->start; ?> to <?php echo $trip->end; ?>, in <?php print ucwords($carName); ?></p>
						</div>
					</div>

					<div id="edit-trip-form">
						<!-- map breaks when this is enabled -->
						<form id="where_to_f"  action="http://<?php print $_SERVER['SERVER_NAME']; ?>/from" method="get">
							<div><?php include("app/include/trip-form-edit-trip.php");?></div>
							<div><?php  include("app/include/car-form.php");?></div>
							<input class="my_c2d_btn" type="image" src="<?php echo get_bloginfo('template_url'); ?>/images/structural/c2d-btn.gif" alt="Calculate my cost to drive" />
						</form>
						<div><?php include("app/include/car-script.php"); ?></div>
					</div> <!-- edit-trip-form -->

					<?php
						$taid = get_taid($get_end_readable);
					?>
					
					<div id="your_cost_info">
						<div class="your_cost_info_content">
							<h2 id="your_cost"><span>Your Cost To Drive Is</span></h2>
							<span id="cost">$<?php echo number_format($trip->cost,2); ?></span>
							<?php
								if ($trip->passengers > 1 ) {
									echo '<div class="trip_info"><p>The cost per passenger: $'.$trip->getCostPerPassenger().'</p></div>';
								}
							?>
							<div class="cost-info cost-info-distance">
								<span>Total Distance:</span><span><?php echo number_format($trip->distance,2);?> miles </span>
							</div>
							<div class="cost-info cost-info-time">
								<span>Driving Time:</span><span><?php echo $trip->getDrivingTime();?></span>
							</div>
						</div>					
						
					<?php
					if ($trip->distance > 50) {
						if ($trip->kayakPrice != 0) {
						?>
						<div class="cost-info cost-info-kayak-price">
							<span><a target="_blank" href="<?php echo $trip->kayakUrl;?>">Fly for $<?php echo $trip->kayakPrice;?></a></span>
						</div>
						<?php
						}
					} else {
						if ($taid != 0) {
							$tripAdvisor = new TripAdvisorDestination($taid);
							?>
							<div class="cost-info trip-advisor-short-info">
							  <?php if ($tripAdvisor->destPhotoURL != ''): ?>
								<a class="dest-photo" target="_blank" href="http://www.tripadvisor.com/Tourism-g<?php print $taid; ?>"><img src="<?php print $tripAdvisor->destPhotoURL; ?>" alt="" /></a>
							  <?php endif; ?>
								<a class="dest-desc" target="_blank" href="http://www.tripadvisor.com/Tourism-g<?php print $taid; ?>">Find top hotels, restaurants and attractions in <?php echo $trip->end; ?></a>
								<div class="powered-by"><span>Powered by</span><img class="ta-logo" src="/wp-content/themes/C2D/images/structural/tripadvisor_logo_api.gif" alt="tripadvisor"></div>
							</div>
							<?php
						}
					}
					?>
							
					</div>

					<div id="map">
						<div class="map-positioner">
							<div id="map_canvas" style="width: 646px; height: 360px;"></div>
						</div>
					</div>

					<div id="complete_directions">
						<p>Get complete directions from:</p>
						<ul>
							<li><a target="_blank" href="<?php echo $trip->googleURL;?>">Google Maps</a></li>
						</ul>
					</div>

					<?php if ($taid != 0) : ?>
					<div class="destination-recommendations">
						<input type="hidden" name="taid-value" value="<?php print $taid; ?>" id="taid-value" />
						<h2 id="dest-recommend">New! <span>Destination Recommendations for <?php echo $trip->end; ?></span></h2>
						<div class="dest-recommend-content-wrap ta-first-dest">
							<div class="dest-recommend-content">
								<img class="spiner" src="http://<?php print $_SERVER['SERVER_NAME']; ?>/wp-content/themes/C2D/images/ajax-loader.gif" alt="loading..." title="" />
							</div>
						</div>
					</div>
					<?php endif; ?>

					<div id="tech-details">
						<h2 id="tenical_details_heading"><span>Details</span></h2>
						<a class="expand" href="#">Expand</a>
						<a class="collapse" href="#">Collapse</a>

						<div class="tech-details-content">
							<div class="trip_info">
								<ul id="prices">
									<li id="gasUsed"><span>Fuel &rsaquo;</span> <?php echo number_format($trip->totalGallons,1); ?> Gallons</li>
									<li id="avgPrice"><span>Average Gas Price &rsaquo;</span> $<?php echo number_format(($trip->cost / $trip->totalGallons),2);?></li>
									<?php
										// $tolls = $trip->getTolls();
										//
										// if (count($tolls) > 0)
										// {
										// 	echo "<li>The toll roads include the following: <br>";
										// 	foreach($tolls as $toll)
										// 	{
										// 		print_r($toll);
										// 	}
										// 	echo "</li>";
										// }
									?>
								</ul>
							</div>

							<div id="tech_details">
								<table id="prices_table">
									<tbody>
										<tr id="prices_row">
											<th></th>
											<th>Miles Driven</th>
											<th>Gallons</th>
											<th>Gas Price</th>
											<th>Cost</th>
										</tr>
										<!-- <td><a onmouseover="GEvent.trigger(markers[0],&quot;click&quot;)" href="#">New York Co., NY</a></td>
										<td><a onmouseover="GEvent.trigger(markers[1],&quot;click&quot;)" href="#">Jefferson Co., PA</a></td> -->
										<?php
											foreach($trip->legs as $leg)
											{
												echo "<tr>\n";
												echo "<td>".$leg->location."</td>\n";
												echo "<td>".number_format($leg->distance,1)."</td>\n";
												echo "<td>".number_format($leg->gallons,1)."</td>\n";
												echo "<td>".number_format($leg->gasPrice,2)."</td>\n";
												echo "<td>".number_format($leg->legCost,2)."</td>\n";
												echo "</tr>\n";
											}
										?>
									</tbody>
								</table>
							</div>
							<div id="total">
								<ul>
									<li id="mini_cost"><span>Total Cost</span> $<?php echo number_format($trip->cost,2); ?></li>
								</ul>
							</div>
						</div><!-- /tech-details-content -->
					</div><!-- /details -->
					
					
					<?php
						require_once ("WeatherBash/WeatherBug.class.php");
						require_once ("WeatherBash/WeatherBugLiveWeather.class.php");
						require_once ("WeatherBash/WeatherBugForecast.class.php");
						$WeatherBug = new WeatherBug;
						$WeatherResponse = $WeatherBug->GetLiveWeatherByLatLong ($trip->endPoint->lat, $trip->endPoint->lng);
						$ForecastResponse = $WeatherBug->GetForecastByLatLong ($trip->endPoint->lat, $trip->endPoint->lng);
					?>
					
					<?php if(isset($WeatherResponse->Temperature)): ?>
					<div class="weather-box">
						<h2 id="weather-heading">
							<span>Weather</span>
						</h2>
						<p class="weather-in">Weather in <?php print $trip->end; ?></p>

						<div class="weather-box-content">
							<div class="weather-today">
								<h3>Currently</h3>
								<img src="<?php print $WeatherResponse->CurrentConditionIcon; ?>" alt="" />
								<div class="today-weather-data">
									<p class="current-condition"><?php print $WeatherResponse->Temperature; ?>&deg;F</p>
									<p class="current-weather-desc"><?php print $WeatherResponse->CurrentConditionDescription; ?></p>
									<p>Humidity: <?php print $WeatherResponse->Humidity; ?>%</p>
									<p>Wind: <?php print $WeatherResponse->WindSpeed; ?>mph</p>
								</div>
							</div>

							<div class="weather-forecast">
								<h3>5 Day Forecast</h3>
								<ul>
								  <?php $i = 1; ?>
								  <?php foreach ($ForecastResponse->Forecasts as $Forecast) : ?>
									<li>
										<p class="forecast-date"><?php print $Forecast->TitleALT; ?></p>
										<img src="<?php print $Forecast->ImageSource; ?>" alt="" />
										<?php $prediction = preg_replace('/(.+?)Chance/', '', $Forecast->ShortPrediction) ?>
										<p class="weather-desc"><?php print preg_replace('/(.+?)of/', '', $prediction); ?></p>
										<p class="max-temp"><?php print $Forecast->HighValue; ?>&deg;F</p>
										<p class="min-temp"><?php print $Forecast->LowValue; ?>&deg;F</p>
									</li>
								  <?php if ($i == 5) break; $i++; ?>
								  <?php endforeach; ?>
								</ul>
							</div>
							<p class="credits">Powered by <a href="http://www.weatherbug.com" target="_blank">WeatherBug</a></p>
						</div>
					</div>
					<?php endif; ?>

				<?php else : ?>

					<div id="your_trip_info">
						<h2 id="your_trip"><span>Error</span></h2>

						<div class="trip_info">
							<p>Start or Destination is not valid. Please <a class="expand-et-er" href="#">edit</a> your trip info.</p>
						</div>
					</div>
					<div id="edit-trip-form">
						<!-- map breaks when this is enabled -->
						<form id="where_to_f"  action="http://<?php print $_SERVER['SERVER_NAME']; ?>/from" method="get">
							<div><?php include("app/include/trip-form-edit-trip.php");?></div>
							<div><?php  include("app/include/car-form.php");?></div>
							<input class="my_c2d_btn" type="image" src="<?php echo get_bloginfo('template_url'); ?>/images/structural/c2d-btn.gif" alt="Calculate my cost to drive" />
						</form>
						<div><?php include("app/include/car-script.php"); ?></div>
					</div> <!-- edit-trip-form -->

				<?php endif; ?>
				</div><!-- /wide-box -->
			</div><!-- #content -->
		</div><!-- #container -->

		<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function($) {
				initialize();
			});
		</script>

<?php get_sidebar(); ?>
<?php get_footer(); ?>