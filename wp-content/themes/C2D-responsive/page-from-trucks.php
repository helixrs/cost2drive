<?php

header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

/**
 * The template for displaying results page.
 *
 * @package WordPress
 * @subpackage cost_to_drive
 * @since Cost to Drive 1.0
 */


$query_string = $_SERVER['QUERY_STRING'];
// query string format:
// pagename=from&trip_info=..

// (I)
// trip_info=start-address/to/end-address/
// (II)
// trip_info=start-address/to/end-address/passengers/carId/~whatever or nothing~
// trip_info=start-address/to/end-address/2/15035/2008-Honda-Civic
// (III)
// trip_info=start-address/to/end-address/passengers/custom/mpg/gpt/fuel_type
// trip_info=start-address/to/end-address/passengers/custom/20/17/Regular

//  to the end of url goes: /via/Stop-Point/via/Stop-Point2/...

if (strpos($query_string, 'start=') === false) {

    //~$query_vars_initial = explode('/', $query_string);
    $query_vars_initial = explode('/', $_GET['trip_info']);


    // waypoints - Stop Points: Read them from the array and then remove them from an array
    $get_waypoints = '';
    $query_vars_bkp = $query_vars_initial;
    if (in_array('via', $query_vars_bkp)) {
        // url contains: /via/Stop-Point/via/Stop-Point2/...
        $get_waypoints = array();
        $next = false;
        foreach ($query_vars_bkp as $var_key => $var_value) {
            if ($next) {
                $get_waypoints[] = $var_value;
                unset($query_vars_initial[$var_key]);
            }
            if ($var_value == 'via') {
                $next = true;
                unset($query_vars_initial[$var_key]);
            } else {
                $next = false;
            }
        }
    }
    $waypoints_bkp = $get_waypoints;
    if ($get_waypoints == '') {
        unset ($get_waypoints);
    }

    // reset array indexes, make final array $query_vars without Stop Points
    $query_vars = array();
    foreach ($query_vars_initial as $var_value) {
        $query_vars[] = $var_value;
    }


    $get_start = str_replace('-', '%20', $query_vars[0]);
    $get_end = str_replace('-', '%20', $query_vars[2]);
    $get_start_readable = str_replace('-', ' ', $query_vars[0]);
    $get_end_readable = str_replace('-', ' ', $query_vars[2]);

    if (count($query_vars) <= 5) {

        // default car - url format: /start-address/to/end-address/
        $get_custom = true;
        $get_mpg = 25;
        $get_car_id = -1;
        $get_fuel_type = 'Regular';
        $get_passengers = 1;
        $get_gpt = 17;

    } else {

        if (in_array('custom', $query_vars)) {

            // custom car info: /start-address/to/end-address/passengers/custom/mpg/gpt/fuel_type
            $get_custom = true;
            $get_passengers = (is_numeric($query_vars[3])) ? $query_vars[3] : 1;
            $custom_var_index = array_search('custom', $query_vars);
            $custom_var_index++;
            $get_mpg = (is_numeric($query_vars[$custom_var_index])) ? $query_vars[$custom_var_index] : 25;
            $custom_var_index++;
            $get_gpt = (is_numeric($query_vars[$custom_var_index])) ? $query_vars[$custom_var_index] : 17;
            $custom_var_index++;
            $get_fuel_type = ($query_vars[$custom_var_index] != '') ? $query_vars[$custom_var_index] : 'Regular';

        } else {

            // carId from url: /start-address/to/end-address/passengers/carId/~whatever or nothing~
            $get_custom = false;
            $get_passengers = (is_numeric($query_vars[3])) ? $query_vars[3] : 1;
            $get_car_id = (is_numeric($query_vars[4])) ? $query_vars[4] : 15395;
        }
    }

} else {
    $get_start = $_GET["start"];
    $get_end = $_GET["end"];
    $get_start_readable = $_GET["start"];
    $get_end_readable = $_GET["end"];
    $get_custom = ($_GET["custom"] == 'true') ? true : false;
    $get_mpg = $_GET["mpg"];
    $get_car_id = $_GET["carId"];
    $get_fuel_type = $_GET["fuelType"];
    $get_waypoints = $_GET["waypoints"];
    $get_passengers = $_GET["passengers"];
    $get_gpt = $_GET["gpt"];
}


//seconds * minutes * hours * days + current time
$inTwoMonths = 60 * 60 * 24 * 60 + time();
setcookie('start', $get_start_readable, $inTwoMonths, "/", ".costtodrive.com");
setcookie('end', $get_end_readable, $inTwoMonths, "/", ".costtodrive.com");
if (isset($get_car_id)) {
    setcookie('carId', $get_car_id, $inTwoMonths, "/", ".costtodrive.com");
}

include("app/helpers/trip-advisor.php");
include("app/include/globals.php");
include("app/classes/object-trip.php");
include("app/classes/object-google-maps-v2.php");
include("app/classes/object-tripadvisor-destination.php");


if ($get_custom == false && isset($get_car_id) && $get_car_id != "") {
    $carId = $get_car_id;
} else {
    if (isset($get_mpg) && $get_mpg != "") {
        $carId = -1;
    } else {
        $carId = 15395;
    }
}
$car = new Car($carId);
if ($get_custom == true && isset($get_mpg) && $get_mpg != "") {
    $car->setCustom($get_fuel_type, $get_mpg, $get_gpt);
    $carName = "Custom Car";
} else {
    $carName = $car->year . " " . $car->mfr . " " . $car->make;
}

$trip = new Trip($car, $get_start, $get_end, $get_waypoints, $get_passengers);
$trip->calculateCost();
//$trip->getKayak();
$result = json_decode($trip->getAirport());
$result = (array)$result;
$result = array_pop($result);
$airfarePrice = $result[0]->price;
$airfareURL = $result[0]->fareUrl;
$gaAirfare = explode("/", $airfareURL);
$gaAirfare = $gaAirfare[count($gaAirfare) - 2];
$map = new C2DMap($trip);


?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title><?php bloginfo('name'); ?> | From <?php echo $trip->start; ?> to <?php echo $trip->end; ?>,
        in <?php print ucwords($carName); ?></title>
    <meta property="og:title"
          content="<?php bloginfo('name'); ?> | From <?php echo $trip->start; ?> to <?php echo $trip->end; ?>, in <?php print ucwords($carName); ?>"/>
    <meta property="og:image" content="<?php print  bloginfo('template_url'); ?>/images/structural/og-logo.png"/>
    <meta property="og:url" content="http://<?php print  $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]; ?>"/>
    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>"/>
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/C2D-responsive/app/jquery-ui.css"/>
    <link rel="stylesheet" type="text/css" media="all"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"/>

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <link rel="image_src" href="/wp-content/themes/C2D/images/structural/logo.png"/>
    <?php
    // load js files here
    if (!is_admin()) {
        wp_enqueue_script('jquery');
        wp_enqueue_script('js_fancybox', get_bloginfo('template_url') . '/jquery.fancybox-1.3.4.js', array('jquery'), '1.0');
        wp_enqueue_script('js_bootstrap', get_bloginfo('template_url') . '/bootstrap.min.js', array('jquery'), '1.0');
        wp_enqueue_script('js_files', get_bloginfo('template_url') . '/main.js', array('jquery'), '1.0');
    }
    ?>
    <?php
    wp_head();
    ?>
    <script type="text/javascript">
        var end_location_name = "<?php print addslashes($trip->end); ?>";
        var end_point_lat = <?php print $trip->endPoint->lat; ?>;
        var end_point_lng = <?php print $trip->endPoint->lng; ?>;
    </script>
    <script type="text/javascript">var switchTo5x = true;</script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript" src="/wp-includes/js/jquery/ui/core.min.js"></script>
    <script type="text/javascript" src="/wp-includes/js/jquery/ui/datepicker.min.js"></script>
    <script src="http://widgets.twimg.com/j/2/widget.js"></script>
    <script type="text/javascript">stLight.options({publisher: '242135f6-62bd-47d6-93f5-b37899a6956c'});</script>
    <?php
    $map->printGoogleJS();
    $map->addAddress($trip->start, $trip->startPoint);
    $map->addAddress($trip->end, $trip->endPoint);
    $map->polyline = $trip->polyline;

    // add waypoints to $map object
    $count_waypoints = count($trip->waypointsList);
    if ($count_waypoints > 1) {
        for ($i = 0; $i < $count_waypoints - 1; $i++) {
            $map->addAddress($trip->waypointsList[$i], $trip->waypointsListPoints[$i]);
        }
    }

    // show map
    $map->showMap();


    // tripAdvisor id for destination
    $taid = get_taid($get_end_readable);
    // tripAdvisor photo url for destination
    if ($taid != 0) {
        $tripAdvisor = new TripAdvisorDestination($taid);
        $destination_ta_photo_url = $tripAdvisor->destPhotoURL;
    }

    // tripAdvisor id for start
    $taid_start = get_taid($get_start_readable);
    // tripAdvisor photo url for destination
    if ($taid_start != 0) {
        $tripAdvisor = new TripAdvisorDestination($taid_start);
        $start_ta_photo_url = $tripAdvisor->destPhotoURL;
    }

    // tripAdvisor ids/names for waypoints
    $waypoints_taid = array();
    if ($count_waypoints > 1) {
        for ($i = 0; $i < $count_waypoints - 1; $i++) {
            $waypoints_taid[$i]['name'] = $trip->waypointsList[$i];
            $waypoints_taid[$i]['taid'] = get_taid($trip->waypointsList[$i]);
        }
    }


    ?>

    <script type="text/javascript">
        function marker_points(map) {
            <?php
            $i = 0;
            foreach($map->addresses as $address) {

            // first two items in $map->addresses array are Start and Destination
            // $map->addresses[0] = Start
            // $map->addresses[1] = Destination
            if ($i == 0) {
                if ($taid_start != 0) {
                    $ta = trip_advisor_box_html($taid_start, $get_start_readable, $start_ta_photo_url);
                } else {
                    $ta = '';
                }
            } else if ($i == 1) {
                if ($taid != 0) {
                    $ta = trip_advisor_box_html($taid, $get_end_readable, $destination_ta_photo_url);
                } else {
                    $ta = '';
                }
            } else if ($i > 1) {
                if ($waypoints_taid[$i - 2]['taid'] != 0) {
                    $tripAdvisor = new TripAdvisorDestination($waypoints_taid[$i - 2]['taid']);
                    $ta = trip_advisor_box_html($waypoints_taid[$i - 2]['taid'], $waypoints_taid[$i - 2]['name'], $tripAdvisor->destPhotoURL);
                } else {
                    $ta = '';
                }
            }

            ?>
            var latlng = new google.maps.LatLng(<?php print $address->point->lat . ',' . $address->point->lng; ?>);
            var addressmarker<?php print $i; ?>  = new google.maps.Marker({
                position: latlng,
                map: map,
                title: "<?php print $address->title; ?>"
            });
            var infoWindow = new google.maps.InfoWindow();

            google.maps.event.addListener(addressmarker<?php print $i; ?>, "click", function () {
                infoWindow.setContent("<?php print $ta; ?>");
                infoWindow.open(map, addressmarker<?php print $i; ?>);
            });

            <?php
            $i++;
            }
            ?>
        }


        function gas_points(map) {
            var markersArrayGas = new Array();
            <?php
            $i = 0;
            while($i < count($map->trip->stops) ) {
            ?>
            var myLatlng = new google.maps.LatLng(<?php print $map->trip->stops[$i]["latitude"]; ?>, <?php print $map->trip->stops[$i]["longitude"]; ?>);
            var marker<?php print $i; ?> = new google.maps.Marker({
                position: myLatlng,
                map: map,
                icon: "/wp-content/themes/C2D/images/gas.png",
                title: "<?php print preg_replace('/<br.*/', '', $map->trip->stops[$i]["station"]); ?>"
            });

            var infoWindow = new google.maps.InfoWindow();

            google.maps.event.addListener(marker<?php print $i; ?>, "click", function () {
                infoWindow.setContent("<?php print $map->trip->stops[$i]["station"]; ?>");
                infoWindow.open(map, marker<?php print $i; ?>);
            });

            markersArrayGas.push(marker<?php print $i; ?>);

            <?php
            $i++;
            }
            ?>

            for (var i = 0; i < markersArrayGas.length; i++) {
                markersArrayGas[i].setMap(map);
            }
        }
        function toll_points(map) {
            return false;
        }
        function weather_points(map) {
            return false;
        }
    </script>

</head>

<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
    <div id="header">
        <div id="masthead" class="container">
            <div id="branding" role="banner" class="clearfix">
                <div id="site-description"><?php bloginfo('description'); ?></div>
            </div><!-- #branding -->
            <div id="login-register">
                <?php if (is_user_logged_in()) {
                    global $current_user;
                    wp_get_current_user(); ?>
                    <span>Welcome back, <?php echo $current_user->nickname; ?>!</span> <?php
                } ?>
            </div>

            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid clearfix">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target="#navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/"><img src="/wp-content/themes/C2D/images/structural/logo.png"
                                                              alt="Cost To Drive"/></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                        <?php
                        $custom_menu = array(
                            'theme_location' => '',
                            'menu' => 'main-nav',
                            'container' => '',
                            'container_class' => '',
                            'container_id' => '',
                            'menu_class' => 'menu nav navbar-nav',
                            'menu_id' => '',
                            'echo' => true,
                            'fallback_cb' => 'wp_page_menu',
                            'before' => '',
                            'after' => '',
                            'link_before' => '',
                            'link_after' => '',
                            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            'depth' => 0,
                            'walker' => ''
                        );
                        wp_nav_menu($custom_menu);
                        ?>
                    </div>
                </div><!-- /.container-fluid -->
            </nav>
        </div><!-- #masthead -->
    </div><!-- #header -->

    <div id="main" class="container">
        <div class="row">
            <div id="container" class="col-md-8">
                <?php /*
			<div class="survey-box">
				<p>Help us improve Cost2Drive by taking our brief ten-second survey.</p>
				<a class="i-help-btn" href="http://www.surveymonkey.com/s/D6YCCD6" target="_blank">I'll Help!</a>
			</div>
			*/ ?>
                <div id="content" role="main" class="clearfix">
                    <div class="wide-box page">
                        <?php if (!is_null($trip->start) && !is_null($trip->end)) : ?>

                            <div id="your_trip_info">
                                <h2 id="your_trip"><span>Your Trip</span></h2>
                                <a class="expand-et" href="#inline">Edit Trip</a>
                                <a class="collapse-et" href="#">Cancel</a>

                                <div class="trip_info">
                                    <p><?php echo $trip->start; ?> to <?php echo $trip->end; ?>,
                                        in <?php print ucwords($carName); ?></p>
                                </div>
                            </div>

                            <div id="edit-trip-form">
                                <!-- map breaks when this is enabled -->
                                <form id="where_to_f" class="form-horizontal"
                                      action="http://<?php print $_SERVER['SERVER_NAME']; ?>/from" method="get">
                                    <div><?php include("app/include/trip-form-edit-trip.php"); ?></div>
                                    <div><?php include("app/include/truck-form.php"); ?></div>
                                    <input class="my_c2d_btn" type="image"
                                           src="<?php echo get_bloginfo('template_url'); ?>/images/structural/c2d-btn.gif"
                                           alt="Calculate my cost to drive"/>
                                    <input class="phone-btn" type="image"
                                           src="<?php echo get_bloginfo('template_url'); ?>/images/structural/c2d-btn-phone.png"
                                           alt="Calculate my cost to drive"/>
                                </form>
                                <div><?php include("app/include/car-script.php"); ?></div>
                            </div> <!-- edit-trip-form -->

                            <div id="your_cost_info">
                                <div class="your_cost_info_content">
                                    <h2 id="your_cost"><span>Your Cost To Drive Is</span></h2>
                                    <span id="cost">$<?php echo number_format($trip->cost, 2); ?></span>
                                    <?php
                                    if ($trip->passengers > 1) {
                                        echo '<div class="trip_info"><p>The cost per passenger: $' . $trip->getCostPerPassenger() . '</p></div>';
                                    }
                                    ?>
                                    <div class="cost-info cost-info-distance">
                                        <span>Total Distance:</span><span><?php echo number_format($trip->distance, 2); ?>
                                            miles </span>
                                    </div>
                                    <div class="cost-info cost-info-time">
                                        <span>Driving Time:</span><span><?php echo $trip->getDrivingTime(); ?></span>
                                    </div>

                                    <?php
                                    $useragent = $_SERVER['HTTP_USER_AGENT'];
                                    $assistance_link = '';
                                    if (strpos($useragent, 'iPhone') !== false || strpos($useragent, 'iPad') !== false || strpos($useragent, 'iPod') !== false) {
                                        $assistance_link = 'https://itunes.apple.com/us/app/urgent.ly-roadside-assistance/id891672095';
                                    } else if (strpos($useragent, 'Android') !== false) {
                                        $assistance_link = 'https://play.google.com/store/apps/details?id=ly.urgently.devapi';
                                    } else {
                                        $assistance_link = 'http://geturgently.com/';
                                    }
                                    ?>

                                    <div class="cost-info cost-info-time roadside-assistance">
                                        <a id="rsa1" href="<?php print $assistance_link; ?>"
                                           onclick="trackOutboundLink('<?php print $assistance_link; ?>'); return false;">FREE
                                            ROADSIDE ASSISTANCE APP</a>
                                        <a style="display: none;" id="rsa1" href="#assistance"
                                           onclick="trackLightboxOpen();">Get Free Roadside Assistance</a>
                                        <span style="display: none;">Roadside Assistance <a id="rsa2" href="#assistance"
                                                                                            onclick="trackLightboxOpen();">Just $5</a></span>
                                    </div>
                                </div>
                                <div class="your_cost_airfare_watchdog">
                                    <a class="text-center airfare-link"
                                       target="_blank" data-ga="<?php echo $gaAirfare; ?>" href="<?php echo $airfareURL ?>" ><h4
                                            class="text-center"> Fly for $<?php echo $airfarePrice; ?></h4><img
                                            src="/wp-content/themes/C2D-responsive/images/awd-logo.jpg"></a>
                                </div>
                                <?php
                                if ($trip->distance > 50) {
                                    if ($trip->kayakPrice != 0) {
                                        ?>
                                        <div class="cost-info cost-info-kayak-price">
                                            <span><a target="_blank" href="<?php echo $trip->kayakUrl; ?>"
                                                     onclick="trackOutboundLink('<?php echo $trip->kayakUrl; ?>'); return false;">Fly for $<?php echo $trip->kayakPrice; ?></a></span>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    if ($taid != 0) {
                                        ?>
                                        <div class="cost-info trip-advisor-short-info">
                                            <?php if ($destination_ta_photo_url != ''): ?>
                                                <a class="dest-photo" target="_blank"
                                                   href="http://www.tripadvisor.com/Tourism-g<?php print $taid; ?>"><img
                                                        src="<?php print $destination_ta_photo_url; ?>" alt=""/></a>
                                            <?php endif; ?>
                                            <a class="dest-desc" target="_blank"
                                               href="http://www.tripadvisor.com/Tourism-g<?php print $taid; ?>">Find top
                                                hotels, restaurants and attractions in <?php echo $trip->end; ?></a>
                                            <div class="powered-by"><span>Powered by</span><img class="ta-logo"
                                                                                                src="/wp-content/themes/C2D/images/structural/tripadvisor_logo_api.gif"
                                                                                                alt="tripadvisor"></div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>

                            </div>

                            <div id="map">
                                <div class="map-positioner">
                                    <div id="map_canvas" style="width: auto; height: 360px;"></div>
                                </div>
                            </div>

                            <div id="complete_directions">
                                <p>Get complete directions from:</p>
                                <ul>
                                    <li><a target="_blank" href="<?php echo $trip->googleURL; ?>">Google Maps</a></li>
                                </ul>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12 text-center">
                                    <h4 class="find-hotel-deals">Find <?php if (isset($trip->end)) echo $trip->end ?>
                                        Hotel Deals</h4>
                                    <a class="find-deals"
                                       data-ga="hotel-deal-<?php echo $trip->end ?>"
                                       target="_blank"
                                       href="http://www.bookingbuddy.com/c/tab-browsing/db5.html?oppLanderId=449&departure_date={Check_Out_Date}&num_travelers=<?php echo $trip->passengers ?>&search_mode=hotel&arrival_date={Check_In_Date}&af=16114203&arrival_city=<?php echo $trip->end ?>&source=63838"><span
                                            class="btn find-deals-button text-capitalize">FIND DEALS</span></a>
                                </div>
                                <div class="col-md-6 col-xs-12 text-center">
                                    <i class="fa fa-calendar"></i>
                                    <input id="datepicker-start" placeholder="Start Trip" class="datepicker"/>
                                    <input id="datepicker-end" placeholder="End Trip" class="datepicker"/>
                                    <div class="small-text">
                                        <p>(enter the dates of your trip)</p>
                                    </div>
                                </div>
                            </div>

                            <!--
					<div id="roadtrippers">
						<a href="http://roadtrippers.com/" onclick="trackOutboundLink('http://roadtrippers.com'); return false;"><img class="img-responsive" src="<?php echo get_template_directory(); ?>/images/Roadtrippers_Cost2Drive_Ad_621x385.jpg" alt="Roadtrippers"></a>
					</div>
-->
                            <?php if ($taid != 0) : ?>
                                <div class="destination-recommendations">
                                    <div class="ta-main-wrap">
                                        <?php
                                        if (count($waypoints_taid) > 0) {
                                            // waypoints exist
                                            $i = 0;
                                            foreach ($waypoints_taid as $waypoint_taid) {
                                                if ($waypoint_taid['taid'] != 0) {
                                                    if ($i == 0) {
                                                        // first stop - expanded
                                                        print trip_advisor_content_first_html($waypoint_taid['taid'], $waypoint_taid['name']);
                                                    } else {
                                                        // other destinations - initially collapsed
                                                        print trip_advisor_content_html($waypoint_taid['taid'], $waypoint_taid['name']);
                                                    }
                                                }
                                                $i++;
                                            }
                                            // last stop
                                            print trip_advisor_content_html($taid, $trip->end);
                                        } else {
                                            // destination - expanded (no waypoints)
                                            print trip_advisor_content_first_html($taid, $trip->end);
                                        }
                                        ?>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div id="tech-details">
                                <h2 id="tenical_details_heading"><span>Details</span>
                                    <a class="expand" href="#">Expand</a>
                                    <a class="collapse" href="#">Collapse</a>
                                </h2>

                                <div class="tech-details-content">
                                    <div class="trip_info">
                                        <ul id="prices">
                                            <li id="gasUsed">
                                                <span>Fuel &rsaquo;</span> <?php echo number_format($trip->totalGallons, 1); ?>
                                                Gallons
                                            </li>
                                            <li id="avgPrice"><span>Average Gas Price &rsaquo;</span>
                                                $<?php echo number_format(($trip->cost / $trip->totalGallons), 2); ?>
                                            </li>
                                            <?php
                                            // $tolls = $trip->getTolls();
                                            //
                                            // if (count($tolls) > 0)
                                            // {
                                            // 	echo "<li>The toll roads include the following: <br>";
                                            // 	foreach($tolls as $toll)
                                            // 	{
                                            // 		print_r($toll);
                                            // 	}
                                            // 	echo "</li>";
                                            // }
                                            ?>
                                        </ul>
                                    </div>

                                    <div id="tech_details">
                                        <table id="prices_table">
                                            <tbody>
                                            <tr id="prices_row">
                                                <th></th>
                                                <th>Miles Driven</th>
                                                <th>Gallons</th>
                                                <th>Gas Price</th>
                                                <th>Cost</th>
                                            </tr>
                                            <!-- <td><a onmouseover="GEvent.trigger(markers[0],&quot;click&quot;)" href="#">New York Co., NY</a></td>
                                            <td><a onmouseover="GEvent.trigger(markers[1],&quot;click&quot;)" href="#">Jefferson Co., PA</a></td> -->
                                            <?php
                                            foreach ($trip->legs as $leg) {
                                                echo "<tr>\n";
                                                echo "<td>" . $leg->location . "</td>\n";
                                                echo "<td>" . number_format($leg->distance, 1) . "</td>\n";
                                                echo "<td>" . number_format($leg->gallons, 1) . "</td>\n";
                                                echo "<td>" . number_format($leg->gasPrice, 2) . "</td>\n";
                                                echo "<td>" . number_format($leg->legCost, 2) . "</td>\n";
                                                echo "</tr>\n";
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="total">
                                        <ul>
                                            <li id="mini_cost"><span>Total Cost</span>
                                                $<?php echo number_format($trip->cost, 2); ?></li>
                                        </ul>
                                    </div>
                                </div><!-- /tech-details-content -->
                            </div><!-- /details -->
                            <?php /*
					<div class="weather-box">
						<!-- ajax populated -->
					</div>
*/ ?>
                        <?php else : ?>

                            <div id="your_trip_info">
                                <h2 id="your_trip"><span>Error</span></h2>

                                <div class="trip_info">
                                    <p>Start or Destination is not valid. Please <a class="expand-et-er"
                                                                                    href="#">edit</a> your trip info.
                                    </p>
                                </div>
                            </div>
                            <div id="edit-trip-form">
                                <!-- map breaks when this is enabled -->
                                <form id="where_to_f" class="form-horizontal"
                                      action="http://<?php print $_SERVER['SERVER_NAME']; ?>/from" method="get">
                                    <div><?php include("app/include/trip-form-edit-trip.php"); ?></div>
                                    <div><?php include("app/include/car-form.php"); ?></div>
                                    <input class="my_c2d_btn" type="image"
                                           src="<?php echo get_bloginfo('template_url'); ?>/images/structural/c2d-btn.gif"
                                           alt="Calculate my cost to drive"/>
                                </form>
                                <div><?php include("app/include/car-script.php"); ?></div>
                            </div> <!-- edit-trip-form -->

                        <?php endif; ?>
                    </div><!-- /wide-box -->
                </div><!-- #content -->
            </div><!-- #container -->

            <div class="hidden">
                <div id="assistance">
                    <p>Thanks for your interest. This feature is not yet available.</p>
                    <p class="close-link"><a href="javascript:;" onclick="jQuery.fancybox.close();">Close</a></p>
                </div>
            </div>

            <script type="text/javascript" charset="utf-8">
                jQuery(document).ready(function ($) {
                    initialize();
                });
            </script>

            <?php get_sidebar(); ?>
        </div><!-- /row -->
        <div class="clb"></div>
    </div><!-- #main -->
<?php get_footer(); ?>