<?php
	
	/************************************************************************
	 * WeatherBugForecast.class.php
	 *
	 * @author		Bashkim Isai (me@bashkim.com.au)
	 * @licence		Creative Commons Attribution 3.0 Unported License
	 * @licence		http://creativecommons.org/licenses/by/3.0/
	 *
	 * @language	php
	 *
	 * @description
	 * Contains the classes required for storing and saving Forecast responses
	 *
	 * This code is released under the Creative Commons Attribution 3.0 Unported 
	 * License. You are free to use this code provided that your use or 
	 * implementation does not deminish the rights of the author or the licence. 
	 * Please attribute this original work in any remixes that you make and email 
	 * me (bash [at] phpoblib [dot] com) to let me know if you're using this code. 
	 * It's always good to hear when your work is helping others. 
	 *
	 * Proudly made in Australia
	 *
	 ***********************************************************************/
	
	/************************************************************************
	 * @class		WeatherBugForecast
	 *
	 * @description
	 * Contains general information about a forecast response
	 *
	 ***********************************************************************/
	
	class WeatherBugForecast
	{
		
		public $APIVersion;
		public $WebURL;
		
		public $ForecastsDate;
		public $Forecasts;
		
		public $LocationCity;
		public $LocationCityCode;
		public $LocationCountry;
		
		function __construct (DOMDocument $domResponse)
		{
			$dxpResponse = new DOMXPath ($domResponse);
			$dxpResponse->registerNamespace ("aws", "http://www.aws.com/aws");
			
			$this->APIVersion			= $dxpResponse->Query ("/rss[@version='2.0']/channel/aws:weather/aws:api")->item (0)->getAttribute ('version');
			$this->WebURL				= $dxpResponse->Query ("/rss[@version='2.0']/channel/aws:weather/aws:WebURL")->item (0)->nodeValue;
			
			$this->Forecasts			= Array ();
			$this->ForecastsDate		= strtotime ($dxpResponse->Query ("/rss[@version='2.0']/channel/aws:weather/aws:forecasts")->item (0)->getAttribute ('date'));
			
			/**/
			
			$dnoLocationCity			= $dxpResponse->Query ("/rss[@version='2.0']/channel/aws:weather/aws:forecasts/aws:location/aws:city");
			$dnoLocationCityCode		= $dxpResponse->Query ("/rss[@version='2.0']/channel/aws:weather/aws:forecasts/aws:location/aws:citycode");
			$dnoLocationCountry			= $dxpResponse->Query ("/rss[@version='2.0']/channel/aws:weather/aws:forecasts/aws:location/aws:country");
			
			$this->LocationCity			= (($dnoLocationCity->length		== 1) ? $dnoLocationCity->item (0)->nodeValue : NULL);
			$this->LocationCityCode		= (($dnoLocationCityCode->length	== 1) ? $dnoLocationCityCode->item (0)->nodeValue : NULL);
			$this->LocationCountry		= (($dnoLocationCountry->length		== 1) ? $dnoLocationCountry->item (0)->nodeValue : NULL);
			
			/**/
			
			$dnlForecast = $dxpResponse->Query ("/rss[@version='2.0']/channel/aws:weather/aws:forecasts/aws:forecast");
			for ($i=0; $i < $dnlForecast->length; ++$i)
			{
				$dnoForecast = $dnlForecast->item ($i);
				$this->Forecasts [] = new WeatherBugForecastDate ($dxpResponse, $dnoForecast);
			}
		}
	}
	
	/************************************************************************
	 * @class		WeatherBugForecastDate
	 *
	 * @description
	 * Contains information about one day in a forecast response
	 *
	 ***********************************************************************/
	
	class WeatherBugForecastDate
	{
		
		public $Title;
		public $TitleALT;
		public $Description;
		
		public $ShortPrediction;
		public $Prediction;
		
		public $ImageSource;
		public $ImageIcon;
		public $ImageIsNight;
		
		public $HighValue;
		public $HighUnits;
		
		public $LowValue;
		public $LowUnits;
		
		function __construct (DOMXPath $dxpResponse, DOMNode $dnoForecast)
		{
			// Day Name
			$this->Title				= $dxpResponse->Query ("aws:title", $dnoForecast)->item (0)->nodeValue;
			$this->TitleALT				= $dxpResponse->Query ("aws:title", $dnoForecast)->item (0)->getAttribute ("alttitle");
			$this->Description			= $dxpResponse->Query ("aws:description", $dnoForecast)->item (0)->nodeValue;
			
			// Predictions
			$this->ShortPrediction		= $dxpResponse->Query ("aws:short-prediction", $dnoForecast)->item (0)->nodeValue;
			$this->Prediction			= $dxpResponse->Query ("aws:prediction", $dnoForecast)->item (0)->nodeValue;
			
			// Icon
			$dnoImage = $dxpResponse->Query ("aws:image", $dnoForecast)->item (0);
			$this->ImageSource			= $dnoImage->nodeValue;
			$this->ImageIcon			= $dnoImage->getAttribute ("icon");
			$this->ImageIsNight			= $dnoImage->getAttribute ("isNight");
			
			// High Temperature
			$dnoHigh = $dxpResponse->Query ("aws:high", $dnoForecast)->item (0);
			$this->HighValue			= $dnoHigh->nodeValue;
			$this->HighUnits			= $dnoHigh->getAttribute ("units");
			
			// Low Temperature
			$dnoLow = $dxpResponse->Query ("aws:low", $dnoForecast)->item (0);
			$this->LowValue				= $dnoLow->nodeValue;
			$this->LowUnits				= $dnoLow->getAttribute ("units");
		}
	}
	
?>