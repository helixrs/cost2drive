<?php
	
	/************************************************************************
	 * WeatherBugCamera.class.php
	 *
	 * @author		Bashkim Isai (me@bashkim.com.au)
	 * @licence		Creative Commons Attribution 3.0 Unported License
	 * @licence		http://creativecommons.org/licenses/by/3.0/
	 *
	 * @language	php
	 *
	 * @description
	 * Contains the classes required for storing and saving Camera Responses
	 *
	 * This code is released under the Creative Commons Attribution 3.0 Unported 
	 * License. You are free to use this code provided that your use or 
	 * implementation does not deminish the rights of the author or the licence. 
	 * Please attribute this original work in any remixes that you make and email 
	 * me (bash [at] phpoblib [dot] com) to let me know if you're using this code. 
	 * It's always good to hear when your work is helping others. 
	 *
	 * Proudly made in Australia
	 *
	 ***********************************************************************/
	
	/************************************************************************
	 * @class		WeatherBugCameras
	 *
	 * @description
	 * Contains the information about a Camera Response
	 *
	 ***********************************************************************/
	
	class WeatherBugCameras
	{
		
		public $APIVersion;
		public $Cameras;
		
		function __construct (DOMDocument $domResponse)
		{
			$dxpResponse = new DOMXPath ($domResponse);
			$dxpResponse->registerNamespace ("aws", "http://www.aws.com/aws");
			
			// Save the API Version
			$this->APIVersion	= $dxpResponse->Query ("/aws:weather/aws:api")->item (0)->getAttribute ('version');
			
			// Save the Camera Objects in an Array
			$this->Cameras	= Array ();
			$dnlCamera		= $dxpResponse->Query ("/aws:weather/aws:cameras/aws:camera");
			
			foreach ($dnlCamera as $dnoCamera)
			{
				$this->Cameras [] = new WeatherBugCamera ($dnoCamera);
			}
		}
	}
	
	/************************************************************************
	 * @class		WeatherBugCamera
	 *
	 * @description
	 * Contains information about an individual camera
	 *
	 ***********************************************************************/
	
	class WeatherBugCamera
	{
		
		public $ID;
		public $Name;
		public $City;
		public $State;
		public $ZipCode;
		public $Distance;
		public $Unit;
		
		function __construct ($dnoLocation)
		{
			// Save all the Camera Information
			$this->ID			= $dnoLocation->getAttribute ('id');
			$this->Name			= $dnoLocation->getAttribute ('name');
			$this->City			= $dnoLocation->getAttribute ('city');
			$this->State		= $dnoLocation->getAttribute ('state');
			$this->ZipCode		= $dnoLocation->getAttribute ('zipcode');
			$this->Distance		= $dnoLocation->getAttribute ('distance');
			$this->Unit			= $dnoLocation->getAttribute ('Unit');
		}
	}
	
?>