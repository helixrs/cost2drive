<?php
	
	/************************************************************************
	 * WeatherBug.class.php
	 *
	 * @author		Bashkim Isai (me@bashkim.com.au)
	 * @licence		Creative Commons Attribution 3.0 Unported License
	 * @licence		http://creativecommons.org/licenses/by/3.0/
	 *
	 * @language	php
	 *
	 * @description
	 * A wrapper for connecting to the Weather Bug API
	 *
	 * This code is released under the Creative Commons Attribution 3.0 Unported 
	 * License. You are free to use this code provided that your use or 
	 * implementation does not deminish the rights of the author or the licence. 
	 * Please attribute this original work in any remixes that you make and email 
	 * me (bash [at] phpoblib [dot] com) to let me know if you're using this code. 
	 * It's always good to hear when your work is helping others. 
	 *
	 * Proudly made in Australia
	 *
	 ***********************************************************************/
	
	/************************************************************************
	 * @class		WeatherBug
	 *
	 * @description
	 * Main wrapper class for making requests to WeatherBug
	 *
	 ***********************************************************************/
	
	class WeatherBug
	{
		
		// Make sure you set this before you start
		const WEATHER_BUG_API_KEY				= 'A5454729584';
		
		// The different types of requests that can be made
		const WEATHER_BUG_REQUEST_FORECAST		= 'getForecastRSS';
		const WEATHER_BUG_REQUEST_LIVE_WEATHER	= 'getLiveWeatherRSS';
		const WEATHER_BUG_REQUEST_CAMERAS		= 'getCamerasXML';
		const WEATHER_BUG_REQUEST_STATIONS		= 'getStationsXML';
		const WEATHER_BUG_REQUEST_LOCATIONS		= 'getLocationsXML';
		
		// Measurement types
		const WEATHER_BUG_UNIT_TYPE_METRIC		= 1;
		const WEATHER_BUG_UNIT_TYPE_IMPERIAL	= 0;
		
		// Selected measurement type
		static $UnitType = self::WEATHER_BUG_UNIT_TYPE_IMPERIAL;
		
		/************************************************************************
		 * @class		WeatherBug
		 * @method		UseMetric
		 *
		 * @description
		 * Call this method when you want to use the Metric System
		 *
		 ***********************************************************************/
		
		public function UseMetric ()
		{
			self::$UnitType = self::WEATHER_BUG_UNIT_TYPE_METRIC;
		}
		
		/************************************************************************
		 * @class		WeatherBug
		 * @method		UseMetric
		 *
		 * @description
		 * Call this method when you want to use the Imperial System
		 *
		 ***********************************************************************/
		
		public function UseImperial ()
		{
			self::$UnitType = self::WEATHER_BUG_UNIT_TYPE_IMPERIAL;
		}
		
		/************************************************************************
		 * @class		WeatherBug
		 * @method		GetLiveWeatherByLatLong
		 *
		 * @param		$fltLat		Latitude Location
		 * @param		$fltLong	Longitude Location
		 *
		 * @description
		 * Get the current live weather of a weather station closest to a 
		 * particular Latitude and Longitude location
		 *
		 ***********************************************************************/
		
		public function GetLiveWeatherByLatLong ($fltLat, $fltLong)
		{
			$domRepsonse = $this->_Request (
				self::WEATHER_BUG_REQUEST_LIVE_WEATHER,
				TRUE,
				Array (
					'lat'	=> $fltLat,
					'long'	=> $fltLong
				)
			);
			
			return new WeatherBugLiveWeather ($domRepsonse);
		}
		
		/************************************************************************
		 * @class		WeatherBug
		 * @method		GetLiveWeatherByCityCode
		 *
		 * @param		$intCityCode	A City Code (outside U.S.)
		 *
		 * @description
		 * Get the current live weather of a weather station closest to a 
		 * particular City Code
		 *
		 ***********************************************************************/
		
		public function GetLiveWeatherByCityCode ($intCityCode)
		{
			$domRepsonse = $this->_Request (
				self::WEATHER_BUG_REQUEST_LIVE_WEATHER,
				TRUE,
				Array (
					'citycode'	=> $intCityCode
				)
			);
			
			return new WeatherBugLiveWeather ($domRepsonse);
		}
		
		/************************************************************************
		 * @class		WeatherBug
		 * @method		GetLiveWeatherByZipCode
		 *
		 * @param		$intZipCode		ZIP Code
		 *
		 * @description
		 * Get the current live weather of a weather station closest to a 
		 * particular U.S. ZIP Code
		 *
		 ***********************************************************************/
		
		public function GetLiveWeatherByZipCode ($intZipCode)
		{
			$domRepsonse = $this->_Request (
				self::WEATHER_BUG_REQUEST_LIVE_WEATHER,
				TRUE,
				Array (
					'zipcode'	=> $intZipCode
				)
			);
			
			return new WeatherBugLiveWeather ($domRepsonse);
		}
		
		/************************************************************************
		 * @class		WeatherBug
		 * @method		GetLiveWeatherByStationId
		 *
		 * @param		$strStation		A station identifier
		 *
		 * @description
		 * Get the current live weather of a weather station
		 *
		 ***********************************************************************/
		
		public function GetLiveWeatherByStationId ($strStation)
		{
			$domRepsonse = $this->_Request (
				self::WEATHER_BUG_REQUEST_LIVE_WEATHER,
				TRUE,
				Array (
					'stationid'	=> $strStation
				)
			);
			
			return new WeatherBugLiveWeather ($domRepsonse);
		}
		
		/************************************************************************
		 * @class		WeatherBug
		 * @method		GetForecastByStationId
		 *
		 * @param		$fltLat		Latitude Location
		 * @param		$fltLong	Longitude Location
		 *
		 * @description
		 * Get a forecast of a weather station by a particular Latitude/Longitude
		 *
		 ***********************************************************************/
		
		public function GetForecastByLatLong ($fltLat, $fltLong)
		{
			$domRepsonse = $this->_Request (
				self::WEATHER_BUG_REQUEST_FORECAST,
				TRUE,
				Array (
					'lat'	=> $fltLat,
					'long'	=> $fltLong
				)
			);
			
			return new WeatherBugForecast ($domRepsonse);
		}
		
		/************************************************************************
		 * @class		WeatherBug
		 * @method		GetForecastByZipCode
		 *
		 * @param		$intZipCode		ZIP Code
		 *
		 * @description
		 * Get a forecast of a weather station by a particular U.S. ZIP Code
		 *
		 ***********************************************************************/
		
		public function GetForecastByZipCode ($intZipCode)
		{
			$domRepsonse = $this->_Request (
				self::WEATHER_BUG_REQUEST_FORECAST,
				TRUE,
				Array (
					'zipcode'	=> $intZipCode
				)
			);
			
			return new WeatherBugForecast ($domRepsonse);
		}
		
		/************************************************************************
		 * @class		WeatherBug
		 * @method		GetForecastByCityCode
		 *
		 * @param		$intCityCode	A City Code (outside U.S.)
		 *
		 * @description
		 * Get a forecast of a weather station by a particular City Code
		 *
		 ***********************************************************************/
		
		public function GetForecastByCityCode ($intCityCode)
		{
			$domRepsonse = $this->_Request (
				self::WEATHER_BUG_REQUEST_FORECAST,
				TRUE,
				Array (
					'citycode'	=> $intCityCode
				)
			);
			
			return new WeatherBugForecast ($domRepsonse);
		}
		
		/************************************************************************
		 * @class		WeatherBug
		 * @method		GetLocations
		 *
		 * @param		$strSearchString		An approximate name being searched
		 *
		 * @description
		 * Attempt to find locations which match a search string
		 *
		 ***********************************************************************/
		
		public function GetLocations ($strSearchString)
		{
			$domRepsonse = $this->_Request (
				self::WEATHER_BUG_REQUEST_LOCATIONS,
				FALSE,
				Array (
					'SearchString'	=> $strSearchString
				)
			);
			
			return new WeatherBugLocations ($domRepsonse);
		}
		
		/************************************************************************
		 * @class		WeatherBug
		 * @method		GetStationsByZipCode
		 *
		 * @param		$intZipCode		ZIP Code
		 *
		 * @description
		 * Attempt to find a Weather Station based on a U.S. ZIP Code
		 *
		 ***********************************************************************/
		
		public function GetStationsByZipCode ($intZipCode)
		{
			$domRepsonse = $this->_Request (
				self::WEATHER_BUG_REQUEST_STATIONS,
				TRUE,
				Array (
					'zipCode'	=> $intZipCode
				)
			);
			
			return new WeatherBugStations ($domRepsonse);
		}
		
		/************************************************************************
		 * @class		WeatherBug
		 * @method		GetStationsByCityCode
		 *
		 * @param		$intCityCode	A City Code (outside U.S.)
		 *
		 * @description
		 * Attempt to find a Weather Station based on a City Code
		 *
		 ***********************************************************************/
		
		public function GetStationsByCityCode ($intCityCode)
		{
			$domRepsonse = $this->_Request (
				self::WEATHER_BUG_REQUEST_STATIONS,
				FALSE,
				Array (
					'cityCode'	=> $intCityCode
				)
			);
			
			return new WeatherBugStations ($domRepsonse);
		}
		
		/************************************************************************
		 * @class		WeatherBug
		 * @method		GetStationsByLatLong
		 *
		 * @param		$fltLat		Latitude Location
		 * @param		$fltLong	Longitude Location
		 *
		 * @description
		 * Attempt to find a Weather Station based on a Latitude/Longitude location
		 *
		 ***********************************************************************/
		
		public function GetStationsByLatLong ($fltLat, $fltLong)
		{
			$domRepsonse = $this->_Request (
				self::WEATHER_BUG_REQUEST_STATIONS,
				FALSE,
				Array (
					'lat'	=> $fltLat,
					'long'	=> $fltLong
				)
			);
			
			return new WeatherBugStations ($domRepsonse);
		}
		
		/************************************************************************
		 * @class		WeatherBug
		 * @method		GetCamerasByLatLong
		 *
		 * @param		$fltLat		Latitude Location
		 * @param		$fltLong	Longitude Location
		 *
		 * @description
		 * Attempt to find surrounding Cameras based on a Latitude/Longitude location
		 *
		 ***********************************************************************/
		
		public function GetCamerasByLatLong ($fltLat, $fltLong)
		{
			$domRepsonse = $this->_Request (
				self::WEATHER_BUG_REQUEST_CAMERAS,
				TRUE,
				Array (
					'lat'	=> $fltLat,
					'long'	=> $fltLong
				)
			);
			
			return new WeatherBugCameras ($domRepsonse);
		}
		
		/************************************************************************
		 * @class		WeatherBug
		 * @method		GetCamerasByLatLong
		 *
		 * @param		$intZipCode		ZIP Code
		 *
		 * @description
		 * Attempt to find surrounding Cameras based on a U.S. ZIP Code
		 *
		 ***********************************************************************/
		
		public function GetCamerasByZipCode ($intZipCode)
		{
			$domRepsonse = $this->_Request (
				self::WEATHER_BUG_REQUEST_CAMERAS,
				TRUE,
				Array (
					'zipCode'	=> $intZipCode
				)
			);
			
			return new WeatherBugCameras ($domRepsonse);
		}
		
		/************************************************************************
		 * @private
		 * @class		WeatherBug
		 * @method		_Request
		 *
		 * @param		$strFilename			The filename/function being requested from WeatherBug
		 * @param		$bolIncludeUnitType		Whether or not to include the UnitType in this Request
		 * @param		$arrParam				A Key=>Value array of any further params to pass
		 *
		 * @description
		 * Does all the sending/receiving of data to/from WeatherBug
		 *
		 ***********************************************************************/
		
		private function _Request ($strFilename, $bolIncludeUnitType, $arrParam)
		{
			if (self::WEATHER_BUG_API_KEY == "")
			{
				echo	'You must fill in your API key from WeatherBug.<br />' .
						'Please open WeatherBug.class.php and fill in &quot;WEATHER_BUG_API_KEY&quot;';
				exit;
			}
			
			$strRequestFile = 'http://api.wxbug.net/' . $strFilename . '.aspx';
			$strRequestFile .= '?ACode=' . self::WEATHER_BUG_API_KEY;
			
			if ($bolIncludeUnitType)
			{
				$strRequestFile .= '&unittype=' . self::$UnitType;
			}
			
			foreach ($arrParam as $strKey => $strValue)
			{
				$strRequestFile .= '&' . $strKey . '=' . urlencode ($strValue);
			}
			
			$strResponse = file_get_contents ($strRequestFile, 'r');
			
			$domResponse = new DOMDocument ('1.0', 'utf-8');
			$domResponse->loadXML ($strResponse);
			
			return $domResponse;
		}
	}
	
?>