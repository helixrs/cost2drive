<?php
	
	/************************************************************************
	 * WeatherBugLiveWeather.class.php
	 *
	 * @author		Bashkim Isai (me@bashkim.com.au)
	 * @licence		Creative Commons Attribution 3.0 Unported License
	 * @licence		http://creativecommons.org/licenses/by/3.0/
	 *
	 * @language	php
	 *
	 * @description
	 * Contains Live Weather Response information for a requested location
	 *
	 * This code is released under the Creative Commons Attribution 3.0 Unported 
	 * License. You are free to use this code provided that your use or 
	 * implementation does not deminish the rights of the author or the licence. 
	 * Please attribute this original work in any remixes that you make and email 
	 * me (bash [at] phpoblib [dot] com) to let me know if you're using this code. 
	 * It's always good to hear when your work is helping others. 
	 *
	 * Proudly made in Australia
	 *
	 ***********************************************************************/
	
	/************************************************************************
	 * @class		WeatherBugLiveWeather
	 *
	 * @description
	 * Used as a singular object to store Live Weather information
	 *
	 ***********************************************************************/
	
	class WeatherBugLiveWeather
	{
		
		public $APIVersion;
		public $WebURL;
		
		public $ObservationDateTime;
		
		public $RequestedStationID;
		public $StationID;
		public $Station;
		public $CityState;
		public $CityCode;
		public $Country;
		public $Latitude;
		public $Longitude;
		public $SiteURL;
		
		public $AuxTemperature;
		public $AuxTemperatureUnits;
		public $AuxTemperatureRate;
		public $AuxTemperatureRateUnits;
		
		public $CurrentConditionIcon;
		public $CurrentConditionDescription;
		
		public $DewPoint;
		public $DewPointUnits;
		public $FeelsLike;
		public $FeelsLikeUnits;
		
		public $Elevation;
		public $ElevationUnits;
		
		public $GustDateTime;
		public $GustDirection;
		public $GustSpeed;
		public $GustSpeedUnits;
		
		public $Humidity;
		public $HumidityUnits;
		public $HumidityRate;
		public $HumidityHigh;
		public $HumidityHighUnits;
		public $HumidityLow;
		public $HumidityLowUnits;
		
		public $IndoorTemperature;
		public $IndoorTemperatureUnits;
		public $IndoorTemperatureRate;
		public $IndoorTemperatureRateUnits;
		
		public $Light;
		public $LightRate;
		public $MoonPhaseImage;
		public $MoonPhaseDescription;
		
		public $Pressure;
		public $PressureUnits;
		public $PressureHigh;
		public $PressureHighUnits;
		public $PressureLow;
		public $PressureLowUnits;
		public $PressureRate;
		public $PressureRateUnits;
		
		public $RainToday;
		public $RainTodayUnits;
		public $RainMonth;
		public $RainMonthUnits;
		public $RainRate;
		public $RainRateUnits;
		public $RainRateMax;
		public $RainRateMaxUnits;
		public $RainYear;
		public $RainYearUnits;
		
		public $Temperature;
		public $TemperatureUnits;
		public $TemperatureHigh;
		public $TemperatureHighUnits;
		public $TemperatureLow;
		public $TemperatureLowUnits;
		public $TemperatureRate;
		public $TemperatureRateUnits;
		
		public $Sunrise;
		public $Sunset;
		
		public $WetBulb;
		public $WetBulbUnits;
		
		public $WindSpeed;
		public $WindSpeedUnits;
		public $WindSpeedAverage;
		public $WindSpeedAverageUnits;
		public $WindDirection;
		public $WindDirectionAverage;
		
		function __construct (DOMDocument $domResponse)
		{
			$dxpResponse = new DOMXPath ($domResponse);
			$dxpResponse->registerNamespace ("aws", "http://www.aws.com/aws");
			
			// API Information
			
			$this->APIVersion					= $dxpResponse->Query ("/rss[@version='2.0']/channel/aws:weather/aws:api")->item (0)->getAttribute ('version');
			$this->WebURL						= $dxpResponse->Query ("/rss[@version='2.0']/channel/aws:weather/aws:WebURL")->item (0)->nodeValue;
			
			// Get the Observation as a base to make it easier to pragmatically get information from it later
			
			$dxpObservation = $dxpResponse->Query ("/rss[@version='2.0']/channel/aws:weather/aws:ob")->item (0);
			
			$this->ObservationDateTime			= mktime (
				$dxpResponse->Query ("aws:ob-date/aws:hour", $dxpObservation)->item (0)->getAttribute ("hour-24"),
				$dxpResponse->Query ("aws:ob-date/aws:minute", $dxpObservation)->item (0)->getAttribute ("number"),
				$dxpResponse->Query ("aws:ob-date/aws:second", $dxpObservation)->item (0)->getAttribute ("number"),
				$dxpResponse->Query ("aws:ob-date/aws:month", $dxpObservation)->item (0)->getAttribute ("number"),
				$dxpResponse->Query ("aws:ob-date/aws:day", $dxpObservation)->item (0)->getAttribute ("number"),
				$dxpResponse->Query ("aws:ob-date/aws:year", $dxpObservation)->item (0)->getAttribute ("number")
			);
			
			$this->RequestedStationID			= $dxpResponse->Query ("aws:requested-station-id", $dxpObservation)->item (0)->nodeValue;
			$this->StationID					= $dxpResponse->Query ("aws:station-id", $dxpObservation)->item (0)->nodeValue;
			$this->Station						= $dxpResponse->Query ("aws:station", $dxpObservation)->item (0)->nodeValue;
			$this->CityState					= $dxpResponse->Query ("aws:city-state", $dxpObservation)->item (0)->nodeValue;
			$this->CityCode						= $dxpResponse->Query ("aws:city-state", $dxpObservation)->item (0)->getAttribute ("citycode");
			$this->Country						= $dxpResponse->Query ("aws:country", $dxpObservation)->item (0)->nodeValue;
			$this->Latitude						= $dxpResponse->Query ("aws:latitude", $dxpObservation)->item (0)->nodeValue;
			$this->Longitude					= $dxpResponse->Query ("aws:longitude", $dxpObservation)->item (0)->nodeValue;
			$this->SiteURL						= $dxpResponse->Query ("aws:site-url", $dxpObservation)->item (0)->nodeValue;
			
			$this->AuxTemperature				= $dxpResponse->Query ("aws:aux-temp", $dxpObservation)->item (0)->nodeValue;
			$this->AuxTemperatureUnits			= $dxpResponse->Query ("aws:aux-temp", $dxpObservation)->item (0)->getAttribute ("units");
			$this->AuxTemperatureRate			= $dxpResponse->Query ("aws:aux-temp-rate", $dxpObservation)->item (0)->nodeValue;
			$this->AuxTemperatureRateUnits		= $dxpResponse->Query ("aws:aux-temp-rate", $dxpObservation)->item (0)->getAttribute ("units");
			
			$this->CurrentConditionIcon			= $dxpResponse->Query ("aws:current-condition", $dxpObservation)->item (0)->getAttribute ("icon");
			$this->CurrentConditionDescription	= $dxpResponse->Query ("aws:current-condition", $dxpObservation)->item (0)->nodeValue;
			
			$this->DewPoint						= $dxpResponse->Query ("aws:dew-point", $dxpObservation)->item (0)->nodeValue;
			$this->DewPointUnits				= $dxpResponse->Query ("aws:dew-point", $dxpObservation)->item (0)->getAttribute ("units");
			$this->FeelsLike					= $dxpResponse->Query ("aws:feels-like", $dxpObservation)->item (0)->nodeValue;
			$this->FeelsLikeUnits				= $dxpResponse->Query ("aws:feels-like", $dxpObservation)->item (0)->getAttribute ("units");
			
			$this->Elevation					= $dxpResponse->Query ("aws:elevation", $dxpObservation)->item (0)->nodeValue;
			$this->ElevationUnits				= $dxpResponse->Query ("aws:elevation", $dxpObservation)->item (0)->getAttribute ("units");
			
			$this->GustDirection				= $dxpResponse->Query ("aws:gust-direction", $dxpObservation)->item (0)->nodeValue;
			$this->GustSpeed					= $dxpResponse->Query ("aws:gust-speed", $dxpObservation)->item (0)->nodeValue;
			$this->GustSpeedUnits				= $dxpResponse->Query ("aws:gust-speed", $dxpObservation)->item (0)->getAttribute ("units");
			$this->GustDateTime					= mktime (
				$dxpResponse->Query ("aws:gust-time/aws:hour", $dxpObservation)->item (0)->getAttribute ("hour-24"),
				$dxpResponse->Query ("aws:gust-time/aws:minute", $dxpObservation)->item (0)->getAttribute ("number"),
				$dxpResponse->Query ("aws:gust-time/aws:second", $dxpObservation)->item (0)->getAttribute ("number"),
				$dxpResponse->Query ("aws:gust-time/aws:month", $dxpObservation)->item (0)->getAttribute ("number"),
				$dxpResponse->Query ("aws:gust-time/aws:day", $dxpObservation)->item (0)->getAttribute ("number"),
				$dxpResponse->Query ("aws:gust-time/aws:year", $dxpObservation)->item (0)->getAttribute ("number")
			);
			
			
			$this->Humidity						= $dxpResponse->Query ("aws:humidity", $dxpObservation)->item (0)->nodeValue;
			$this->HumidityUnits				= $dxpResponse->Query ("aws:humidity", $dxpObservation)->item (0)->getAttribute ("units");
			$this->HumidityRate					= $dxpResponse->Query ("aws:humidity-rate", $dxpObservation)->item (0)->nodeValue;
			$this->HumidityHigh					= $dxpResponse->Query ("aws:humidity-high", $dxpObservation)->item (0)->nodeValue;
			$this->HumidityHighUnits			= $dxpResponse->Query ("aws:humidity-high", $dxpObservation)->item (0)->getAttribute ("units");
			$this->HumidityLow					= $dxpResponse->Query ("aws:humidity-low", $dxpObservation)->item (0)->nodeValue;
			$this->HumidityLowUnits				= $dxpResponse->Query ("aws:humidity-low", $dxpObservation)->item (0)->getAttribute ("units");
			
			$this->IndoorTemperature				= $dxpResponse->Query ("aws:indoor-temp", $dxpObservation)->item (0)->nodeValue;
			$this->IndoorTemperatureUnits		= $dxpResponse->Query ("aws:indoor-temp", $dxpObservation)->item (0)->getAttribute ("units");
			$this->IndoorTemperatureRate			= $dxpResponse->Query ("aws:indoor-temp-rate", $dxpObservation)->item (0)->nodeValue;
			$this->IndoorTemperatureRateUnits	= $dxpResponse->Query ("aws:indoor-temp-rate", $dxpObservation)->item (0)->getAttribute ("units");
			
			$this->Light						= $dxpResponse->Query ("aws:light", $dxpObservation)->item (0)->nodeValue;
			$this->LightRate					= $dxpResponse->Query ("aws:light-rate", $dxpObservation)->item (0)->nodeValue;
			$this->MoonPhaseImage				= $dxpResponse->Query ("aws:moon-phase", $dxpObservation)->item (0)->nodeValue;
			$this->MoonPhaseDescription			= $dxpResponse->Query ("aws:moon-phase", $dxpObservation)->item (0)->getAttribute ("moon-phase-img");
			
			$this->Pressure						= $dxpResponse->Query ("aws:pressure", $dxpObservation)->item (0)->nodeValue;
			$this->PressureUnits				= $dxpResponse->Query ("aws:pressure", $dxpObservation)->item (0)->getAttribute ("units");
			$this->PressureHigh					= $dxpResponse->Query ("aws:pressure-high", $dxpObservation)->item (0)->nodeValue;
			$this->PressureHighUnits			= $dxpResponse->Query ("aws:pressure-high", $dxpObservation)->item (0)->getAttribute ("units");
			$this->PressureLow					= $dxpResponse->Query ("aws:pressure-low", $dxpObservation)->item (0)->nodeValue;
			$this->PressureLowUnits				= $dxpResponse->Query ("aws:pressure-low", $dxpObservation)->item (0)->getAttribute ("units");
			$this->PressureRate					= $dxpResponse->Query ("aws:pressure-rate", $dxpObservation)->item (0)->nodeValue;
			$this->PressureRateUnits			= $dxpResponse->Query ("aws:pressure-rate", $dxpObservation)->item (0)->getAttribute ("units");
			
			$this->RainToday					= $dxpResponse->Query ("aws:rain-today", $dxpObservation)->item (0)->nodeValue;
			$this->RainTodayUnits				= $dxpResponse->Query ("aws:rain-today", $dxpObservation)->item (0)->getAttribute ("units");
			$this->RainMonth					= $dxpResponse->Query ("aws:rain-month", $dxpObservation)->item (0)->nodeValue;
			$this->RainMonthUnits				= $dxpResponse->Query ("aws:rain-month", $dxpObservation)->item (0)->getAttribute ("units");
			$this->RainRate						= $dxpResponse->Query ("aws:rain-rate", $dxpObservation)->item (0)->nodeValue;
			$this->RainRateUnits				= $dxpResponse->Query ("aws:rain-rate", $dxpObservation)->item (0)->getAttribute ("units");
			$this->RainRateMax					= $dxpResponse->Query ("aws:rain-rate-max", $dxpObservation)->item (0)->nodeValue;
			$this->RainRateMaxUnits				= $dxpResponse->Query ("aws:rain-rate-max", $dxpObservation)->item (0)->getAttribute ("units");
			$this->RainYear						= $dxpResponse->Query ("aws:rain-year", $dxpObservation)->item (0)->nodeValue;
			$this->RainYearUnits				= $dxpResponse->Query ("aws:rain-year", $dxpObservation)->item (0)->getAttribute ("units");
			
			$this->Temperature					= $dxpResponse->Query ("aws:temp", $dxpObservation)->item (0)->nodeValue;
			$this->TemperatureUnits				= $dxpResponse->Query ("aws:temp", $dxpObservation)->item (0)->getAttribute ("units");
			$this->TemperatureHigh				= $dxpResponse->Query ("aws:temp-high", $dxpObservation)->item (0)->nodeValue;
			$this->TemperatureHighUnits			= $dxpResponse->Query ("aws:temp-high", $dxpObservation)->item (0)->getAttribute ("units");
			$this->TemperatureLow				= $dxpResponse->Query ("aws:temp-low", $dxpObservation)->item (0)->nodeValue;
			$this->TemperatureLowUnits			= $dxpResponse->Query ("aws:temp-low", $dxpObservation)->item (0)->getAttribute ("units");
			$this->TemperatureRate				= $dxpResponse->Query ("aws:temp-rate", $dxpObservation)->item (0)->nodeValue;
			$this->TemperatureRateUnits			= $dxpResponse->Query ("aws:temp-rate", $dxpObservation)->item (0)->getAttribute ("units");
			
			$this->Sunrise						= mktime (
				$dxpResponse->Query ("aws:sunrise/aws:hour", $dxpObservation)->item (0)->getAttribute ("hour-24"),
				$dxpResponse->Query ("aws:sunrise/aws:minute", $dxpObservation)->item (0)->getAttribute ("number"),
				$dxpResponse->Query ("aws:sunrise/aws:second", $dxpObservation)->item (0)->getAttribute ("number"),
				$dxpResponse->Query ("aws:sunrise/aws:month", $dxpObservation)->item (0)->getAttribute ("number"),
				$dxpResponse->Query ("aws:sunrise/aws:day", $dxpObservation)->item (0)->getAttribute ("number"),
				$dxpResponse->Query ("aws:sunrise/aws:year", $dxpObservation)->item (0)->getAttribute ("number")
			);
			
			$this->Sunset						= mktime (
				$dxpResponse->Query ("aws:sunset/aws:hour", $dxpObservation)->item (0)->getAttribute ("hour-24"),
				$dxpResponse->Query ("aws:sunset/aws:minute", $dxpObservation)->item (0)->getAttribute ("number"),
				$dxpResponse->Query ("aws:sunset/aws:second", $dxpObservation)->item (0)->getAttribute ("number"),
				$dxpResponse->Query ("aws:sunset/aws:month", $dxpObservation)->item (0)->getAttribute ("number"),
				$dxpResponse->Query ("aws:sunset/aws:day", $dxpObservation)->item (0)->getAttribute ("number"),
				$dxpResponse->Query ("aws:sunset/aws:year", $dxpObservation)->item (0)->getAttribute ("number")
			);
			
			$this->WetBulb						= $dxpResponse->Query ("aws:wet-bulb", $dxpObservation)->item (0)->nodeValue;
			$this->WetBulbUnits					= $dxpResponse->Query ("aws:wet-bulb", $dxpObservation)->item (0)->getAttribute ("units");
			
			$this->WindSpeed					= $dxpResponse->Query ("aws:wind-speed", $dxpObservation)->item (0)->nodeValue;
			$this->WindSpeedUnits				= $dxpResponse->Query ("aws:wind-speed", $dxpObservation)->item (0)->getAttribute ("units");
			$this->WindSpeedAverage				= $dxpResponse->Query ("aws:wind-speed-avg", $dxpObservation)->item (0)->nodeValue;
			$this->WindSpeedAverageUnits		= $dxpResponse->Query ("aws:wind-speed-avg", $dxpObservation)->item (0)->getAttribute ("units");
			$this->WindDirection				= $dxpResponse->Query ("aws:wind-direction", $dxpObservation)->item (0)->nodeValue;
			$this->WindDirectionAverage			= $dxpResponse->Query ("aws:wind-direction-avg", $dxpObservation)->item (0)->nodeValue;
		}
	}
	
?>