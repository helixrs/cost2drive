<?php
	
	/************************************************************************
	 * WeatherBugStation.class.php
	 *
	 * @author		Bashkim Isai (me@bashkim.com.au)
	 * @licence		Creative Commons Attribution 3.0 Unported License
	 * @licence		http://creativecommons.org/licenses/by/3.0/
	 *
	 * @language	php
	 *
	 * @description
	 * Contains the classes required for storing and saving Station responses
	 *
	 * This code is released under the Creative Commons Attribution 3.0 Unported 
	 * License. You are free to use this code provided that your use or 
	 * implementation does not deminish the rights of the author or the licence. 
	 * Please attribute this original work in any remixes that you make and email 
	 * me (bash [at] phpoblib [dot] com) to let me know if you're using this code. 
	 * It's always good to hear when your work is helping others. 
	 *
	 * Proudly made in Australia
	 *
	 ***********************************************************************/
	
	/************************************************************************
	 * @class		WeatherBugStations
	 *
	 * @description
	 * Used as an array for storing a list of stations
	 *
	 ***********************************************************************/
	
	class WeatherBugStations
	{
		
		public $APIVersion;
		public $Stations;
		
		function __construct (DOMDocument $domResponse)
		{
			$dxpResponse = new DOMXPath ($domResponse);
			$dxpResponse->registerNamespace ("aws", "http://www.aws.com/aws");
			
			$this->APIVersion	= $dxpResponse->Query ("/aws:weather/aws:api")->item (0)->getAttribute ('version');
			$this->Stations		= Array ();
			
			$dnlStation = $dxpResponse->Query ("/aws:weather/aws:stations/aws:station");
			foreach ($dnlStation as $dnoStation)
			{
				$this->Stations [] = new WeatherBugStation ($dnoStation);
			}
		}
	}
	
	/************************************************************************
	 * @class		WeatherBugStation
	 *
	 * @description
	 * Contains information about a singular station from a response
	 *
	 ***********************************************************************/
	
	class WeatherBugStation
	{
		
		public $ID;
		public $Name;
		public $City;
		public $Country;
		public $CityCode;
		public $Latitude;
		public $Longitude;
		
		function __construct ($dnoLocation)
		{
			$this->ID			= $dnoLocation->getAttribute ('id');
			$this->Name			= $dnoLocation->getAttribute ('name');
			$this->City			= $dnoLocation->getAttribute ('city');
			$this->Country		= $dnoLocation->getAttribute ('country');
			$this->CityCode		= $dnoLocation->getAttribute ('citycode');
			$this->Latitude		= $dnoLocation->getAttribute ('latitude');
			$this->Longitude	= $dnoLocation->getAttribute ('longitude');
		}
	}
	
?>