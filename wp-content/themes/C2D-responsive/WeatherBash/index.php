<?php
	
	/************************************************************************
	 * index.php
	 *
	 * @author		Bashkim Isai (me@bashkim.com.au)
	 * @licence		Creative Commons Attribution 3.0 Unported License
	 * @licence		http://creativecommons.org/licenses/by/3.0/
	 *
	 * @language	php
	 *
	 * @see			WeatherBug.class.php
	 * @see			WeatherBugForecast.class.php
	 * @see			WeatherBugForecastDate.class.php
	 * @see			WeatherBugLiveWeather.class.php
	 *
	 * @description
	 * This is an example file which shows an example of how the "Weather Bash"
	 * wrapper works
	 *
	 * This code is released under the Creative Commons Attribution 3.0 Unported 
	 * License. You are free to use this code provided that your use or 
	 * implementation does not deminish the rights of the author or the licence. 
	 * Please attribute this original work in any remixes that you make and email 
	 * me (bash [at] phpoblib [dot] com) to let me know if you're using this code. 
	 * It's always good to hear when your work is helping others. 
	 *
	 * Proudly made in Australia
	 *
	 ***********************************************************************/
	
	require_once ("WeatherBug.class.php");
	require_once ("WeatherBugLiveWeather.class.php");
	
	// For the code example on this page, we will use the Live Weather
	// from Los Angeles, CA
	
	$WeatherBug			= new WeatherBug;
	$WeatherResponse	= $WeatherBug->GetLiveWeatherByZipCode (90210);
	
	// The code below shows examples of other ways you can implement this system
//	$WeatherResponse	= $WeatherBug->GetForecastByLatLong (-27.5, 153.0);
//	$WeatherResponse	= $WeatherBug->GetForecastByCityCode (50391);
//	$WeatherResponse	= $WeatherBug->GetForecastByZipCode (90210);
//	$WeatherResponse	= $WeatherBug->GetLiveWeatherByZipCode (90210);
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>WeatherBug Results</title>
		<link rel="stylesheet" type="text/css" href="style.css" />
	</head>
	<body>
		<div id="Weather-Container">
			<div id="Weather-Content">
				<h1 id="Weather-CityState">
					<a href="http://maps.google.com/maps?q=<?php echo $WeatherResponse->Latitude ?>,<?php echo $WeatherResponse->Longitude ?>">
						<?php echo $WeatherResponse->CityState ?>
					</a>
				</h1>
				
				<div id="Weather-Station">
					<small>(<?php echo $WeatherResponse->Station ?>)</small>
				</div>
				
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<th>Temperature (Now)</th>
						<td><?php echo $WeatherResponse->Temperature ?><?php echo $WeatherResponse->TemperatureUnits ?></td>
					</tr>
					<tr>
						<th>Temperature (High)</th>
						<td><?php echo $WeatherResponse->TemperatureHigh ?><?php echo $WeatherResponse->TemperatureHighUnits ?></td>
					</tr>
					<tr>
						<th>Temperature (Low)</th>
						<td><?php echo $WeatherResponse->TemperatureLow ?><?php echo $WeatherResponse->TemperatureLowUnits ?></td>
					</tr>
					<tr>
						<th>Sunrise</th>
						<td><?php echo date ("g:ia", $WeatherResponse->Sunrise) ?></td>
					</tr>
					<tr>
						<th>Sunset</th>
						<td><?php echo date ("g:ia", $WeatherResponse->Sunset) ?></td>
					</tr>
				</table>
				
				<p>
					<img src="<?php echo $WeatherResponse->CurrentConditionIcon ?>" id="Weather-CurrentConditionsIcon" />
					<?php echo $WeatherResponse->CurrentConditionDescription ?>
				</p>
			</div>
		</div>
		
		<div id="Description-Container">
			<div id="Description-Content">
				<h1>Weather Bash</h1>
				<small>Written by <a href="http://www.bashkim.com.au/">Bashkim Isai</a> &copy; 2008, <a href="documentation.html#Heading-Licence">Some Rights Reserved</a>.</small>
				
				<p>
					Here is a quick example of how you can include this weather API in your own applications.
					This receives weather information from the <a href="http://api.wxbug.net/">Weather Bug API</a>
					and processes all the information you requested, allowing you to output the information in a
					friendly, easy-to-use form.
				</p>
				
				<p>
					You can extend this example or use the <em>wrapper</em> provided to display the information
					which you find most useful on your own website. Examples of how to retrieve information from
					Weather Bug are included below.
				</p>
				
				<p>
					<em>
						Also see <a href="documentation.html">Project Documentation</a>.
					</em>
				</p>
			</div>
		</div>
		
		<div class="Clear"></div>
	</body>
</html>