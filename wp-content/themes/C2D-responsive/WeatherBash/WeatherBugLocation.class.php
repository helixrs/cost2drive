<?php
	
	/************************************************************************
	 * WeatherBugLocation.class.php
	 *
	 * @author		Bashkim Isai (me@bashkim.com.au)
	 * @licence		Creative Commons Attribution 3.0 Unported License
	 * @licence		http://creativecommons.org/licenses/by/3.0/
	 *
	 * @language	php
	 *
	 * @description
	 * Contains the classes required for storing and saving Location responses
	 *
	 * This code is released under the Creative Commons Attribution 3.0 Unported 
	 * License. You are free to use this code provided that your use or 
	 * implementation does not deminish the rights of the author or the licence. 
	 * Please attribute this original work in any remixes that you make and email 
	 * me (bash [at] phpoblib [dot] com) to let me know if you're using this code. 
	 * It's always good to hear when your work is helping others. 
	 *
	 * Proudly made in Australia
	 *
	 ***********************************************************************/
	
	/************************************************************************
	 * @class		WeatherBugLocations
	 *
	 * @description
	 * Used as an array for storing a list of locations
	 *
	 ***********************************************************************/
	
	class WeatherBugLocations
	{
		
		public $APIVersion;
		public $Locations;
		
		function __construct (DOMDocument $domResponse)
		{
			$dxpResponse = new DOMXPath ($domResponse);
			$dxpResponse->registerNamespace ("aws", "http://www.aws.com/aws");
			
			$this->APIVersion	= $dxpResponse->Query ("/aws:weather/aws:api")->item (0)->getAttribute ('version');
			$this->Locations	= Array ();
			
			$dnlLocation = $dxpResponse->Query ("/aws:weather/aws:locations/aws:location");
			foreach ($dnlLocation as $dnoLocation)
			{
				$this->Locations [] = new WeatherBugLocation ($dnoLocation);
			}
		}
	}
	
	/************************************************************************
	 * @class		WeatherBugLocation
	 *
	 * @description
	 * Contains information about a singular location from a response
	 *
	 ***********************************************************************/
	
	class WeatherBugLocation
	{
		
		public $CityName;
		public $StateName;
		public $CountryName;
		public $ZipCode;
		public $CityCode;
		public $isOutsideUS;
		
		function __construct ($dnoLocation)
		{
			$this->CityName			= $dnoLocation->getAttribute ('cityname');
			$this->StateName		= $dnoLocation->getAttribute ('statename');
			$this->CountryName		= $dnoLocation->getAttribute ('countryname');
			$this->ZipCode			= $dnoLocation->getAttribute ('zipcode');
			$this->CityCode			= $dnoLocation->getAttribute ('citycode');
			$this->isOutsideUS		= $dnoLocation->getAttribute ('citytype');
		}
	}
	
?>