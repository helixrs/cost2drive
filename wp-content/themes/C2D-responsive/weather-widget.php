<?php
/**
 * Weather Widget appearing in page content
 */
require_once ("../WeatherBash/WeatherBug.class.php");
require_once ("../WeatherBash/WeatherBugLiveWeather.class.php");
require_once ("../WeatherBash/WeatherBugForecast.class.php");

$WeatherBug = new WeatherBug;

$end_nend_location_nameame = $_POST['end_location_name'];
$lat = $_POST['lat'];
$lng = $_POST['lng'];

$WeatherResponse = $WeatherBug->GetLiveWeatherByLatLong ($lat, $lng);
$ForecastResponse = $WeatherBug->GetForecastByLatLong ($lat, $lng);

?>
<div class="weather-box">
	<h2 id="weather-heading">
		<span>Weather</span>
	</h2>
	<p class="weather-in">Weather in <?php print end_location_name; ?></p>

	<div class="weather-box-content">
		<div class="weather-today">
			<h3>Currently</h3>
			<img src="<?php print $WeatherResponse->CurrentConditionIcon; ?>" alt="" />
			<div class="today-weather-data">
				<p class="current-condition"><?php print $WeatherResponse->Temperature; ?>&deg;F</p>
				<p class="current-weather-desc"><?php print $WeatherResponse->CurrentConditionDescription; ?></p>
				<p>Humidity: <?php print $WeatherResponse->Humidity; ?>%</p>
				<p>Wind: <?php print $WeatherResponse->WindSpeed; ?>mph</p>
			</div>
		</div>

		<div class="weather-forecast">
			<h3>5 Day Forecast</h3>
			<ul>
			  <?php $i = 1; ?>
			  <?php foreach ($ForecastResponse->Forecasts as $Forecast) : ?>
				<li>
					<p class="forecast-date"><?php print $Forecast->TitleALT; ?></p>
					<img src="<?php print $Forecast->ImageSource; ?>" alt="" />
					<?php $prediction = preg_replace('/(.+?)Chance/', '', $Forecast->ShortPrediction) ?>
					<p class="weather-desc"><?php print preg_replace('/(.+?)of/', '', $prediction); ?></p>
					<p class="max-temp"><?php print $Forecast->HighValue; ?>&deg;F</p>
					<p class="min-temp"><?php print $Forecast->LowValue; ?>&deg;F</p>
				</li>
			  <?php if ($i == 5) break; $i++; ?>
			  <?php endforeach; ?>
			</ul>
		</div>
		<p class="credits">Powered by <a href="http://www.weatherbug.com" target="_blank">WeatherBug</a></p>
	</div>
</div>
