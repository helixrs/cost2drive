<?php
/**
 * Top Beaches
 */

get_header(); ?>

		<div id="container" class="dest-category">
			<div id="content" role="main">

			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			//get_template_part( 'loop', 'page' );
			?>

				<div class="page type-page hentry">
					<h1 class="entry-title">Popular Destinations</h1>
					<div class="entry-content">
						<div class="intro-line"><p><?php print $post->post_content; ?></p></div>
						<h2><?php the_title(); ?></h2>

						<?php
						// print destinations by category
						destinations_list_by_category($category=285);
						?>

					</div><!-- .entry-content -->
				</div>

			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
