<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage cost_to_drive
 * @since Cost to Drive 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="apple-itunes-app" content="app-id=909952082">
<meta name="google-play-app" content="app-id=com.tollsmart.leonidiogansen.tolls">
<script type="text/javascript">var NREUMQ=NREUMQ||[];NREUMQ.push(["mark","firstbyte",new Date().getTime()]);</script>
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	// Add the blog name.
	bloginfo( 'name' );

	wp_title( '|', true, 'left' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'costtodrive' ), max( $paged, $page ) );

	?></title>
<meta property="og:title" content="<?php
	bloginfo( 'name' );
	wp_title( '|', true, 'left' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
?>" />
<meta property="og:image" content="<?php  print bloginfo('template_url'); ?>/images/structural/og-logo.png" />
<meta property="og:url" content="http://<?php print  $_SERVER["SERVER_NAME"]; ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_bloginfo('template_url') . "/jquery-ui.css" ?>" />
<link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="image_src" href="/wp-content/themes/C2D/images/structural/logo.png" />
<?php
// load js files here
    if (!is_admin()) {
        wp_enqueue_script('jquery');
        wp_enqueue_script('js_fancybox', get_bloginfo('template_url') . '/jquery.fancybox-1.3.4.js', array('jquery'), '1.0');
        wp_enqueue_script('js_bootstrap', get_bloginfo('template_url') . '/bootstrap.min.js', array('jquery'), '1.0');
		//wp_enqueue_script('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js', array('jquery'), '1.8.6');
        wp_enqueue_script('jquery-ui', get_bloginfo('template_url') . '/jquery-ui.js', array('jquery'), '1.0');
        wp_enqueue_script('js_files', get_bloginfo('template_url') . '/main.js', array('jquery'), '1.0');
        // wp_enqueue_script('input-placeholder', get_bloginfo('template_url') . '/app/jquery.inputplaceholder.js', array('jquery'), '1.0');
        // wp_enqueue_script('input-placeholder', get_bloginfo('template_url') . '/app/jquery.bgiframe.js', array('jquery'), '1.0');
        // wp_enqueue_script('input-placeholder', get_bloginfo('template_url') . '/app/ui.js', array('jquery'), '1.0');
    }
?>
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing 
</head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<?php //<script src="http://widgets.twimg.com/j/2/widget.js"></script> ?>
<script type="text/javascript">stLight.options({publisher:'242135f6-62bd-47d6-93f5-b37899a6956c'});</script>
<?php if ( is_home() || is_front_page() ): ?>
<meta name="google-site-verification" content="lgbbuT8vFgE0oYbqzpCm8nQMuJwFrfRaFX5cm_9mVBc" />
<?php endif; ?>

</head>

<body <?php body_class(); ?>>
<div class="sr-only"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'costtodrive' ); ?>"><?php _e( 'Skip to content', 'costtodrive' ); ?></a></div>
<div id="wrapper" class="hfeed">
	<div id="header">

		<div id="masthead" class="container">
			<div id="branding" role="banner" class="clearfix">
				<div id="site-description"><?php bloginfo( 'description' ); ?></div>

				<?php
					// Check if this is a post or page, if it has a thumbnail, and if it's a big one
					if ( is_singular() && current_theme_supports( 'post-thumbnails' ) &&
							has_post_thumbnail( $post->ID ) &&
							( /* $src, $width, $height */ $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnail' ) ) &&
							$image[1] >= HEADER_IMAGE_WIDTH ) :
						// Houston, we have a new header image!
						echo get_the_post_thumbnail( $post->ID );
					elseif ( get_header_image() ) : ?>
						<img src="<?php header_image(); ?>" width="<?php echo HEADER_IMAGE_WIDTH; ?>" height="<?php echo HEADER_IMAGE_HEIGHT; ?>" alt="" />
					<?php endif; ?>
			</div><!-- #branding -->
			<div id="login-register">
				<?php if ( is_user_logged_in() ) {
					 global $current_user;
                    wp_get_current_user(); ?>
					 <span>Welcome back, <?php echo $current_user->nickname; ?>!</span> <?php
				} else {
				?>  <!--  <a href="<?php //echo wp_login_url( get_permalink() ); ?>" title="Login">Login</a> or <?php //wp_register('', ''); ?> --> <?php
					}
				?>

			</div>

            <nav class="navbar navbar-default" role="navigation">
              <div class="container-fluid clearfix">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="/">
                    <img src="/wp-content/themes/C2D/images/structural/logo.png" alt="Cost To Drive" />
                    <img class="ipad-logo" src="/wp-content/themes/C2D-responsive/images/structural/ipad-logo.png" alt="Cost To Drive" />
                  </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                  <?php
                    $custom_menu = array(
                        'theme_location'  => '',
                        'menu'            => 'main-nav',
                        'container'       => '',
                        'container_class' => '',
                        'container_id'    => '',
                        'menu_class'      => 'menu nav navbar-nav',
                        'menu_id'         => '',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'before'          => '',
                        'after'           => '',
                        'link_before'     => '',
                        'link_after'      => '',
                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth'           => 0,
                        'walker'          => ''
                    );
                    wp_nav_menu( $custom_menu );
                  ?>
                </div>
              </div><!-- /.container-fluid -->
            </nav>

		</div><!-- #masthead -->
	</div><!-- #header -->

	<div id="main" class="container">
