<?php

	//error_reporting(E_ALL);

	// ?start=Chicago%20Ilinois&end=Washington%20DC&passengers=1&carId=14522
	//		start=Start Address
	//		end=End Address
	//		passengers=NUMBER
	//		carId=NUMBER
	// ?start=Chicago%20Ilinois&end=Washington%20DC&passengers=1&custom=true&mpg=30&gpt=10&fuelType=Regular
	//		if custom=true (false by default):
	//			mpg=NUMBER - Miles Per Gallon
	//			gpt=NUMBER - Tank Size (Gallons)
	//			fuelType=Regular,Premium,Diesel
	// ?start=Chicago%20Ilinois&end=washington%20DC&waypoints[]=Indianapolis%20IN&waypoints[]=Lexington%20KY&passengers=1&carId=16326
	//		waypoints[]=Waypoint Address 1
	//		waypoints[]=Waypoint Address 2
	//		...
	//		waypoints[]=Waypoint Address N

	// The result contains:
	// End Point formatted name
	// Start Point latitude and longitude
	// End Point formatted name
	// End Point latitude and longitude
	// Points - an array of the poliline [lat,lng] points for placing on the map
	// Car Name
	// Trip Cost ($)
	// Cost Per Passenger (provided if passengers>1)
	// Distance (ml)
	// Gas Stops - objects with all geo data
	// Waypoints - an array of objects containing formatted addres, latitude and longitude


$keys = array(
	'xNXvVkQY9sibA9953du2', // test
	'MLzakZMz2asqQC2NQqry', // aarp
	'ntH8fdop7KJq3Pjf654v', // webelinks
	'o03rW6npSA23mi8V89jc'  // trial
);

if (isset($_GET["key"]) && in_array($_GET["key"], $keys) ) {

	$key = $_GET["key"];

	if (isset($_GET["start"]) && $_GET["start"] != '' && isset($_GET["end"]) && $_GET["end"] != '') {

		$get_start = $_GET["start"];
		$get_end =   $_GET["end"];

		if (isset($_GET["passengers"]) && $_GET["passengers"] != '') {
			$get_passengers = $_GET["passengers"];
		} else {
			$get_passengers = 1;
		}

		if (isset($_GET["waypoints"])) {
			$get_waypoints = $_GET["waypoints"];
		}

		if (isset($_GET["carId"])) {
			$get_car_id = $_GET["carId"];
		}

		if (isset($_GET["custom"])) {
			$get_custom = ($_GET["custom"] == 'true') ? true : false;
		}
		if (isset($_GET["mpg"])) {
			$get_mpg = $_GET["mpg"];
		}
		if (isset($_GET["gpt"])) {
			$get_gpt = $_GET["gpt"];
		}
		if (isset($_GET["fuelType"])) {
			$get_fuel_type = $_GET["fuelType"];
		}


		include("app/include/globals.php");
		include("app/classes/object-trip.php");


		if ($get_custom==false && isset($get_car_id) && $get_car_id!="")
		{
			$carId = $get_car_id;
		}
		else
		{
			if (isset($get_mpg) && $get_mpg!="")
			{
				$carId=-1;
			}
			else
			{
				$carId = 15395;
			}
		}
		$car = new Car($carId);
		if ($get_custom==true && isset($get_mpg) && $get_mpg!="")
		{
			$car->setCustom($get_fuel_type,$get_mpg,$get_gpt);
			$carName = "Custom Car";
		}
		else
		{
			$carName = $car->year." ".$car->mfr." ".$car->make;
		}

		$trip = new Trip($car,$get_start,$get_end,$get_waypoints,$get_passengers);

		$trip->calculateCost();
		//~$trip->getKayak();


		// #########################################################


		$output = array();

		$output['startAddress'] = $trip->start;
		$output['startPoint'] = $trip->startPoint;
		$output['endAddress'] = $trip->end;
		$output['endPoint'] = $trip->endPoint;

		//~$output['waypoints'] = $trip->waypointsList;

		$output['waypoints'] = array();
		$count_waypoints = count($trip->waypointsList);
		if ($count_waypoints > 0) {
			for ($i = 0; $i < $count_waypoints-1; $i++)	{
				$output['waypoints'][$i]['waypointAddress'] = $trip->waypointsList[$i];
				$output['waypoints'][$i]['waypointPoint'] = $trip->waypointsListPoints[$i];
			}
		}

		$output['gasStops'] = $trip->stops;
		$output['car'] = ucwords($carName);



		// cost
		$output['cost'] =  ($trip->cost > 0) ? number_format($trip->cost,2) : 'Unavailable';

		if ($trip->passengers > 1 ) {
			$output['costPerPassenger'] =  'The cost per passenger: $'.$trip->getCostPerPassenger();
		}

		// distance
		$output['distance'] =  number_format($trip->distance,2). 'ml';
		// driving time
		$output['drivingTime'] =  $trip->getDrivingTime();

		// polyline points
		$output['points'] = $trip->points;

		// cost details
		$output['totalGallons'] = number_format($trip->totalGallons,1);
		$output['averageGasPrice'] = number_format(($trip->cost / $trip->totalGallons),2);



		//~print '<pre>';
		//~print_r($output);
		//~print '</pre>';

		print json_encode($output);

	}

} else {
	print 'You are not authorized to access this page';
	$key = 0;
}



/* Google Analytics */

$var_utmac='UA-36779256-1'; //enter the new urchin code
$var_utmhn='beta.costtodrive.com'; //enter your domain
$var_utmn=rand(1000000000,9999999999); //random request number
$var_cookie=rand(10000000,99999999); //random cookie number
$var_random=rand(1000000000,2147483647); //number under 2147483647
$var_today=time(); //today
$var_referer=$_SERVER['HTTP_REFERER']; //referer url

$var_uservar=$key; //enter your own user defined variable
$var_utmp='tracker/'.$_SERVER['QUERY_STRING'].'.'.$_GET['filetype']; //this example adds a fake file request to the (fake) tracker directory (the image/pdf filename).

$urchinUrl='http://www.google-analytics.com/__utm.gif?utmwv=1&utmn='.$var_utmn.'&utmsr=-&utmsc=-&utmul=-&utmje=0&utmfl=-&utmdt=-&utmhn='.$var_utmhn.'&utmr='.$var_referer.'&utmp='.$var_utmp.'&utmac='.$var_utmac.'&utmcc=__utma%3D'.$var_cookie.'.'.$var_random.'.'.$var_today.'.'.$var_today.'.'.$var_today.'.2%3B%2B__utmb%3D'.$var_cookie.'%3B%2B__utmc%3D'.$var_cookie.'%3B%2B__utmz%3D'.$var_cookie.'.'.$var_today.'.2.2.utmccn%3D(direct)%7Cutmcsr%3D(direct)%7Cutmcmd%3D(none)%3B%2B__utmv%3D'.$var_cookie.'.'.$var_uservar.'%3B';

$handle = fopen ($urchinUrl, "r");
$test = fgets($handle);
fclose($handle);

?>
