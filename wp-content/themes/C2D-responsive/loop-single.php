<?php
/**
 * The loop that displays a single post.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop-single.php.
 *
 * @package WordPress
 * @subpackage cost_to_drive
 * @since Cost to Drive 1.2
 */
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1 class="entry-title"><?php the_title(); ?></h1>

					<div class="cdd-post-date">
						<?php costtodrive_posted_on(); ?>
					</div><!-- .entry-meta -->

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'costtodrive' ), 'after' => '</div>' ) ); ?>
					</div><!-- .entry-content -->

					<div class="entry-utility">
						<?php costtodrive_posted_in(); ?>
						<?php edit_post_link( __( 'Edit', 'costtodrive' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-utility -->

  				<?php comments_template( '', true ); ?>
				</div><!-- #post-## -->

<?php endwhile; // end of the loop. ?>