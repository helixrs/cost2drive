<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage cost_to_drive
 * @since Cost to Drive 1.0
 */

get_header(); ?>

	<div class="row">
		<div id="container" class="col-md-8">
			<div id="content" role="main">

      <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

      				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    						<h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
      					<div class="entry-content">
        					<div class="cdd-post-date">
        						<?php costtodrive_posted_on(); ?>
        					</div><!-- .entry-meta -->
                  <?php the_excerpt(); ?>
      						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'costtodrive' ), 'after' => '</div>' ) ); ?>
      						<?php edit_post_link( __( 'Edit', 'costtodrive' ), '<span class="edit-link">', '</span>' ); ?>
      					</div><!-- .entry-content -->
      				</div><!-- #post-## -->

      				<?php comments_template( '', true ); ?>

      <?php endwhile; // end of the loop. ?>

			<?php kriesi_pagination($pages = '', $range = 2); ?>
			<?php //kriesi_pagination(); ?>
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
	</div>
<?php get_footer(); ?>
