<?php
/**
 * The template for displaying c2d widget.
 *
 * @package WordPress
 * @subpackage cost_to_drive
 * @since Cost to Drive 1.0
 */
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
include("app/include/globals.php");
include("app/classes/object-car.php");

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="UTF-8" />
<link rel="stylesheet" type="text/css" media="all" href="http://<?php print $_SERVER['SERVER_NAME']; ?>/wp-content/themes/C2D/style-widget.css" />
<script type='text/javascript' src='http://<?php print $_SERVER['SERVER_NAME']; ?>/wp-includes/js/jquery/jquery.js?ver=1.6.1'></script>
<script type='text/javascript' src='http://<?php print $_SERVER['SERVER_NAME']; ?>/wp-content/themes/C2D/widget-main2.js?ver=1.0'></script>
</head>

<body>

	<div class="c2d_widget c2d_widget_<?php print (isset($_GET['wid'])) ? $_GET['wid'] : 1; ?>">
		<form id="where_to_f"  action="http://<?php print $_SERVER['SERVER_NAME']; ?>/from" method="get">
			<?php include("app/include/widget-trip-form.php");?>
			<?php include("app/include/widget-car-form.php");?>
			<input class="my_c2d_btn" type="submit" value="s" />
		</form>
		<?php include("app/include/widget-car-script.php"); ?>
	</div>


</body>

</html>
