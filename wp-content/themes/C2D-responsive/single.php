<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage cost_to_drive
 * @since Cost to Drive 1.0
 */

get_header(); ?>
	<div class="row">
		<div id="container" class="col-md-8">
			<div id="content" role="main">

			<?php
			/* Run the loop to output the post.
			 * If you want to overload this in a child theme then include a file
			 * called loop-single.php and that will be used instead.
			 */
			get_template_part( 'loop', 'single' );
			?>

			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
	</div><!-- /row -->
<?php get_footer(); ?>
