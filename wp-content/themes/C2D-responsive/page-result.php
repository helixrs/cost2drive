<?php
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
/**
 * The template for displaying results page.
 *
 * @package WordPress
 * @subpackage cost_to_drive
 * @since Cost to Drive 1.0
 */

	
	//seconds * minutes * hours * days + current time
	$inTwoMonths = 60 * 60 * 24 * 60 + time(); 
	setcookie('start', $_GET["start"], $inTwoMonths, "/", ".costtodrive.com"); 
	setcookie('end', $_GET["end"], $inTwoMonths, "/", ".costtodrive.com"); 
	setcookie('carId', $_GET["carId"], $inTwoMonths, "/", ".costtodrive.com"); 

	include("app/include/globals.php"); 
	include("app/classes/object-trip.php");
	include("app/classes/object-google-maps.php");
	//Calculate 60 days in the futures
	if ($_GET["custom"]=="false" && isset($_GET["carId"]) && $_GET["carId"]!="")
	{
		$carId = $_GET["carId"];
	}
	else
	{
		if (isset($_GET["mpg"]) && $_GET["mpg"]!="")
		{
			$carId=-1;
		}
		else
		{
			$carId = 15395;
		}
	}
	$car = new Car($carId);
	if ($_GET["custom"]=="true" && isset($_GET["mpg"]) && $_GET["mpg"]!="")
	{
		$car->setCustom($_GET["fuelType"],$_GET["mpg"],$_GET["gpt"]);
		$carName = "Custom Car";
	}
	else
	{
		$carName = $car->year." ".$car->mfr." ".$car->make; 
	}
	
	$trip = new Trip($car,$_GET["start"],$_GET["end"],$_GET["waypoints"],$_GET["passengers"]);
	$trip->calculateCost();
	$trip->getKayak();
	$map = new C2DMap($trip);

	get_header();
?>
<?php 
	$map->printGoogleJS(); 
	$map->addAddress($trip->start,$trip->startPoint);
	$map->addAddress($trip->end,$trip->endPoint);
	$map->polyline=$trip->polyline;
	
	$map->showMap();
?>

		<div id="container">
			<div id="content" role="main">
				<div class="wide-box page">

					<div id="your_trip_info">
						<h2 id="your_trip"><span>Your Trip</span></h2>
						<a class="expand-et" href="#inline">Edit Trip</a>
						<a class="collapse-et" href="#">Cancel</a>

						<div class="trip_info">
													<p><?php echo $trip->start; ?> to <?php echo $trip->end; ?>, in <?php echo $carName; ?></p>
						</div>
					</div>

					<div id="edit-trip-form">
						<!-- map breaks when this is enabled -->
						<form id="where_to_f"  action="http://www.costtodrive.com/beta/result" method="get">
							<div><?php  include("app/include/trip-form.php");?></div>
							<div><?php  include("app/include/car-form.php");?></div>
							<input class="my_c2d_btn" type="image" src="<?php echo get_bloginfo('template_url'); ?>/images/structural/c2d-btn.gif" alt="Calculate my cost to drive" />
						</form>
						<div><?php include("app/include/car-script.php"); ?></div>
					</div> <!-- edit-trip-form -->

					<div id="your_cost_info">
						<h2 id="your_cost"><span>Your Cost To Drive Is</span></h2>
						<span id="cost">$<?php echo number_format($trip->cost,2); ?></span>
						<?php 
							if ($trip->passengers > 1 ) {
								echo '<div class="trip_info"><p>The cost per passenger: $'.$trip->getCostPerPassenger().'</p></div>';
							}
						?>
						<span style="float: left; clear: left; font-family: Arial, Verdana, sans-serif; font-size: 15px; color: #7d7d7d;">Total Distance:</span><span style="float: left; padding-left: 30px; font-family: Arial, Verdana, sans-serif; font-size: 15px; color: #7d7d7d;"><?php echo number_format($trip->distance,2);?> miles </span>
						<span style="float: left; clear: left; font-family: Arial, Verdana, sans-serif; font-size: 15px; color: #7d7d7d;">Driving Time:</span><span style="float: left; padding-left: 30px; font-family: Arial, Verdana, sans-serif; font-size: 15px; color: #7d7d7d;"><?php echo $trip->getDrivingTime();?></span>
						<?php if ($trip->kayakPrice != 0) { ?>
						 <span style="float: left; clear: left; font-family: Arial, Verdana, sans-serif; font-size: 15px; color: #7d7d7d;"><a target="_blank" href="<?php echo $trip->kayakUrl;?>">Fly for $<?php echo $trip->kayakPrice;?></a></span>
						<?php } ?>
					</div>

					<div id="map">
						<div class="map-positioner">
							<div id="map_canvas" style="width: 646px; height: 360px;"></div>
						</div>
					</div>

					<div id="complete_directions">
						<p>Get complete directions from:</p>
						<ul>
							<li><a target="_blank" href="<?php echo $trip->googleURL;?>">Google Maps</a></li>
						</ul>
					</div>

		
					<?php
						//$end_location = $trip->end;
						$end_location = $get_end_readable;
						
						if (!empty($end_location)) {					
							$query = sprintf("SELECT geo_id FROM geos_id_cache WHERE geo_name='%s' AND geo_id<>0 LIMIT 1",
											mysql_real_escape_string($end_location));					
							$row = $wpdb->get_row($query);
							$taid_is_ok = false;
					
							if ($row != null) {								
								$taid = $row->geo_id;
								if (is_numeric($taid) && $taid > 0) {
									$taid_is_ok = true;
								}								
								//print '<p>#from cache: '.$taid.'#</p>';								
							} else {					
								$contents = file_get_contents('https://www.google.com/search?q=tripadvisor+hotels+'.urlencode($end_location));
								$pattern = '/http:\/\/www\.tripadvisor(.*?)-g([0-9]+)/';					
								preg_match($pattern, $contents, $matches);					
								$taid = $matches[2];
								if (is_numeric($taid) && $taid > 0) {
									if ($wpdb->insert('geos_id_cache', array('geo_id' => $taid, 'geo_name' => $end_location), array('%d', '%s'))) {
										$taid_is_ok = true;
										//print '<p>#write into db#</p>';
									}
								}								
								//print '<p>#live: '.$taid.'#</p>';								
							}
						}
					?>
					
				<?php if ($taid_is_ok) : ?>
					<div class="destination-recommendations">
						<input type="hidden" name="taid-value" value="<?php print $taid; ?>" id="taid-value" />
						<h2 id="dest-recommend">New! <span>Destination Recommendations for <?php echo $trip->end; ?></span></h2>
						<div class="dest-recommend-content-wrap">
							<div class="dest-recommend-content">
								<img class="spiner" src="http://www.costtodrive.com/beta/wp-content/themes/C2D/images/ajax-loader.gif" alt="loading..." title="" />
							</div>
						</div>
					</div>
				<?php endif; ?>


			<div id="tech-details">
						<h2 id="tenical_details_heading"><span>Details</span></h2>
						<a class="expand" href="#">Expand</a>
						<a class="collapse" href="#">Collapse</a>

						<div class="tech-details-content">
							<div class="trip_info">
								<ul id="prices">
									<li id="gasUsed"><span>Fuel &rsaquo;</span> <?php echo number_format($trip->totalGallons,1); ?> Gallons</li>
									<li id="avgPrice"><span>Average Gas Price &rsaquo;</span> $<?php echo number_format(($trip->cost / $trip->totalGallons),2);?></li>
<?php
	// $tolls = $trip->getTolls();
	// 
	// if (count($tolls) > 0)
	// {
	// 	echo "<li>The toll roads include the following: <br>";
	// 	foreach($tolls as $toll)
	// 	{
	// 		print_r($toll);
	// 	}
	// 	echo "</li>";
	// }
?>
								</ul>
							</div>

							<div id="tech_details">
								<table id="prices_table">
									<tbody>
										<tr id="prices_row">
											<th></th>
											<th>Miles Driven</th>
											<th>Gallons</th>
											<th>Gas Price</th>
											<th>Cost</th>
										</tr>
										<!-- <td><a onmouseover="GEvent.trigger(markers[0],&quot;click&quot;)" href="#">New York Co., NY</a></td>
										<td><a onmouseover="GEvent.trigger(markers[1],&quot;click&quot;)" href="#">Jefferson Co., PA</a></td> -->
<?php
	foreach($trip->legs as $leg)
	{
		echo "<tr>\n";
		echo "<td>".$leg->location."</td>\n";
		echo "<td>".number_format($leg->distance,1)."</td>\n";
		echo "<td>".number_format($leg->gallons,1)."</td>\n";
		echo "<td>".number_format($leg->gasPrice,2)."</td>\n";
		echo "<td>".number_format($leg->legCost,2)."</td>\n";
		echo "</tr>\n";
	}
?>
									</tbody>
								</table>
							</div>
							<div id="total">
								<ul>
									<li id="mini_cost"><span>Total Cost</span> $<?php echo number_format($trip->cost,2); ?></li>
								</ul>
							</div>
		</div><!-- /tech-details-content -->
					</div><!-- /details -->	


				</div><!-- /wide-box -->
			</div><!-- #content -->
		</div><!-- #container -->

		<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function($) {
				initialize();
			});
		</script>

<?php get_sidebar(); ?>
<?php get_footer(); ?>