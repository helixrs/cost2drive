<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage cost_to_drive
 * @since Cost to Drive 1.0
 */

get_header(); ?>

		<div id="container">
			<div id="content" role="main">
				<div class="page type-page">
					<h1 class="entry-title">Contact Us</h1>	
					<div class="entry-content">
						<?php
						/* Run the loop to output the page.
						 * If you want to overload this in a child theme then include a file
						 * called loop-page.php and that will be used instead.
						get_template_part( 'loop', 'page' );
						 */
						?>

						<script type="text/javascript" charset="utf-8">
						  var is_ssl = ("https:" == document.location.protocol);
						  var asset_host = is_ssl ? "https://s3.amazonaws.com/getsatisfaction.com/" : "http://s3.amazonaws.com/getsatisfaction.com/";
						  document.write(unescape("%3Cscript src='" + asset_host + "javascripts/feedback-v2.js' type='text/javascript'%3E%3C/script%3E"));
						</script>

						<script type="text/javascript" charset="utf-8">
						  var feedback_widget_options = {};

						  feedback_widget_options.display = "inline";  
						  feedback_widget_options.company = "c2g";
						  
						  feedback_widget_options.style = "idea";

						  var feedback_widget = new GSFN.feedback_widget(feedback_widget_options);
						</script>
					</div><!-- .entry-content -->
				</div>
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
