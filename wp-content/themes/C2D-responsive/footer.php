<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage cost_to_drive
 * @since Cost to Drive 1.0
 */
?>
	<div class="clb"></div>
	</div><!-- #main -->

	<div id="footer" role="contentinfo">

	<div class="footer-links-container container">
		<div class="">
			<div class="footer-us-tools col-md-3 col-sm-6">
				<div class="row">
					<div class="col-md-12"><h2>US TOLLS</h2></div>
					<ul class="col-sm-6">
						<li><a href="http://beta.costtodrive.com/california-tolls/">California Tolls</a></li>
						<li><a href="http://beta.costtodrive.com/delaware-tolls/">Delaware Tolls</a></li>
						<li><a href="http://beta.costtodrive.com/florida-tolls/">Florida Tolls</a></li>
						<li><a href="http://beta.costtodrive.com/illinois-tolls/">Illinois Tolls</a></li>
						<li><a href="http://beta.costtodrive.com/maryland-tolls/">Maryland Tolls</a></li>
					</ul>
					<ul class="col-sm-6">
						<li><a href="http://beta.costtodrive.com/new-jersey-tolls/">New Jersey Tolls</a></li>
						<li><a href="http://beta.costtodrive.com/new-york-tolls/">New York Tolls</a></li>
						<li><a href="http://beta.costtodrive.com/oklahoma-tolls/">Oklahoma Tolls</a></li>
						<li><a href="http://beta.costtodrive.com/pennsylvania-tolls/">Pennsylvania Tolls</a></li>
						<li><a href="http://beta.costtodrive.com/texas-tolls/">Texas Tolls</a></li>
					</ul>
				</div>
			</div>

			<div class="footer-top-destinations col-md-3 col-sm-6">
				<div class="row">
					<div class="col-md-12"><h2>TOP DESTINATIONS</h2></div>
					<ul class="col-sm-6">
						<li><a href="http://beta.costtodrive.com/popular-destinations/orlando-florida/">Orlando, FL</a></li>
						<li><a href="http://beta.costtodrive.com/popular-destinations/las-vegas-nevada/">Las Vegas, NV</a></li>
						<li><a href="http://beta.costtodrive.com/popular-destinations/new-york-new-york/">New York, NY</a></li>
						<li><a href="http://beta.costtodrive.com/popular-destinations/los-angeles-california/">Los Angeles, CA</a></li>
						<li><a href="http://beta.costtodrive.com/popular-destinations/chicago-illinois/">Chicago, IL</a></li>
					</ul>
					<ul class="col-sm-6">
						<li><a href="http://beta.costtodrive.com/popular-destinations/philadelphia-pennsylvania/">Philadelphia, PA</a></li>
						<li><a href="http://beta.costtodrive.com/popular-destinations/phoenix-arizona/">Phoenix, AZ</a></li>
						<li><a href="http://beta.costtodrive.com/popular-destinations/san-antonio-texas/">San Antonio, TX</a></li>
						<li><a href="http://beta.costtodrive.com/popular-destinations/san-diego-california/">San Diego, CA</a></li>
						<li><a href="http://beta.costtodrive.com/popular-destinations/san-francisco-california/">San Francisco, CA</a></li>
					</ul>
				</div>
			</div>

			<div class="footer-tunnels-bridges col-md-6 col-sm-12">
				<div class="row">
					<div class="col-md-12"><h2>TOP TOLL ROADS, BRIDGES &amp; TUNNELS</h2></div>
					<ul class="col-sm-4">
						<li><a href="http://beta.costtodrive.com/tolls-on-popular-bridges-and-tunnels/">G. Washington Bridge</a></li>
						<li><a href="http://beta.costtodrive.com/tolls-on-popular-bridges-and-tunnels/">Lincoln Tunnel</a></li>
						<li><a href="http://beta.costtodrive.com/tolls-on-popular-bridges-and-tunnels/">Holland Tunnel</a></li>
						<li><a href="http://beta.costtodrive.com/new-york-tolls/">Tappan Zee Bridge</a></li>
						<li><a href="http://beta.costtodrive.com/tolls-on-popular-bridges-and-tunnels/">Golden Gate Bridge</a></li>
					</ul>
					<ul class="col-sm-4">
						<li><a href="http://beta.costtodrive.com/new-jersey-tolls/">New Jersey Turnpike</a></li>
						<li><a href="http://beta.costtodrive.com/us-tolls/">Massachusetts Turnpike</a></li>
						<li><a href="http://beta.costtodrive.com/tolls-on-popular-bridges-and-tunnels/">Verrazano Narrows Bridge</a></li>
						<li><a href="http://beta.costtodrive.com/pennsylvania-tolls/">Pennsylvania Turnpike</a></li>
						<li><a href="http://beta.costtodrive.com/us-tolls/">Chicago Skyway</a></li>
					</ul>
					<ul class="col-sm-4">
						<li><a href="http://beta.costtodrive.com/florida-tolls/">Florida Turnpike</a></li>
						<li><a href="http://beta.costtodrive.com/us-tolls/">Ohio Turnpike</a></li>
						<li><a href="http://beta.costtodrive.com/new-york-tolls/">NY State Thruway</a></li>
						<li><a href="http://beta.costtodrive.com/new-york-tolls/">Indiana Toll Road</a></li>
						<li><a href="http://beta.costtodrive.com/us-tolls/http://beta.costtodrive.com/us-tolls/">Kansas Turnpike</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div><!-- footer-links-container -->

	<div id="colophon" class="container">

<?php
	/* A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
	get_sidebar( 'footer' );
?>
		</div><!-- #colophon -->
	</div><!-- #footer -->

</div><!-- #wrapper -->

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>

<link rel="stylesheet" href="http://beta.costtodrive.com/wp-content/themes/C2D-responsive/jquery.smartbanner.css" type="text/css" media="screen">
<script src="http://beta.costtodrive.com/wp-content/themes/C2D-responsive/jquery.smartbanner.js"></script>
<script type="text/javascript">
	jQuery(function () {
		if ( !(/(iPad|iPhone|iPod).*OS [6-7].*AppleWebKit.*Mobile.*Safari/.test(navigator.userAgent)) ) {
			jQuery.smartbanner({
				title: 'Tollsmart Toll Calculator',
				author: 'Tollsmart, LLC',
				price: '$1.99',
				icon: 'http://beta.costtodrive.com/wp-content/themes/C2D-responsive/images/tollsmart.jpeg',
				daysHidden: 1,
				daysReminder: 1
			});
		}
	});
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-5525368-2', 'auto');
  ga('send', 'pageview');

</script>

<script>
	var trackOutboundLink = function(url) {
		ga('send', 'event', 'outbound', 'click', url, {'hitCallback':
			function () {
				document.location = url;
			}
		});
	}

	var trackLightboxOpen = function() {
		ga('send', 'event', 'button', 'click', 'roadside-assistance-dollars5', {'hitCallback':
			function () {
				//console.log('click');
			}
		});
	}

</script>

<?php if (!is_page(1251)) : ?>
<script type="text/javascript" charset="utf-8">
  var is_ssl = ("https:" == document.location.protocol);
  var asset_host = is_ssl ? "https://d3rdqalhjaisuu.cloudfront.net/" : "http://d3rdqalhjaisuu.cloudfront.net/";
  document.write(unescape("%3Cscript src='" + asset_host + "javascripts/feedback-v2.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<script type="text/javascript" charset="utf-8">
  var feedback_widget_options = {};

  feedback_widget_options.display = "overlay";
  feedback_widget_options.company = "c2g";
  feedback_widget_options.placement = "left";
  feedback_widget_options.color = "#222";
  feedback_widget_options.style = "idea";

  var feedback_widget = new GSFN.feedback_widget(feedback_widget_options);
</script>
<?php endif; ?>

<?php //<script>(function(t,e,n,o){var s,c,i;t.SMCX=t.SMCX||[],e.getElementById(o)||(s=e.getElementsByTagName(n),c=s[s.length-1],i=e.createElement(n),i.type="text/javascript",i.async=!0,i.id=o,i.src=["https:"===location.protocol?"https://":"http://","widget.surveymonkey.com/collect/website/js/PybavBXk2mNvbkhCh_2B46eCkCaY7KyOy4RMndQARGi7bBi2byGVQggTtqjDylprnV.js"].join(""),c.parentNode.insertBefore(i,c))})(window,document,"script","smcx-sdk");</script> ?>
<?php //<script>(function(e,t,s,n){var o,c,a;e.SMCX=e.SMCX||[],t.getElementById(n)||(o=t.getElementsByTagName(s),c=o[o.length-1],a=t.createElement(s),a.type="text/javascript",a.async=!0,a.id=n,a.src=["https:"===location.protocol?"https://":"http://","widget.surveymonkey.com/collect/website/js/yaegale0Jlqu7ji131LLx0j41eT38LhHKPa1OEwBB11tYLDZAI0yN_2Bw3G9suW1_2Fg.js"].join(""),c.parentNode.insertBefore(a,c))})(window,document,"script","smcx-sdk");</script> ?>
<?php // <script src="https://www.surveymonkey.com/jsPop.aspx?sm=KfgdbbcwrZAbCe2iXfBkMA_3d_3d"> </script> ?>
<?php //<script src="https://www.surveymonkey.com/jsPop.aspx?sm=BVMzjjJiOarJau5E_2fRFz6A_3d_3d"> </script> ?>
<?php //<script src="https://www.surveymonkey.com/jsPop.aspx?sm=0C6CYzzfExvEWTGc3_2fhoTA_3d_3d"> </script> ?>
<?php //<script src="https://www.surveymonkey.com/jsPop.aspx?sm=n2_2bowbEnGJfIxkxD7M9v1Q_3d_3d"> </script> ?>
<?php //<script src="https://www.surveymonkey.com/jsPop.aspx?sm=AXJxoD8YK2ZQ1qe4cLoL8w_3d_3d"> </script> ?>
<?php //<script src="https://www.surveymonkey.com/jsPop.aspx?sm=8eV82ae0cdCPNomyOZ_2fGQQ_3d_3d"> </script> ?>

</body>
</html>
