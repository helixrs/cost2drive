<?php
/**
 * The template for displaying c2d widget.
 *
 * @package WordPress
 * @subpackage cost_to_drive
 * @since Cost to Drive 1.0
 */
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
include("app/include/globals.php");
include("app/classes/object-car.php");

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" media="all" href="http://<?php print $_SERVER['SERVER_NAME']; ?>/wp-content/themes/C2D/style-widget-gettysburg.css" />
<script type='text/javascript' src='http://<?php print $_SERVER['SERVER_NAME']; ?>/wp-includes/js/jquery/jquery.js?ver=1.6.1'></script>
<script type='text/javascript' src='http://<?php print $_SERVER['SERVER_NAME']; ?>/wp-content/themes/C2D/widget-main2-gettysburg.js?ver=1.0'></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-5525368-16', 'costtodrive.com');
  ga('send', 'pageview');

</script>
</head>

<body>

	<div class="c2d_widget c2d_widget_<?php print (isset($_GET['wid'])) ? $_GET['wid'] : 1; ?>">
		<form id="where_to_f"  action="http://<?php print $_SERVER['SERVER_NAME']; ?>/from" method="get">
			<!-- <img src="<?php print get_bloginfo('template_url'); ?>/images/structural/c2d_widget_logo.png" alt="Cost To Drive" title="Cost To Drive" /> -->
			<h2 id="getty-tp-heading">Gettysburg Trip Planner</h2>
			<p class="getty-info">Calculate Fuel Costs, Find Top Hotels,<br/>Restaurants &amp; Attractions</p>
			<?php include("app/include/widget-trip-form-gettysburg.php");?>
			<?php include("app/include/widget-car-form.php");?>
			<div>
				<input class="my_c2d_btn_getty" type="submit" value="s" />
				<a class="powered-by-c2d" target="_blank" href="http://beta.costtodrive.com/">Powered by cost2drive.com</a>
			</div>
		</form>
		<?php include("app/include/widget-car-script.php"); ?>
	</div>


</body>

</html>
