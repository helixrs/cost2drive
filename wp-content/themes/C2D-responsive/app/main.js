jQuery(document).ready(function ($) {

    // twitter/facebook tabs
    $('ul.snf-tabs li a').click(function () {
        $('ul.snf-tabs li a').removeClass('active');
        $(this).addClass('active');
        class_name = $(this).parent('li').attr('class');
        $('.soc-net-feed div.feed').hide();
        $('.soc-net-feed div.' + class_name).show();
        return false;
    });

    // tripadvisor - first destination
    var taid_value = $('#taid-value').val();
    $.ajax({
        type: "POST",
        url: 'http://beta.costtodrive.com/tripadvisor',
        //data: "taid=60745",
        data: "taid=" + taid_value,
        success: function (msg) {
            $('.dest-recommend-content').html(msg);
            $('.ta-first-dest .destinations-tabs-content ul:eq(0)').css('display', 'block');
            $('.ta-first-dest .destinations-tabs ul li:eq(0) a').addClass('active');
        }
    });

    // weather widget
    if ($('.weather-box').length > 0) {
        $.ajax({
            type: "POST",
            url: 'http://beta.costtodrive.com/wp-content/themes/C2D/ajax/weather-widget.php',
            data: {'end_location_name': end_location_name, 'lat': end_point_lat, 'lng': end_point_lng},
            success: function (msg) {
                if (msg.indexOf('Warning') === -1) {
                    $('.weather-box').html(msg);
                }
            }
        });
    }

    // tripadvisor - waypoints
    $('.load-waypoint-ta').click(function () {
        var _wrapper = $(this).parent().parent('.dest-recommend-content-wrap');
        $(this).hide();
        _wrapper.find('.hide-waypoint-ta').show();
        _wrapper.find('.dest-recommend-content-waypoint').show();
        var taid_value = _wrapper.find('input').val();

        $.ajax({
            type: "POST",
            url: 'http://beta.costtodrive.com/tripadvisor',
            data: "taid=" + taid_value,
            success: function (msg) {
                _wrapper.find('.dest-recommend-content-waypoint').html(msg);
                _wrapper.find('.destinations-tabs-content ul:eq(0)').css('display', 'block');
                _wrapper.find('.destinations-tabs ul li:eq(0) a').addClass('active');
            }
        });

        return false;
    });
    $('.hide-waypoint-ta').click(function () {
        var _wrapper = $(this).parent().parent('.dest-recommend-content-wrap');
        $(this).hide();
        _wrapper.find('.show-waypoint-ta').show();
        _wrapper.find('.dest-recommend-content-waypoint').hide();
        return false;
    });
    $('.show-waypoint-ta').click(function () {
        var _wrapper = $(this).parent().parent('.dest-recommend-content-wrap');
        $(this).hide();
        _wrapper.find('.hide-waypoint-ta').show();
        _wrapper.find('.dest-recommend-content-waypoint').show();
        return false;
    });


    // tripadvisor tabs
    $('.destinations-tabs ul li a').live('click', function () {
        tab_index = $(this).parent('li').index('.destinations-tabs ul li');
        _wrapper = $(this).parents('.dest-recommend-content-wrap');
        _wrapper.find('.destinations-tabs ul li a').removeClass('active');
        $(this).addClass('active');
        _wrapper.find('.destinations-tabs-content ul').hide();
        $('.destinations-tabs-content ul:eq(' + tab_index + ')').show();
        return false;
    });

    // recently calculated routes
    $('.popular-destinations .destination-recommendations').after($('#recently-calculated-routes'));


    // twitter
    /*
     new TWTR.Widget({
     version: 2,
     id: 'on-tweeter',
     type: 'search',
     search: '\"Cost2Drive\" OR @Cost2Drive',
     interval: 6000,
     title: '',
     subject: '',
     width: 'auto',
     height: 268,
     theme: {
     shell: {
     background: '#e6f0f4',
     color: '#ffffff'
     },
     tweets: {
     background: '#ffffff',
     color: '#666666',
     links: '#007A87'
     }
     },
     features: {
     scrollbar: true,
     loop: true,
     live: true,
     hashtags: true,
     timestamp: true,
     avatars: true,
     toptweets: true,
     behavior: 'default'
     }
     }).render().start();
     */

    // expand / collapse
    $('#tech-details .expand').click(function () {
        $(this).hide();
        $('#tech-details .collapse').show();
        $('.tech-details-content').show();
        return false;
    });
    $('#tech-details .collapse').click(function () {
        $(this).hide();
        $('#tech-details .expand').show();
        $('.tech-details-content').hide();
        return false;
    });

    $('#your_trip_info .expand-et').click(function () {
        $(this).hide();
        $('#your_trip_info .collapse-et').show();
        $('#edit-trip-form').show();
        return false;
    });
    $('#your_trip_info .expand-et-er').click(function () {
        $('#edit-trip-form').show();
        return false;
    });
    $('#your_trip_info .collapse-et').click(function () {
        $(this).hide();
        $('#your_trip_info .expand-et').show();
        $('#edit-trip-form').hide();
        return false;
    });

    // fancybox popup - edit trip
    $('a#edit').fancybox({
        'hideOnContentClick': false
    });

    var fBoxWidth = 450;
    if (window.innerWidth < 550) {
        fBoxWidth = window.innerWidth - 50;
    }
    $("#rsa1, #rsa2").fancybox({
        'modal': true,
        'hideOnContentClick': false,
        'autoDimensions': false,
        'width': fBoxWidth,
        'height': 'auto'
    });


    // form
    var where_initial = $('#end').val();
    $('.home #where_to_f input[type="text"]:not(".prepopulated")').focusin(function () {
        if ($(this).val() == where_initial) $(this).val('');
    });
    $('.home #where_to_f input[type="text"]:not(".prepopulated")').focusout(function () {
        if ($(this).val() == '') $(this).val(where_initial);
    });

    $('#where_to_f').submit(function () {

        // validation
        $('.error-message').remove();

        var value_placeholder = 'Enter Address, City, State or Zip';
        if ($('input#start').val() == '' || $('input#start').val() == value_placeholder) {
            $('#start').after('<p class="error-message">Please enter Origin</p>');
            valid = false;
        }
        if ($('input#end').val() == '' || $('input#end').val() == value_placeholder) {
            $('#end').after('<p class="error-message">Please enter Destination</p>');
            valid = false;
        }

        // make clean url
        /*
         var clean_url = '/from';
         clean_url = clean_url + '/' + $('input#start').val();
         clean_url = clean_url + '/to/' + $('input#end').val();
         $('input.waypoint').each(function () {
         if ($(this).val() != '') clean_url = clean_url + '/via/' + $(this).val();
         });
         clean_url = clean_url + '/' + $('select#passengers').val();

         if ($('#carf').is(':visible')) {

         if (!$('select#year_c option:first').is(':selected') && !$('select#make option:first').is(':selected') && !$('select#model option:first').is(':selected')) {
         clean_url = clean_url + '/' + $('input#carId').val();
         clean_url = clean_url + '/' + $('select#year_c').val();
         clean_url = clean_url + '-' + $('select#make').val();
         clean_url = clean_url + '-' + $('select#model').val();
         } else {
         valid = false;
         $('#model_fs').after('<p class="error-message">Please select your car Year / Make / Model</p>');
         }

         } else if ($('#customcar').is(':visible')) {

         clean_url = clean_url + '/custom';
         if ($('input#mpg').val() != '' && $('input#gpt').val() != '') {
         clean_url = clean_url + '/' + $('input#mpg').val();
         clean_url = clean_url + '/' + $('input#gpt').val();
         clean_url = clean_url + '/' + $('select#fuelType').val();
         } else {
         valid = false;
         $('#fuelType').after('<p class="error-message">Please enter you car info</p>');
         }

         }
         clean_url1 = (clean_url.replace(/ /g, '-')).replace(/,/g, '');

         if (!valid) {
         return false;
         } else {
         window.location = clean_url1;
         return false;
         }
         */
        var cleanUrl = makeCleanUrl();
        if (cleanUrl !== false) {
            window.location = cleanUrl;
            return false;
        }
        else {
            return false;
        }
    });

    function makeCleanUrl() {
        // make clean url
        var valid = true;
        var clean_url = '/from';
        clean_url = clean_url + '/' + $('input#start').val();
        clean_url = clean_url + '/to/' + $('input#end').val();
        $('input.waypoint').each(function () {
            if ($(this).val() != '') clean_url = clean_url + '/via/' + $(this).val();
        });
        clean_url = clean_url + '/' + $('select#passengers').val();

        if ($('#carf').is(':visible')) {

            if (!$('select#year_c option:first').is(':selected') && !$('select#make option:first').is(':selected') && !$('select#model option:first').is(':selected')) {
                clean_url = clean_url + '/' + $('input#carId').val();
                clean_url = clean_url + '/' + $('select#year_c').val();
                clean_url = clean_url + '-' + $('select#make').val();
                clean_url = clean_url + '-' + $('select#model').val();
            } else {
                valid = false;
                $('#model_fs').after('<p class="error-message">Please select your car Year / Make / Model</p>');
            }

        } else if ($('#customcar').is(':visible')) {

            clean_url = clean_url + '/custom';
            if ($('input#mpg').val() != '' && $('input#gpt').val() != '') {
                clean_url = clean_url + '/' + $('input#mpg').val();
                clean_url = clean_url + '/' + $('input#gpt').val();
                clean_url = clean_url + '/' + $('select#fuelType').val();
            } else {
                valid = false;
                $('#fuelType').after('<p class="error-message">Please enter you car info</p>');
            }

        }
        var clean_url1 = (clean_url.replace(/ /g, '-')).replace(/,/g, '');

        if (!valid) {
            return false;
        } else {
            return clean_url1;
        }
    }

    //lightbox event handlers
    $(document).keypress(function (e) {

        if (e.which == 13) {
            e.preventDefault();
            $('.my_c2d_btn').trigger('click');
        }
    });

    //displaymodal for testing
    //$(".modal").modal('show');


    $('.my_c2d_btn, .phone-btn').on('click', function (e) {

        //check if we are on home page or popular destinations page
        if ($('body').hasClass('home') || $('body').hasClass('page-template-page-pop-detinations-php')) {
            console.log(validateHomePageForm());
            if (validateHomePageForm()) {
                // if (true) {

                if (!readCookie('awd_subscribed')) {
                    e.preventDefault();
                    $('#start-location').val($('#start').val());
                    $(".modal").modal('show');
                }
                else {
                    //jQuery('#where_to_f').submit();
                }

            }
            else {
                e.preventDefault();
            }
        }
        else {
            //jQuery('#where_to_f').submit();
        }

    });

    $('.front-page-lightbox .close').on('click', function () {
        gaTrack('email-capture-closed');
        $('#where_to_f').submit();
    })

    $('#submit-email').on('click', function (e) {
        if (!validateEmail($('#email').val()) && $('#start-location').val() == '') {
            $('#email').addClass('invalid');
            $('#start-location').addClass('invalid');
            e.preventDefault();
            return false;
        }
        if (!validateEmail($('#email').val())) {
            $('#email').addClass('invalid');
            e.preventDefault();
            return false;
        }
        if ($('#start-location').val() == '') {
            $('#start-location').addClass('invalid');
            e.preventDefault();
            return false;
        }
        $('#start-location').removeClass('invalid');
        $('#email').removeClass('invalid');

        $.ajax({
            type: "POST",
            // url: 'http://beta.costtodrive.com/email-capture',
            url: 'http://beta.costtodrive.com/wp-content/themes/C2D-responsive/email-capture.php',
            //data: "taid=60745",
            data: {
                email: $('#email').val(),
                start: $('#start-location').val(),
                end: $('#end').val(),
                queryString: makeCleanUrl()
            },
            success: function (msg) {
                try {
                    console.log(msg);
                    var response = JSON.parse(msg);
                    if (response.status) {
                        gaTrack('email-captured');
                        $('#where_to_f').submit();
                        createCookie('awd_subscribed', true, 365);
                        $(".modal").modal('hide');
                    }
                    else {
                        alert(response.message);
                    }
                }
                catch (e) {
                    alert(response.message);
                }

            },
            error: function (error) {
                console.log(error);
                $(".modal").modal('hide');

            }
        });
    });

    $("body").on("click", ".learn-more", function () {
        $(".step1,.step-1-footer").fadeOut("fast", function () {
            $('.step2, .step-2-footer').fadeIn("fast");
        });
    });
    $("body").on("click", ".payment-btn", function () {
        $(".step2,.step-2-footer").fadeOut("fast", function () {
            $('.step3, .step-3-footer').fadeIn("fast");
        });
    });

    $(".cancel-step-1").on("click", function () {
        console.log('step 1 cancel');
        $(".modal").modal('hide');
        gaTrack('step1');
        $('#where_to_f').submit();
    });
    $("body").on("click", ".cancel-step-3", function () {
        $(".modal").modal('hide');
        gaTrack('step3');
        jQuery('#where_to_f').submit();
    });

    $("body").on("click", ".cancel-step-2", function () {
        $(".modal").modal('hide');
        gaTrack('step2');
        jQuery('#where_to_f').submit();
    });

    //send ga on find deals click
       gaTrack("displayed-"+$('.find-deals').attr('data-ga'));

    $('.find-deals').on('click', function (e) {
        if ($('#datepicker-end').val() == '' && $('#datepicker-start').val() == '') {
            $('#datepicker-end').addClass('invalid');
            $('#datepicker-start').addClass('invalid');
            e.preventDefault();
            return false;
        }
        if ($('#datepicker-end').val() == '') {
            $('#datepicker-end').addClass('invalid');
            e.preventDefault();
            return false;
        }
        if ($('#datepicker-start').val() == '') {
            $('#datepicker-start').addClass('invalid');
            e.preventDefault();
            return false;
        }
        gaTrack('clicked-'+$(this).attr('data-ga'));
    });


    //ajax test


    //init datepickers
    if ($("#datepicker").length > 0) {
        $("#datepicker").datepicker();
        $("#datepicker2").datepicker();
    }
   /* $('body').on('focus', '#start-trip-time', function () {
        if ($('#end-trip-time').val() != "") {
            var endDate = $('#end-trip-time').val();
            console.log(endDate);
            endDate = endDate.split("/");
            var year = endDate.pop();
            console.log(year);
            var day = endDate.pop();
            console.log(day);
            var month = endDate.pop();
            console.log(month);

            $("#start-trip-time").datepicker("option", 'maxDate', new Date(parseInt(year), parseInt(month), parseInt(day)));
        }
        else {
            $("#start-trip-time").datepicker();
        }
    });*/
   /* $('body').on('focus', '#end-trip-time', function () {
        if ($('#start-trip-time').val() != "") {
            var startDate = $('#start-trip-time').val();
            console.log(startDate);
            startDate = startDate.split("/");
            var year = startDate.pop();
            console.log(year);
            var day = startDate.pop();
            console.log(day);
            var month = startDate.pop();
            console.log(month);

            $("#end-trip-time").datepicker("option", 'minDate', new Date(parseInt(year), parseInt(month), parseInt(day)));
        }
        else {
            $("#end-trip-time").datepicker();
        }
    });
    if ($("#start-trip-time").length > 0) {
        $("#start-trip-time").datepicker({minDate: new Date(),
        onSelect: function (selectedDate) {
            if ($("#end-trip-time").val()!="")
            {

            }
            $("#end-trip-time").datepicker("option", "minDate", selectedDate);
        }
        });
        $("#end-trip-time").datepicker({minDate: new Date()});
    }*/
    if ($(".datepicker").length > 0) {
        $('#datepicker-start').datepicker({
            minDate: new Date(),
            onSelect: function (selectedDate) {
                $('#datepicker-start').removeClass('invalid');
                $("#datepicker-end").datepicker("option", "minDate", selectedDate);
                var href = $('.find-deals').attr('href');
                if (href.indexOf('arrival_date') < 0) {
                    $('.find-deals').attr('href', href + '&arrival_date=' + encodeURIComponent(selectedDate));
                }
                else {
                    var params = jQuery('.find-deals').attr('href').split('&');
                    console.log(params);
                    $.each(params, function (i, v) {
                        if (v.indexOf('arrival_date') >= 0) {
                            console.log('params');
                            params[i] = '&arrival_date=' + encodeURIComponent(selectedDate);
                        }
                        else {
                            params[i] = v;
                        }
                    });
                    $('.find-deals').attr('href', params.join("&"));

                }
            }

        })
        $("#datepicker-end").datepicker({
            minDate: new Date(),
            onSelect: function (selectedDate) {
                $('#datepicker-end').removeClass('invalid');
                $("#datepicker-start").datepicker("option", "maxDate", selectedDate);
                var $this = $(this);
                var href = $('.find-deals').attr('href');

                if (href.indexOf('departure_date') < 0) {
                    $('.find-deals').attr('href', href + '&departure_date=' + encodeURIComponent(selectedDate));
                }
                else {
                    var params = jQuery('.find-deals').attr('href').split('&');
                    $.each(params, function (i, v) {
                        if (v.indexOf('departure_date') >= 0) {
                            params[i] = '&departure_date=' + encodeURIComponent(selectedDate);
                        }
                        else {
                            params[i] = v;
                        }
                    });
                    $('.find-deals').attr('href', params.join("&"));

                }
            }
        });
    }
    //booking buddy select date
    $('#datepicker-end').on('change', function () {

    });
    $('#datepicker-start').on('change', function () {

    });


});
function validateHomePageForm() {
    var valid = true;
    jQuery('.error-message').remove();

    var value_placeholder = 'Enter Address, City, State or Zip';
    if (jQuery('input#start').val() == '' || jQuery('input#start').val() == value_placeholder) {
        jQuery('#start').after('<p class="error-message">Please enter Origin</p>');
        valid = false;
    }
    if (jQuery('input#end').val() == '' || jQuery('input#end').val() == value_placeholder) {
        jQuery('#end').after('<p class="error-message">Please enter Destination</p>');
        valid = false;
    }

    // make clean url
    var clean_url = '/from';
    clean_url = clean_url + '/' + jQuery('input#start').val();
    clean_url = clean_url + '/to/' + jQuery('input#end').val();
    jQuery('input.waypoint').each(function () {
        if (jQuery(this).val() != '') clean_url = clean_url + '/via/' + jQuery(this).val();
    });
    clean_url = clean_url + '/' + jQuery('select#passengers').val();

    if (jQuery('#carf').is(':visible')) {

        if (!jQuery('select#year_c option:first').is(':selected') && !jQuery('select#make option:first').is(':selected') && !jQuery('select#model option:first').is(':selected')) {
            clean_url = clean_url + '/' + jQuery('input#carId').val();
            clean_url = clean_url + '/' + jQuery('select#year_c').val();
            clean_url = clean_url + '-' + jQuery('select#make').val();
            clean_url = clean_url + '-' + jQuery('select#model').val();
        } else {
            valid = false;
            jQuery('#model_fs').after('<p class="error-message">Please select your car Year / Make / Model</p>');
        }

    } else if (jQuery('#customcar').is(':visible')) {

        clean_url = clean_url + '/custom';
        if (jQuery('input#mpg').val() != '' && jQuery('input#gpt').val() != '') {
            clean_url = clean_url + '/' + jQuery('input#mpg').val();
            clean_url = clean_url + '/' + jQuery('input#gpt').val();
            clean_url = clean_url + '/' + jQuery('select#fuelType').val();
        } else {
            valid = false;
            jQuery('#fuelType').after('<p class="error-message">Please enter you car info</p>');
        }

    }
    return valid;
}

function gaTrack(step) {
    console.log('ga track called for:' + step);

    ga('send', 'event', 'button', 'click', step, {
        'hitCallback': function () {
            console.log('ga track callback called for:' + step);
        }
    });
}
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}