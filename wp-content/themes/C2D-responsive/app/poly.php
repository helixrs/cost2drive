<?php

function distanceFrom($lat1, $lng1, $lat2, $lng2, $miles = TRUE)
{
     $pi80 = M_PI / 180;
     $lat1 *= $pi80;
     $lng1 *= $pi80;
     $lat2 *= $pi80;
     $lng2 *= $pi80;
 
     $r = 6372.797; // mean radius of Earth in km
     $dlat = $lat2 - $lat1;
     $dlng = $lng2 - $lng1;
     $a = SIN($dlat / 2) * SIN($dlat / 2) + COS($lat1) * COS($lat2) * SIN($dlng / 2) * SIN($dlng / 2);
     $c = 2 * ATAN2(SQRT($a), SQRT(1 - $a));
     $km = $r * $c;
 
     RETURN ($miles ? ($km * 0.621371192) : $km);
}

function GetPointAtDistance($miles,$pointsArray)
{
  if ($miles == 0) return $pointsArray[0];
  if ($miles < 0) return null;
  $dist=0;
  $olddist=0;
  for ($i=1; ($i < count($pointsArray) && $dist < $miles); $i++) {
    $olddist = $dist;
    $dist += distanceFrom($pointsArray[$i][0],$pointsArray[$i][1],$pointsArray[$i-1][0],$pointsArray[$i-1][1]);
  }
  if ($dist < $miles) {return null;}
  $p1= $pointsArray[$i-2];
  $p2= $pointsArray[$i-1];
  
  $m = ($miles-$olddist)/($dist-$olddist);
  $lat = $p1[0] + ($p2[0]-$p1[0])*m;
  $long = $p1[1] + ($p2[1]-$p1[1])*m;
  $pointAtDistance[0] = $lat;
  $pointAtDistance[1] = $long;
  return $pointAtDistance;
}
?>