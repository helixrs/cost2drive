<?php
	
	// get tripAdvisor id by location name
	function get_taid($location_readable_name) {
		
		global $wpdb;
		$taid_is_ok = false;
		if (!empty($location_readable_name)) {					
			$query = sprintf("SELECT geo_id FROM geos_id_cache WHERE geo_name='%s' AND geo_id<>0 LIMIT 1",
							mysql_real_escape_string($location_readable_name));					
			$row = $wpdb->get_row($query);
	
			if ($row != null) {
				$taid_num = $row->geo_id;
				if (is_numeric($taid_num) && $taid_num > 0) {
					$taid_is_ok = true;
				}
				//print '<!-- from cache: '.$taid.' -->';								
			} else {
				$contents = file_get_contents('https://www.google.com/search?q=tripadvisor+hotels+'.urlencode($location_readable_name));
				$pattern = '/http:\/\/www\.tripadvisor(.*?)-g([0-9]+)/';					
				preg_match($pattern, $contents, $matches);					
				$taid_num = $matches[2];
				if (is_numeric($taid_num) && $taid_num > 0) {
					if ($wpdb->insert('geos_id_cache', array('geo_id' => $taid_num, 'geo_name' => $location_readable_name), array('%d', '%s'))) {
						$taid_is_ok = true;
						//print '<!-- write into db -->';
					}
				}
				//print '<!-- live: '.$taid.' -->';
			}
		}
		if ($taid_is_ok) {
			return $taid_num;
		} else {
			return 0;
		}
	}
	
	function trip_advisor_content_first_html($taid, $location_name) {
		
		$ta_html = '<h2 id="dest-recommend">New! <span>Destination Recommendations for '.$location_name.'</span></h2>
		<input type="hidden" name="taid-value" value="'.$taid.'" id="taid-value" />
		<div class="dest-recommend-content-wrap ta-first-dest">
			<div class="dest-recommend-content">
				<img class="spiner" src="http://'.$_SERVER['SERVER_NAME'].'/wp-content/themes/C2D/images/ajax-loader.gif" alt="loading..." title="" />
			</div>
		</div>';
		
		return $ta_html;
	}
	
	
	function trip_advisor_content_html($taid, $location_name) {
		$ta_html = '<div class="dest-recommend-content-wrap">
            <div class="dest-recommend-content-wrap-way">
                <h2 class="ta-waypoint-title">Destination Recommendations for '.$location_name.'</h2>
                <a class="load-waypoint-ta" href="#">Expand</a>
                <a class="show-waypoint-ta" href="#">Expand</a>
                <a class="hide-waypoint-ta" href="#">Collapse</a>
                <input type="hidden" name="taid-'.$taid.'" value="'.$taid.'" />
                <div class="dest-recommend-content-waypoint">
                    <img class="spiner" src="http://'.$_SERVER['SERVER_NAME'].'/wp-content/themes/C2D/images/ajax-loader.gif" alt="loading..." title="" />
                </div>
            </div>
		</div>';
		
		return $ta_html;
	}
	
	function trip_advisor_box_html($taid, $location_name, $photo_url) {
		
		$ta = '<div style=\"width: 210px;\"><a style=\"float: left; width: 70px;\" target=\"_blank\" href=\"http://www.tripadvisor.com/Tourism-g'.$taid.'\"><img style=\"width: 60px; height: 60px; border: 2px solid #589442;\" src=\"'.$photo_url.'\" /></a><a style=\"float: left; width: 140px; text-decoration: none;\" target=\"_blank\" href=\"http://www.tripadvisor.com/Tourism-g'.$taid.'\">Find top hotels, restaurants and attractions in '.$location_name.'</a><div style=\"float: left; padding-top: 7px; width: 210px;\"><span style=\"float: left; width: 65px; font-size: 10px; font-weight: bold; color: #7C8080;\">Powered by</span><img style=\"float: left; width: 90px;\" src=\"/wp-content/themes/C2D/images/structural/tripadvisor_logo_api.gif\" alt=\"tripadvisor\"></div></div>';
		
		return $ta;
		
	}

?>