<?php
error_reporting(0);

function getStopDetails($fuelType="Unleaded",$points)
{
	$mh = curl_multi_init();
	
	$baseUrl = "http://localhost:8080/solr/rgeocoder/select?q=*:*&qt=geo&radius=50&rows=1&sort=geo_distance%20asc&wt=php";
	$results =0;

	if ( !isset($fuelType))
	{
	  $fuelType="Unleaded";
	} 
	elseif ($fuelType == "Regular")
	{
	  $fuelType = "Unleaded";
	}
	
	
	foreach($points as $p => $point)
	{
		$lat = $point->lat;	
	  	$lon = $point->lng;
	
	  	$conn[$p] = curl_init();
	
	  	$req = "$baseUrl&lat=$lat&long=$lon";
		curl_setopt($conn[$p], CURLOPT_URL, $req);
		curl_setopt ($conn[$p], CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
		curl_setopt ($conn[$p], CURLOPT_TIMEOUT, 20); 
		curl_setopt($conn[$p], CURLOPT_RETURNTRANSFER, true);
		curl_multi_add_handle($mh, $conn[$p]);
		$results++;
	}
	
	$active = null;
	do {
	  $n = curl_multi_exec($mh , $active);
	}  while($active > 0);
	
	
	$r2=0;
	$js = array();
	
	$zips = array();
	
	$i = 0;
	foreach ( $points as $p => $point) {
	  
	  $data = curl_multi_getcontent($conn[$p]);
	
	  eval("\$content = " .$data.";");
	
	  if ($content["response"]["numFound"] > 0 ) {
	
	    $d = $content["response"]["docs"]["0"];

	    array_push($js, $d);
	    $zips[ $d["zipcode"]] = $d["latitude"].",".$d["longitude"] ;
	
	  }
	  
	  curl_multi_remove_handle($mh, $conn[$p]);
	  curl_close($conn[$p]);
	
	  $r2++;
	}
	
	$gas_prices = getOpisPricesByLatLong_proxy($zips, $fuelType);
	$outputarray = array();
	foreach ($js as $d => $c)
	{
	  $zd = $c["zipcode"];
	  $c["gas_price"] = $gas_prices[$zd]["gp"];
	  $c["station"] = $gas_prices[$zd]["station"];
	  array_push($outputarray, $c);
	}

	curl_multi_close($mh);
	
	
	return $outputarray;
}

function getOpisTokens($numOfTokens) {

  $mh = curl_multi_init();
  $c2d_id = "6Y8zws/8U/0FUIgEdrgbYQ==";
  $url = "https://services.opisnet.com/RealtimePriceService/RealtimePriceService.asmx/Authenticate";
  
  $conn = array();
  for ($i =0 ; $i < $numOfTokens; $i++) {

    $conn[$i] = curl_init();

    curl_setopt($conn[$i], CURLOPT_URL, $url."?"."CustomerToken=$c2d_id");
    curl_setopt ($conn[$i], CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
    curl_setopt ($conn[$i], CURLOPT_TIMEOUT, 2000); 
    curl_setopt($conn[$i], CURLOPT_RETURNTRANSFER, true);

# Debug
#    curl_setopt($conn[$i], CURLOPT_VERBOSE, 1);

    curl_multi_add_handle($mh, $conn[$i]);
  }
  $active = null;
  do {
    $n = curl_multi_exec($mh, $active);
  } while($active > 0);

  $data = array();

  $token_regexp = "/.*<string.*?>(.*?)<\/string>/";

  for($i =0 ; $i < $numOfTokens; $i++) {

    $result = curl_multi_getcontent($conn[$i]);

    curl_multi_remove_handle($mh, $conn[$i]);
    curl_close($conn[$i]);

    preg_match($token_regexp, $result, $matches);
    $token = $matches[1];

    $data[$i] = $token;
  }

  curl_multi_close($mh);

  return $data;
}


function getOpisPricesByZip($zips)
{

  $mh = curl_multi_init();
  $url = "https://services.opisnet.com/RealtimePriceService/RealtimePriceService.asmx/GetZipCodeResults";

  $tokens = getOpisTokens(sizeof($zips));
  $pos = 0;
  $conn = array();

  $token_regexp = "/.*?(<StationPrices .*?<\/StationPrices>).*/mis";

  foreach ($zips as $k => $zip) {
    
    $token = $tokens[$pos];

    $conn[$zip] = curl_init();

    curl_setopt($conn[$zip], CURLOPT_URL, $url."?"."UserTicket=".urlencode($token)."&ZipCode=$zip");

# Debug
#    echo $url."?"."UserTicket=".urlencode($token)."&ZipCode=$zip";

    curl_setopt ($conn[$zip], CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
    curl_setopt ($conn[$zip], CURLOPT_TIMEOUT, 2000); 
    curl_setopt($conn[$zip], CURLOPT_RETURNTRANSFER, true);
    curl_setopt($conn[$zip], CURLOPT_FRESH_CONNECT, true);
    curl_setopt($conn[$zip], CURLOPT_FORBID_REUSE, true);

# Debug
#    curl_setopt($conn[$zip], CURLOPT_VERBOSE, 1);

    curl_multi_add_handle($mh, $conn[$zip]);
    $pos++;
  }
  $active = null;
  do {
    $n = curl_multi_exec($mh, $active);
  } while($active > 0);

  $data = array();
  foreach ($zips as $k => $zip) {

    $content =  curl_multi_getcontent($conn[$zip]);

    preg_match($token_regexp, $content, $matches);
    $content1 = $matches[1];

    $xml = new SimpleXMLElement($content1);
    $data[$zip] = $xml;

    curl_multi_remove_handle($mh, $conn[$zip]);
    curl_close($conn[$zip]);

  }

  curl_multi_close($mh);

  return $data;
  
}

function RecurseXML($xml,&$vals,$parent="")
{

  $childs=0;
  $child_count=-1; # Not realy needed.
		     $arr=array();
  foreach ($xml->children() as $key=>$value) {
    if (in_array($key,$arr)) {
      $child_count++;
    } else {
      $child_count=0;
    }
    $arr[]=$key;
    $k=($parent == "") ? "$key.$child_count" : "$parent.$key.$child_count";
    $childs=RecurseXML($value,$vals,$k);
    if ($childs==0) {
      $vals[$k]= (string)$value;
    }
  }

  return $childs;
}

function noop($errno, $errstr, $errfile, $errline) {

}

function getOpisPricesByLatLong_proxy($zips, $product)
{
  $url = "http://www.gissearch.com/apps/opis_proxy_v3.php?v=v6&product=$product";
  $proxy = "216.121.92.34";
  $proxyport = "3128";
  $zips_p = "&zips=";

  $zips_p .= urlencode(serialize($zips));


  $ch = curl_init();
  curl_setopt ($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
  curl_setopt($ch, CURLOPT_PROXY,  $proxy);
  curl_setopt($ch, CURLOPT_PROXYPORT, $proxyport); 
  curl_setopt($ch, CURLOPT_URL, "$url$zips_p");

  curl_setopt($ch, CURLOPT_VERBOSE, TRUE);


  curl_setopt ($ch, CURLOPT_TIMEOUT, 20); 
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  $result = curl_exec($ch);
  curl_close($ch);
  

  return unserialize($result);

}


?>
