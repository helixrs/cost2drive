<?php

function getAirport($points)
{
    $mh = curl_multi_init();
    $baseUrl = "http://localhost:8080/solr/airports/select?q=*:*%20AND%20rank:[1%20TO%20*]&qt=geo&radius=75&rows=1&sort=rank%20asc&wt=php";
    $results = 0;

    foreach ($points as $p => $point) {

        // Request from local rgeocoder engine and return results as json
        list($lon, $lat) = split(",", $point);

        $conn[$p] = curl_init();
        $req = "$baseUrl&lat=$lat&long=$lon";
        #  echo "---- $req--\n";
        curl_setopt($conn[$p], CURLOPT_URL, $req);
        curl_setopt($conn[$p], CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        curl_setopt($conn[$p], CURLOPT_TIMEOUT, 20);
        curl_setopt($conn[$p], CURLOPT_RETURNTRANSFER, true);
        curl_multi_add_handle($mh, $conn[$p]);
        $results++;
    }

    $active = null;
    do {
        $n = curl_multi_exec($mh, $active);
    } while ($active > 0);

    $airports = array();

    foreach ($points as $p => $point) {
        $data = curl_multi_getcontent($conn[$p]);

        eval("\$content = " . $data . ";");

        if ($content["response"]["numFound"] > 0) {

            $d = $content["response"]["docs"]["0"];
            array_push($airports, $d);
        }

        curl_multi_remove_handle($mh, $conn[$p]);
        curl_close($conn[$p]);
    }

    curl_multi_close($mh);
    return $airports;
}

function getKayakAirport($codes)
{

    $ch = curl_init();

    $url = "http://www.kayak.com/h/rss/fare?code=" . $codes[0] . "&dest=" . $codes[1] . "&mc=USD&src=costtodrive.com";
#  echo "$url\n";
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($ch);
    curl_close($ch);
    $result = preg_replace("/kyk\:/", "", $result);
    $xml = new SimpleXMLElement($result);

    $data = array();

    foreach ($xml->channel->item as $item) {

        $feed = array();
        $furl = urldecode((string)$item->link);
        $furl = preg_replace("/ai\=/", "", $furl);
        $feed["title"] = (string)$item->title;
        $feed["link"] = $furl . "&src=costtodrive.com&ai=costtodrive";
        $feed["originName"] = (string)$item->originName;
        $feed["destName"] = (string)$item->destName;
        $feed["price"] = (string)$item->price;
        array_push($data, $feed);
//    RecurseXML($item,$i);
//    echo"<pre>\n";
//      print_r($i);
//    echo "</pre>";
    }

    return $data;

}

function getKayakInfo($stops)
{


    if (sizeof($stops) < 2) {

        $stops[0] = "-73.958508,40.766659"; //Manhattan, NY
        $stops[1] = "-77.339946,38.980241"; //Reston, VA

    }

    $cb = $_GET["jsonp"];

    #echo "/*<pre>\n";
    $codes = getAirport($stops);
    #print_r($codes);
    $feeds = getKayakAirport(array($codes[0]["code"], $codes[1]["code"]));


    $r = rand(3, 99999);
    $res = array();

    foreach ($feeds as $feed) {


        $l = "<a href='http://ads.costtogo.com/www/delivery/ck.php?oaparams=1__bannerid=2__zoneid=3__cb=" . $r . "__maxdest=" . urlencode($feed["link"]) . "' target='_blank'>" .
            $codes[0]["location"] . " to " . $codes[1]["location"] .
            " $" . $feed["price"] . "</a>\n<p>" . $feed["title"] . "</p> <img src='http://ads.costtogo.com/www/delivery/lg.php?bannerid=2&campaignid=1&zoneid=3&channel_ids=,&loc=http%3A%2F%2Fdev03.pjaol.com%2Ftest%2Fkayak.html&cb=f3073f45f8' border='0' alt='' />";

        array_push($res, $l);
    }

    return $feed;
}

?>
