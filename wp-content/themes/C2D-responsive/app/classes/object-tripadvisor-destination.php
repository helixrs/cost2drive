<?php
//error_reporting(E_ALL);

	function get_curl_data_follow($url) {
		$ch = curl_init();
		$timeout = 30;
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
	function save_image_to_file($url, $file) {

		$ch = curl_init();
		$fp = fopen($file, 'wb');
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_exec($ch);
		curl_close($ch);
		fclose($fp);
	}

	class TripAdvisorDestination
	{

		public $taid;
		public $destPhotoURL;

		public function __construct($taid)
		{
			$this->taid = $taid;
			self::tripAdvisorDestPhoto();
		}

		public function __destruct()
		{

		}

		public function tripAdvisorDestPhoto() {

			$dir_path = dirname(__FILE__).'/../../' . 'images-trip-advisor/';

			$this->destPhotoURL = '';

			if (file_exists($dir_path . $this->taid.'.jpg') && filesize($dir_path . $this->taid.'.jpg') > 1024) {

				$this->destPhotoURL = 'http://'.$_SERVER['SERVER_NAME'].'/wp-content/themes/C2D-responsive/images-trip-advisor/'.$this->taid.'.jpg';

			} else {

				$contents = get_curl_data_follow('http://www.tripadvisor.com/Tourism-g'. $this->taid);

				$pattern = '/id="photoBox"(.*)\n(.*)\n(.*?)<img src="(.*?)"/';
				preg_match($pattern, $contents, $matches);
				$photo_url = $matches[4];

				if (strpos($photo_url, 'http') !== false) {

					save_image_to_file($photo_url, $dir_path.$this->taid.'.jpg');

				} else {

					// $pattern = '/id="fourPhotoBox"(.*)\n(.*)\n(.*?)<img src="(.*?)"/';
					$pattern = '/<img src="(http\:\/\/media(.*?))"/';
					preg_match($pattern, $contents, $matches);

					$photo_url = $matches[1];

					if (strpos($photo_url, 'http') !== false) {
						save_image_to_file($photo_url, $dir_path.$this->taid.'.jpg');
					}

				}


				if (strpos($photo_url, 'http') !== false) {
					$this->destPhotoURL = 'http://'.$_SERVER['SERVER_NAME'].'/wp-content/themes/C2D-responsive/images-trip-advisor/'.$this->taid.'.jpg';
				}

			}
		}


	}


?>
