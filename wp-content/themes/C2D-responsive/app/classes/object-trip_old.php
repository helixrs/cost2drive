<?php
	include("object-trip-steps.php");
	include("object-trip-legs.php");
	include("object-car.php");
	include("object-weather.php");
	include("google_maps_api.php");

	$now_at_dir = getcwd();
	chdir(realpath(dirname(__FILE__).'/../'));

	include("helpers/decodePolylineToArray.php");
	include("helpers/poly.php");
	include("helpers/conversion.php");
	include("helpers/opisnet.php");
	include("helpers/airport.php");

	chdir($now_at_dir);

class Trip extends DBO
    {
    	public $car;
    	public $start;
    	public $end;
    	public $waypoints;
    	public $polyline;
    	public $steps;
    	public $startPoint;
    	public $endPoint;
    	public $distance;	//in miles
    	public $duration;  //in seconds
    	public $arr;
    	public $legs;
    	public $cost;
    	public $totalGallons;
    	public $passengers;
    	public $stops;
    	public $kayakUrl;
    	public $kayakPrice;
    	public $googleURL;
    	public $weather;

    	public function __construct($carIn,$startIn,$endIn,$waypointsIn,$passengersIn=1)
    	{
    		parent::__construct();

    		$this->car = $carIn;
    		$this->start = $startIn;
    		$this->end = $endIn;
    		$this->waypoints = $waypointsIn;
    		$this->passengers = $passengersIn;
    		self::loadRoute();
    	}

    	public function __destruct()
    	{
    		//dont forget to call the parent dbclass destructor, its not called automatically by php
    		parent::__destruct();
    	}

 		public function loadRoute()
 		{
 			if(!isset($this->start) || !isset($this->end) )
 			{
 				return new ReturnReason(false, "Start or End address is not set");
 			}

 			//remove spaces for API call
 			$start = str_replace(" ","%20",$this->start);
			$end = str_replace(" ","%20",$this->end);

			$this->weather = new weather();
			$this->weather->location = $this->end;
			$this->weather->get();


			//initialize waypoints
			$waypoints="";
			$waypointsgoogle="";

			if (count($this->waypoints)>0)
			{
				$waypoints="&waypoints=";
				$waypointsgoogle="+to:";
				for ($i=0; $i<count($this->waypoints); $i++)
				{
					if ($i == (count($this->waypoints)-1) )
					{
						$waypoints .= str_replace(" ","%20",$this->waypoints[$i]);
						$waypointsgoogle .= str_replace(" ","%20",$this->waypoints[$i]);
					}
					else
					{
						$waypoints .= str_replace(" ","%20",$this->waypoints[$i]."|");
						$waypointsgoogle .= str_replace(" ","%20",$this->waypoints[$i]."+to:");
					}
				}
			}
			//Request the data from Google
			$request='http://maps.googleapis.com/maps/api/directions/json?origin='.$start.'&alternatives=false&units=imperial&destination='.$end.$waypoints.'&client=gme-c2gllc&sensor=false';
			$signedRequest=signUrl($request);
			//get the response of Google maps using PHP file_get_contents and then assign the response to $jsondata variable
			$jsondata = file_get_contents($signedRequest);

//$request = 'http://maps.googleapis.com/maps/api/directions/json?origin='.$start.'&alternatives=false&units=imperial&destination='.$end.$waypoints.'&sensor=false';
//$jsondata = file_get_contents($request);

			$this->googleURL = 'http://maps.google.com/?saddr='.$start.$waypointsgoogle."+to:".$end;


			//Convert the Google maps Json data to an array by using the function:
			$convertedtoarray = json_decode($jsondata);
			//initialize to 0
			$this->duration += 0;
			$this->distance += 0;

	        foreach($convertedtoarray->routes[0]->legs as $leg)
	        {
	        	$this->duration += $leg->duration->value;
				$this->distance += meters2miles($leg->distance->value);
	        }


			$this->start = $convertedtoarray->routes[0]->legs[0]->start_address;
			$this->startPoint = new Point($convertedtoarray->routes[0]->legs[0]->start_location->lat, $convertedtoarray->routes[0]->legs[0]->start_location->lng);

			$this->end  = $convertedtoarray->routes[0]->legs[count($convertedtoarray->routes[0]->legs)-1]->end_address;
			$this->endPoint = new Point($convertedtoarray->routes[0]->legs[count($convertedtoarray->routes[0]->legs)-1]->end_location->lat, $convertedtoarray->routes[0]->legs[count($convertedtoarray->routes[0]->legs)-1]->end_location->lng);

			//load the steps into an array of step objects
			for($i=0; $i<count($convertedtoarray->routes[0]->legs[0]->steps); $i++)
			{
				$this->steps[$i]= new TripStep($convertedtoarray->routes[0]->legs[0]->steps[$i]);
			}

			//decode the polyine into lag/lot points
			$points = decodePolylineToArray($convertedtoarray->routes[0]->overview_polyline->points);
			//conevert Array of Lat/log into Array of "Points"
			for($i=0; $i<count($points); $i++)
			{
				$this->polyline[$i] = new Point($points[$i][0],$points[$i][1]);
			}

 		}

 		public function calculateCost($milesToRefuel=0)
 		{
 			//set the MPG to use.
     		if ($this->distance > 15)
     		{
         		if ($this->distance > 25)
         		{
             		$avgMPG = $this->car->highwayMPG;
          		}
          		else
          		{
   	     			$avgMPG = $this->car->combinedMPG;
          		}
          	}
          	else
          	{
				$avgMPG = $this->car->cityMPG;;
          	}

 			//if milesToRefuel is not set, set to 90% of a tank
 			if ($milesToRefuel==0)
 			{
 				$milesToRefuel=$this->car->gallonsPerTank * .9 * $avgMPG;
 			}

 			//get the gas stops
 			$stops = $this->getGasStops($milesToRefuel);

			$trip_cost = 0;
			$origin =0;

			$milesToTravel = $this->distance;
			$milesCovered = 0;
			$driven = 0;
			$avgPrice = 0;
			$totalGals = 0;

			for ($i=0; $i < count($stops); $i++)
			{
				$gals_used;
				$leg_cost;

				if (!isset($stops[$i]["gas_price"]) || $stops[$i]["gas_price"]==0 )
				{
					//TO DO: hook up national gas price
		  			//$gasPrice = getNationalGasPrice();
		  			$gasPrice = "3.65";
				}
				else
				{
					$gasPrice = $stops[$i]["gas_price"];
				}

				if ($i == 0)
				{
					$milesCovered = $milesToRefuel;
					if ($milesCovered > $milesToTravel)
					{
						$milesCovered = $milesToTravel;
						$gals_used = $milesToTravel / $avgMPG;
					}
					else
					{
						$gals_used = $milesToRefuel / $avgMPG;
					}
				}
				else
				{
					if($i < (count($stops) - 1))
					{
						$gals_used = $milesToRefuel / $avgMPG;
						$milesCovered = $milesToRefuel;
						$milesToTravel -= $milesCovered;
					}
					else
					{
						$milesToTravel -= $milesToRefuel;
						$milesCovered = $milesToTravel;
						$lastGals = $milesToTravel / $avgMPG;
						$gals_used = $lastGals;
					}
				}

				$leg_cost = $gals_used * $gasPrice;

				$trip_cost += $leg_cost;
				$totalGals += $gals_used ;

				$this->legs[$i] = new TripLeg();

				$this->legs[$i]->location = $stops[$i]["county"]." Co., ".$stops[$i]["state"];
				$this->legs[$i]->distance = $milesCovered;
				$this->legs[$i]->gasPrice = $gasPrice;
				$this->legs[$i]->legCost = $leg_cost;
				$this->legs[$i]->gallons = $gals_used;

				//stations[i] = obj[i]["station"];
				$driven += $milesCovered;

			}

			$this->stops = $stops;
			$this->cost = $trip_cost;
			$this->totalGallons = $totalGals;
 		}

 		public function getCostPerPassenger()
 		{
 			if(isset($this->cost))
 			{
 				$returnValue = number_format($this->cost / $this->passengers,2);
 			}
 			else
 			{
 				$returnValue = "Error:  The cost has not been set";
 			}

 			return $returnValue;
 		}

 		public function getGasStops($milesToRefuel)
 		{
 			$distanceTraveled =0;
  			$step = 0;

  			while ($distanceTraveled < $this->distance)
  			{
				if ($step >  0 )
				{
        			$distanceTraveled += $milesToRefuel;
     			}

     			if ($distanceTraveled < $this->distance)
     			{
					$latLng = GetPointAtDistance($distanceTraveled, $this->polyline);
					$stops[$step] = $latLng;
					$step++;
     			}
  			}


     	   $stopDetails = getStopDetails($this->car->fuelType,$stops);

     		return $stopDetails;
  		}

  		public function getTolls()
  		{
  			$i=0;
  			foreach($this->steps as $step)
  			{
  				if (strpos(strtolower($step->htmlInstructions),"toll"))
  				{
  					$tolls[$i] = $step->htmlInstructions;
  					$i++;
  				}
  			}
  			return $tolls;
  		}

    	public function getKayak()
  		{
  			$stops[0] =$this->startPoint->lng.",".$this->startPoint->lat; //Manhattan, NY
  			$stops[1] =$this->endPoint->lng.",".$this->endPoint->lat; //Reston, VA
  			$feed = getKayakInfo($stops);
  			$this->kayakUrl = $feed["link"];
  			$this->kayakPrice = intval($feed["price"]);
  		}

  		public function getDrivingTime()
  		{

		    // start with a blank string
		    $hms = "";

		    $hours = intval(intval($this->duration) / 3600);

		    $minutes = intval(($this->duration / 60) % 60);

		    $seconds = intval($this->duration % 60);

		    if ($hours != 0)
		    {
		    	$hms .= $hours." hours ";
		    }
		    if ($minutes != 0)
		    {
		    	$hms .= $minutes." minutes ";
		    }

		    // done!
		    return $hms;
  		}

	}
?>
