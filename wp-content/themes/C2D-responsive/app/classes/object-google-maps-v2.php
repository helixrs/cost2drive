<?php
	include("object-address.php");

class C2DMap{

    public $mapWidth = 300;
    public $mapHeight = 300;
    public $apiKey = "";
    public $controlType = 'small';
    public $zoomLevel = 15;
    public $addresses;
    public $polyline;
    public $trip;

	public function __construct($trip)
    	{
    		$this->trip=$trip;
    	}

	function addAddress($address,$point)
	{
		if ( !isset($this->addresses) )
		{
			$this->addresses[0] = new Address($point,$address);
		}
		else
		{
			array_push($this->addresses, new Address($point,$address));
		}

	}

	//function printGoogleJS()
	//{
	//	echo "\n<script src=\"http://maps.google.com/maps/api/js?libraries=places&sensor=false\" type=\"text/javascript\"></script>\n";
	//}
	function printGoogleJS()
	{
		echo '<script src="http://maps.googleapis.com/maps/api/js?v=3&client=gme-c2gllc&sensor=false&libraries=places" type="text/javascript"></script>'."\n";
	}

	function showMap()
	{
		echo '<script type="text/javascript">
			 function initialize() {';

		echo '
			var latlng = new google.maps.LatLng('.$this->addresses[0]->point->lat.','.$this->addresses[0]->point->lng.');
			var latlngEnd = new google.maps.LatLng('.$this->addresses[count($this->addresses)-1]->point->lat.','.$this->addresses[count($this->addresses)-1]->point->lng.');
			var myOptions = {
      			zoom: 8,
      			center: latlng,
      			mapTypeId: google.maps.MapTypeId.ROADMAP
    		};
    		var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
			';


		echo 'marker_points(map);';


			echo '
		 	var polylinePoints = [';
		$pointTest = "";
		foreach($this->polyline as $point)
		{
    		$pointText .= 'new google.maps.LatLng('.$point->lat.','.$point->lng.'),';
		}

		//remove the extra comma
		$pointText = substr($pointText,0,strlen($pointText)-1);
  		echo $pointText;
  		echo '
  			];';
  		echo '
			var latlngbounds = new google.maps.LatLngBounds();
			for (var i = 0; i < polylinePoints.length; i++ )
			{
				latlngbounds.extend(polylinePoints[i]);
			}
			map.setCenter(latlngbounds.getCenter());
			map.fitBounds(latlngbounds);
			';
  		echo '
  			var drivePath = new google.maps.Polyline({
    		path: polylinePoints
  			});
  			drivePath.setMap(map);
			gas_points(map);
			toll_points(map);
			weather_points(map);
        	}</script>';

  	}

}

?>
