<?php

include("object-trip-steps.php");
include("object-trip-legs.php");
include("object-car.php");
//	include("object-weather.php");
include("google_maps_api.php");

$now_at_dir = getcwd();

chdir(realpath(dirname(__FILE__).'/../'));

include("helpers/decodePolylineToArray.php");

include("helpers/poly.php");
include("helpers/conversion.php");
include("helpers/opisnet.php");


include("helpers/airport.php");

chdir($now_at_dir);

class Trip extends DBO
{
	public $car;
	public $start;
	public $end;
	public $waypoints;
	public $waypointsList;
	public $waypointsListPoints;
	public $polyline;
	public $steps;
	public $startPoint;
	public $endPoint;
	public $distance;	//in miles
	public $duration;  //in seconds
	public $arr;
	public $legs;
	public $cost;
	public $totalGallons;
	public $passengers;
	public $stops;
	public $kayakUrl;
	public $kayakPrice;
	public $googleURL;
//    	public $weather;

	public $points;


	public static function CreateFromJSON($object) {
		$trip = new Trip();
		foreach (get_object_vars($object) as $key => $value) {
			$trip->$key = $value;
		}
		return $trip;
	}

	public function __construct($carIn = null,$startIn = null,$endIn = null,$waypointsIn = null,$passengersIn=1)
	{
		parent::__construct();
		$this->car = $carIn;
		$this->start = $startIn;
		$this->end = $endIn;
		$this->waypoints = $waypointsIn;
		$this->passengers = $passengersIn;
		//check if constructor is empty
		if (!empty($carIn)) {
			self::loadRoute();
		}
	}

	public function __destruct()
	{
		//dont forget to call the parent dbclass destructor, its not called automatically by php
		parent::__destruct();
	}

	public function loadRoute()
	{

		if(!isset($this->start) || !isset($this->end) )
		{
			return new ReturnReason(false, "Start or End address is not set");
		}

		if( $this->start == $this->end )
		{
			$this->duration = 0;
			$this->distance = 0;
			$this->cost = 0;
			$this->totalGallons =0;

			return new ReturnReason(false, "Start or End address are the same");
		}

		//remove spaces for API call
		$start = str_replace(" ","%20",$this->start);
		$end = str_replace(" ","%20",$this->end);

//			$this->weather = new weather();
//			$this->weather->location = $this->end;
//			$this->weather->get();


		//initialize waypoints
		$waypoints="";
		$waypointsgoogle="";

		if (count($this->waypoints)>0)
		{
			$waypoints="&waypoints=";
			$waypointsgoogle="+to:";
			for ($i=0; $i<count($this->waypoints); $i++)
			{
				if ($i == (count($this->waypoints)-1) )
				{
					$waypoints .= str_replace(" ","%20",$this->waypoints[$i]);
					$waypointsgoogle .= str_replace(" ","%20",$this->waypoints[$i]);
				}
				else
				{
					$waypoints .= str_replace(" ","%20",$this->waypoints[$i]."|");
					$waypointsgoogle .= str_replace(" ","%20",$this->waypoints[$i]."+to:");
				}
			}
		}
		//Request the data from Google
		$request='http://maps.googleapis.com/maps/api/directions/json?origin='.$start.'&alternatives=false&units=imperial&destination='.$end.$waypoints.'&client=gme-c2gllc&sensor=false';
		$signedRequest=signUrl($request);
		//get the response of Google maps using PHP file_get_contents and then assign the response to $jsondata variable
		$jsondata = file_get_contents($signedRequest);

//$request = 'http://maps.googleapis.com/maps/api/directions/json?origin='.$start.'&alternatives=false&units=imperial&destination='.$end.$waypoints.'&sensor=false';
//$jsondata = file_get_contents($request);

		$this->googleURL = 'http://maps.google.com/?saddr='.$start.$waypointsgoogle."+to:".$end;





		//Convert the Google maps Json data to an array by using the function:
		$convertedtoarray = json_decode($jsondata);
		//initialize to 0
		$this->duration = 0;
		$this->distance = 0;
		//	error_log($convertedtoarray);
//print "<!-- \n";
//print_r($convertedtoarray);
//print "\n -->";
		foreach($convertedtoarray->routes[0]->legs as $leg)
		{
			$this->duration += $leg->duration->value;
			$this->distance += meters2miles($leg->distance->value);
		}


		$this->start = $convertedtoarray->routes[0]->legs[0]->start_address;
		$this->startPoint = new Point($convertedtoarray->routes[0]->legs[0]->start_location->lat, $convertedtoarray->routes[0]->legs[0]->start_location->lng);

		$this->end  = $convertedtoarray->routes[0]->legs[count($convertedtoarray->routes[0]->legs)-1]->end_address;
		$this->endPoint = new Point($convertedtoarray->routes[0]->legs[count($convertedtoarray->routes[0]->legs)-1]->end_location->lat, $convertedtoarray->routes[0]->legs[count($convertedtoarray->routes[0]->legs)-1]->end_location->lng);


		// waypoints
		$this->waypointsList = array();
		$this->waypointsListPoints = array();
		$legs_count = count($convertedtoarray->routes[0]->legs);
		if ($legs_count > 1) {
			for ($i = 1; $i <= $legs_count ; $i++) {
				array_push($this->waypointsList, $convertedtoarray->routes[0]->legs[$i]->start_address);
				array_push($this->waypointsListPoints, new Point($convertedtoarray->routes[0]->legs[$i]->start_location->lat, $convertedtoarray->routes[0]->legs[$i]->start_location->lng));
			}
		}


		//load the steps into an array of step objects
		for($i=0; $i<count($convertedtoarray->routes[0]->legs[0]->steps); $i++)
		{
			$this->steps[$i]= new TripStep($convertedtoarray->routes[0]->legs[0]->steps[$i]);
		}

		//decode the polyine into lag/lot points
		$points = decodePolylineToArray($convertedtoarray->routes[0]->overview_polyline->points);

		$this->points = $points;

		//conevert Array of Lat/log into Array of "Points"
		for($i=0; $i<count($points); $i++)
		{
			$this->polyline[$i] = new Point($points[$i][0],$points[$i][1]);
		}

	}

	public function calculateCost($milesToRefuel=0)
	{
		if ($this->distance == 0)
		{
			return 0;
		}

		//set the MPG to use.
		if ($this->distance > 15)
		{
			if ($this->distance > 25)
			{
				$avgMPG = $this->car->highwayMPG;
			}
			else
			{
				$avgMPG = $this->car->combinedMPG;
			}
		}
		else
		{
			$avgMPG = $this->car->cityMPG;;
		}
		//if milesToRefuel is not set, set to 90% of a tank
		if ($milesToRefuel==0)
		{
			$milesToRefuel=$this->car->gallonsPerTank * .9 * $avgMPG;
		}

		//get the gas stops
		$stops = $this->getGasStops($milesToRefuel);
		$trip_cost = 0;
		$origin =0;

		$milesToTravel = $this->distance;
		$milesCovered = 0;
		$driven = 0;
		$avgPrice = 0;
		$totalGals = 0;

		for ($i=0; $i < count($stops); $i++)
		{
			$gals_used =0;
			$leg_cost = 0;

			if (!isset($stops[$i]["gas_price"]) || $stops[$i]["gas_price"]==0 )
			{
				//TO DO: hook up national gas price
				//$gasPrice = getNationalGasPrice();
				$gasPrice = "2.77";
			}
			else
			{
				$gasPrice = $stops[$i]["gas_price"];
			}

			if ($i == 0)
			{
				$milesCovered = $milesToRefuel;
				if ($milesCovered > $milesToTravel)
				{
					$milesCovered = $milesToTravel;
					$gals_used = $milesToTravel / $avgMPG;
				}
				else
				{
					$gals_used = $milesToRefuel / $avgMPG;
				}
			}
			else
			{
				if($i < (count($stops) - 1))
				{
					$gals_used = $milesToRefuel / $avgMPG;
					$milesCovered = $milesToRefuel;
					$milesToTravel -= $milesCovered;
				}
				else
				{
					$milesToTravel -= $milesToRefuel;
					$milesCovered = $milesToTravel;
					$lastGals = $milesToTravel / $avgMPG;
					$gals_used = $lastGals;
				}
			}

			$leg_cost = $gals_used * $gasPrice;

			$trip_cost += $leg_cost;
			$totalGals += $gals_used ;

			$this->legs[$i] = new TripLeg();

			$this->legs[$i]->location = $stops[$i]["county"]." Co., ".$stops[$i]["state"];
			$this->legs[$i]->distance = $milesCovered;
			$this->legs[$i]->gasPrice = $gasPrice;
			$this->legs[$i]->legCost = $leg_cost;
			$this->legs[$i]->gallons = $gals_used;

			//stations[i] = obj[i]["station"];
			$driven += $milesCovered;

		}

		$this->stops = $stops;
		$this->cost = $trip_cost;
		$this->totalGallons = $totalGals;
	}

	public function getCostPerPassenger()
	{
		if(isset($this->cost))
		{
			$returnValue = number_format($this->cost / $this->passengers,2);
		}
		else
		{
			$returnValue = "Error:  The cost has not been set";
		}

		return $returnValue;
	}

	public function getGasStops($milesToRefuel)
	{
		$distanceTraveled =0;
		$step = 0;
		while ($distanceTraveled < $this->distance)
		{
			if ($step >  0 )
			{
				$distanceTraveled += $milesToRefuel;
			}

			if ($distanceTraveled < $this->distance)
			{
				$latLng = GetPointAtDistance($distanceTraveled, $this->polyline);
				$stops[$step] = $latLng;
				$step++;
			}
		}

		$stopDetails = getStopDetails($this->car->fuelType,$stops);

		return $stopDetails;
	}

	public function getTolls()
	{
		$i=0;
		foreach($this->steps as $step)
		{
			if (strpos(strtolower($step->htmlInstructions),"toll"))
			{
//print "<!-- \n";
//print_r($step);
//print "\n -->";
				preg_match('/([a-zA-Z\s]*)[Tt]oll([a-zA-Z\s]*)/', $step->htmlInstructions, $matches);
				$toll_desc = $matches[0];

				$tolls[$i] = array(
					'instructions' => $step->htmlInstructions,
					'toll_desc' => $toll_desc,
					'point' => $step->startPoint,
				);
				$i++;
			}
		}
		return $tolls;
	}

	public function GetAWD() {

		$this->kayakPrice = 0;

		$stops[0] =$this->startPoint->lng.",".$this->startPoint->lat;
		$stops[1] =$this->endPoint->lng.",".$this->endPoint->lat;
		$airport = getAirport($stops);
		$ch = curl_init();
		$url = sprintf("http://awdapi.airfarewatchdog.com:8000/airfarewatchdog/fares/byRoute/%s?apikey=da49663a4e19458bc5d62801de682b0d&sourceId=63841&sortField=price",$airport[0]["code"].$airport[1]["code"]);
		#  echo "---- $req--\n";
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
		curl_setopt ($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($ch);

		$response = json_decode($response);

		if ($response) {
			$propertyName = $airport[0]["code"] . $airport[1]["code"];

			$minprice = 900000000;
			$minFare = null;

			foreach ($response->$propertyName as $res) {
				if ($res->price < $minprice) {
					$minprice = $res->price;
					$minFare = $res;
				}
			}
			//$result = reset($response->$propertyName);
			$result = $minFare;

			$this->kayakPrice = $result->price;
			$this->kayakUrl = $result->fareUrl;

			/*
                $html = "<div>";
                $html.= "Fly for ". $result->price . "$";
                $html.= "</div>";
                $html.="<div> <a href='" . $result->fareUrl ."'><img src='https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRv2herM2vHHC2qv3IVwZZQ69GWGPRuuRkw-x2BB9daNd7dM03fUSqv8ThG'</a></div>";
            */
			//return $html;
		}
		else {
			//return "";
		}
	}

	public function getAirport() {
		$stops[0] =$this->startPoint->lng.",".$this->startPoint->lat;
		$stops[1] =$this->endPoint->lng.",".$this->endPoint->lat;
		$airport = getAirport($stops);
		$ch = curl_init();
		$url = sprintf("http://awdapi.airfarewatchdog.com:8000/airfarewatchdog/fares/byRoute/%s?apikey=da49663a4e19458bc5d62801de682b0d&sourceId=63841",$airport[0]["code"].$airport[1]["code"]);
		#  echo "---- $req--\n";
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
		curl_setopt ($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);



		$response = curl_exec($ch);



		return $response;
	}

	public function getKayak()
	{
		$stops[0] =$this->startPoint->lng.",".$this->startPoint->lat; //Manhattan, NY
		$stops[1] =$this->endPoint->lng.",".$this->endPoint->lat; //Reston, VA
		$feed = getKayakInfo($stops);
		$this->kayakUrl = $feed["link"];
		$this->kayakPrice = intval($feed["price"]);
	}

	public function getDrivingTime()
	{

		// start with a blank string
		$hms = "";

		$hours = intval(intval($this->duration) / 3600);

		$minutes = intval(($this->duration / 60) % 60);

		$seconds = intval($this->duration % 60);

		if ($hours != 0)
		{
			$hms .= $hours." hours ";
		}
		if ($minutes != 0)
		{
			$hms .= $minutes." minutes ";
		}

		// done!
		return $hms;
	}

}
?>
