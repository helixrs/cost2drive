<?php 	
	/**
     * Encapsulates a boolean value and string message which is
     * useful for method return values with user readable error
     * messages.
     */
    class ReturnReason
    {
    	public $returnVal;
    	public $returnString;

    	function __construct($val, $string)
    	{
    		//we must provide for a default assignment since we cant
    		//over load our constructor yet you can still create an
    		//object without providing any parameters
    		$this->returnVal = true;
    		$this->returnString = "";
    		
    		$this->returnVal = $val;
    		$this->returnString = $string;	
    	}
    }
?>