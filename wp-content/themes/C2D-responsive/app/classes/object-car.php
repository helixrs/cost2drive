<?php
	include("object-return-reason.php");
	include("object-db.php");
	
  	class Car extends DBO
    {
    	public $id;
    	public $year;
    	public $mfr;
    	public $make;
    	public $fuelType;
    	public $highwayMPG;
    	public $cityMPG;
    	public $combinedMPG;
    	public $gallonsPerTank;
    	
    	public function __construct($idIn)
    	{	
    		parent::__construct();
    		if ($idIn!=-1)
    		{
    			self::GetFromDB($idIn);
    		}
    	}
    	
    	public function __destruct()
    	{
    		//dont forget to call the parent dbclass destructor, its not called automatically by php
    		parent::__destruct(); 
    	}
    	
    	public function LoadFromDbRow($dbRow)
    	{
    		$this->id = $dbRow["id"];
    		$this->year = $dbRow["year"];
    		$this->mfr = $dbRow["mfr"];
    		$this->make = $dbRow["car_line"];
    		$this->fuelType = $dbRow["fueltype"];
    		$this->highwayMPG = $dbRow["highway08"];
    		$this->cityMPG = $dbRow["city08"];
    		$this->combinedMPG = $dbRow["cmb08"];
    		$this->gallonsPerTank = $dbRow["tank_size"]; 
    	}
    	
    	public function GetFromDb($uid)
    	{
    		$this->id = $uid;
    		$query = "SELECT * FROM cars_import2 WHERE id = '$this->id' LIMIT 1";
			$ret = new ReturnReason(false,"Could not retrieve the car from the database.");    		
    		if($result = $this->runQuery($query))
	    	{
				if($row=mysql_fetch_array($result))
				{	
					$this->LoadFromDbRow($row);
					$ret = new ReturnReason(true, "");
				}
	    	}
	    	
			return $ret;
    	}
    	
    	public function setCustom($fuelType,$mpg,$gallonsPerTank)
    	{
    		$this->year =  "Custom";
    		$this->mfr = "Custom";
    		$this->make = "Custom";
        	$this->fuelType=$fuelType;
    		$this->highwayMPG=$mpg;
    		$this->cityMPG=$mpg;
    		$this->combinedMPG=$mpg;
    		$this->gallonsPerTank=$gallonsPerTank;
    	}
    	
    	

    } 
?>