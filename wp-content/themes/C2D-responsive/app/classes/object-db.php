<?php

class DBO
{	
	public function __construct()
	{
		$this->dbconnect();
		$this->xml=NULL;
	}
	
	public function __destruct()
	{
		@$this->dbclose();
	}
	
	private function dbconnect()  
	{


		//global $dbserver, $dbhost, $dbuser, $dbpass, $dbname;

        $dbhost = 'localhost:3306';
        $dbuser = 'app_user';
        $dbpass = 'cl0v3rl34f';

        //$conn = mysql_connect($dbhost, $dbuser, $dbpass) or die                      ('Error connecting to mysql');

        $dbname = 'c2d_data';

		$conn = mysql_connect($dbhost, $dbuser, $dbpass) or die('Error connecting to mysql');
		mysql_select_db($dbname);
	}
	
	public function dbclose()
	{
		mysql_close($conn);
	}
	
	/**
	 * Provides a way for classes extending this class to easily execute some sql code.
	 */
	protected function runQuery($sql)
	{

		$result = mysql_query($sql) or die('Error, query failed'.mysql_error());	
    	return $result;
	}
	
	public function getDistinctCarYears()
	{
		return self::runQuery("SELECT DISTINCT year FROM cars_import2 ORDER BY year DESC");
	}
	
	public function getCarMfrByYear($year)
	{
		return self::runQuery("SELECT DISTINCT mfr FROM cars_import2 WHERE year='$year' ORDER BY mfr ASC");
	}
	
	public function getCarMakeByYearAndMfr($year,$make)
	{
		return self::runQuery("SELECT car_line, max(mpg) AS mpg, max(city08) AS city08, max(highway08) AS highway08, max(cmb08) AS cmb08, fueltype, max(tank_size) AS tank_size FROM cars_import2 WHERE mfr='$make' AND year='$year' GROUP BY car_line, fueltype ORDER BY car_line ASC");
	}


	/*
	 * Cleans the data from "bad" strings.  This was taking from db_conn of the original implementation
	 */
    public function cleanData ($data) 
    {
		$badStrs[0] = '/\-\-+/';
  		$badStrs[1] = '/\*+/';
  		$badStrs[2] = '/\'+/';
  		$badStrs[3] = '/\"+/';

  		$data = preg_replace($badStrs, "", $data);
  		$data = mysql_real_escape_string($data);

  		return $data;
	}
}
?>