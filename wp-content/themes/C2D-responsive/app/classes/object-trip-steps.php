<?php
	include("object-point.php");
	
  class TripStep
    {
    	public $distance; //in meters
    	public $duration; //in seconds
    	public $htmlInstructions;
    	public $startPoint;
    	public $endPoint;
    	
    	public function __construct($googleMapsArray)
    	{	
    		$this->distance = $googleMapsArray->distance->value;
    		$this->duration = $googleMapsArray->duration->value;
    		$this->htmlInstructions = $googleMapsArray->html_instructions;
    		$this->startPoint = new Point($googleMapsArray->start_location->lat,$googleMapsArray->start_location->lng);
    		$this->endPoint = new Point($googleMapsArray->end_location->lat,$googleMapsArray->end_location->lng);
    	}
    	
    	public function __destruct()
    	{
    	}
    	
    } 
?>