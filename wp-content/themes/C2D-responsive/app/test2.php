<?php
include("decodePolylineToArray.php");
include("poly.php");

//The request URL can be defined in the PHP variable

$start = "Washington DC";
$end = "New York";

$start = str_replace(" ","%20",$start);
$end = str_replace(" ","%20",$end);

$request='http://maps.googleapis.com/maps/api/directions/json?origin='.$start.'&alternatives=false&units=imperial&destination='.$end.'&sensor=false';

//get the response of Google maps using PHP file_get_contents and then assign the response to $jsondata variable

$jsondata = file_get_contents($request);

//Convert the Google maps Json data to an array by using the function:

$convertedtoarray = json_decode($jsondata);

print_r($convertedtoarray->routes[0]->legs[0]->steps);

$start_address = $convertedtoarray->routes[0]->legs[0]->start_address;
$start_addressLat = $convertedtoarray->routes[0]->legs[0]->start_location->lat;
$start_addressLng = $convertedtoarray->routes[0]->legs[0]->start_location->lng;

$end_address = $convertedtoarray->routes[0]->legs[0]->end_address;
$end_addressLat = $convertedtoarray->routes[0]->legs[0]->end_location->lat;
$end_addressLng = $convertedtoarray->routes[0]->legs[0]->end_location->lng;


//decode the polyine into lag/lot points
$points = decodePolylineToArray($convertedtoarray->routes[0]->overview_polyline->points);

//print_r($points);

//var_dump($convertedtoarray->routes[0]->overview_polyline->points);

$distancePoint = GetPointAtDistance(100,$points);

require_once 'phoogle.php';
$map = new PhoogleMap();
$map->setAPIKey("ABQIAAAAex1HzIguHP1nUi7ayG8s1RSWEt1ZVDNA0PO8tqDbzWT7JFoaERTkHb8bAVfYviCi-r2IIbi1q0ps6g");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <? $map->printGoogleJS(); ?>
   </head>
  <body>
  <?php
  $map->addAddress($start_address,$start_addressLat,$start_addressLng);
  $map->addAddress($end_address,$end_addressLat,$end_addressLng);
  $map->showValidPoints("table","points");
  $map->showMap();
  ?>
    </body>
</html>
<?php


//echo print_r($convertedtoarray);
/*
$i=0;
$miles=20;
$dist=0;
$olddist=0;

foreach($convertedtoarray['routes']['0']['legs']['0']['steps'] as $step)
{
	$i++;
	echo 'lat : '.$step['start_location']['lat'].'\n';
	echo 'distance : '.$step['distance']['value'].'\n';
	
}

//echo print_r($convertedtoarray['routes']['0']['legs']['0']['steps']['0']['start_location']['lat']);
$totalDistance = substr($convertedtoarray['routes']['0']['legs']['0']['distance']['text'],0,strlen($convertedtoarray['routes']['0']['legs']['0']['distance']['text'])-3);

print_r($convertedtoarray);
//For testing purposes, you can dump the entire array data and then paste it to your favorite PHP editor for troubleshooting purposes:
echo "Therefore the driving distance from New York to New Jersey is around: ".$totalDistance;

echo "\n";
*/
//var_dump($convertedtoarray);
?>