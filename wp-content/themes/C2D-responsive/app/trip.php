<?php
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
//seconds * minutes * hours * days + current time
$inTwoMonths = 60 * 60 * 24 * 60 + time(); 
setcookie('start', $_GET["start"], $inTwoMonths); 
setcookie('end', $_GET["end"], $inTwoMonths); 
setcookie('carId', $_GET["carId"], $inTwoMonths); 

include("include/globals.php"); 
include("classes/object-trip.php");
//? include("app/classes/object-car.php");
include("classes/object-google-maps.php");
//Calculate 60 days in the futures

if ($_GET["custom"]=="false")
{
	$car = new Car($_GET["carId"]);
	echo "cool";
}
else
{
	$car = new Car(13967);
	echo "huh";
}
	
$trip = new Trip($car,$_GET["start"],$_GET["end"],$_GET["waypoints"],$_GET["passengers"]);
$trip->calculateCost();
$map = new C2DMap();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <?php 
  
  	$map->printGoogleJS(); 
  	$map->addAddress($trip->start,$trip->startPoint);
  	$map->addAddress($trip->end,$trip->endPoint);
  	$map->polyline=$trip->polyline;
  	
  	$map->showMap();?>
   </head>
  <body onload="initialize()">
  <?php

	echo "The Cost to drive a test".$car->year." ".$car->mfr." ".$car->make;
	echo " From ".$trip->start." To ".$trip->end;
	echo ": <h2>$".number_format($trip->cost,2)."</h2>";
	
	if ($trip->passengers > 1 )
	{
		echo "<br>The cost per passenger: <h3>$".$trip->getCostPerPassenger()."</h3>";
	}
	
	echo '<div id="map_canvas" style="width: 500px; height: 400px;"></div>';

  
	$tolls = $trip->getTolls();
	
	if (count($tolls) > 0)
	{
		echo "<br>The toll roads include the following: <br>";
		foreach($tolls as $toll)
		{
			print_r($toll);
		}
	}
	
	echo "<br>Refueling Points: ";
	
	echo "<table>";
	echo "<tr>";
	echo "<td></td>";
	echo "<td>Miles Driven</td>";
	echo "<td>Gallons</td>";
	echo "<td>Gas Price</td>";
	echo "<td>Cost</td>";
	echo "</tr>";
	foreach($trip->legs as $leg)
	{
		echo "<tr>";
		echo "<td>".$leg->location."</td>";
		echo "<td>".number_format($leg->distance,1)."</td>";
		echo "<td>".number_format($leg->gallons,1)."</td>";
		echo "<td>".number_format($leg->gasPrice,2)."</td>";
		echo "<td>".number_format($leg->legCost,2)."</td>";
		echo "</tr>";
	}
	
	echo "</table>";
	
	echo "<br>Total cost: ".number_format($trip->cost,2);
	echo "<br>Total gallons: ".number_format($trip->totalGallons,1);
	

?>
    </body>
</html>