<?php
$db = new DBO();
if(isset($_COOKIE['carId']))
    $car = new Car($_COOKIE['carId']);
?>
    <h2 id="what_car">What Vehicle Are You Driving?</h2>

    <div id="trucks"  class="fr-wrp fr-wrp-from container">
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6"><label for="mpg">MPG</label></div>
                    <div class="col-md-6"><input type="text" name="mpg" id="mpg" size="5"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6"><label for="gpt">Tank Size (Gallons) </label></div>
                    <div class="col-md-6"><input type="text" name="gpt" id="gpt" size="5"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6"><label for="fuelType">Fuel Type</label></div>
                    <div class="col-md-6">
                        <select name="fuelType" id="fuelType">
                            <option value="Diesel">Diesel</option>
                            <option value="Regular">Regular</option>
                            <option value="Premium">Premium</option>
                        </select>
                        <input type="hidden" name="truck" id="truck"  value="true">
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
if (isset($car))
{
    echo '<input type="hidden" value ="'.$car->id.'" name="carId" id="carId">';
}
else
{
    echo '<input type="hidden" name="carId" id="carId">';
}
?>