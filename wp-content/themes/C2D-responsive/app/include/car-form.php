<script type="text/javascript">
function showCarsForm()
{

	jQuery(document).ready(function($) {
		if ($('#custom').val() == "false"){
			$('#carf').hide();
			$('#customcar').show();
			$('#custom').val('true');
		}
	else{
		$('#carf').show();
		$('#customcar').hide();
		$('#custom').val('false');
	}
		 });
}
</script>

<?php
$db = new DBO();
if(isset($_COOKIE['carId']))
	$car = new Car($_COOKIE['carId']);
?>
	<h2 id="what_car">What Car Are You Driving?</h2>
	<a onclick="showCarsForm();" href="#" class="top_link">Can't find your car?</a>
    <div class="car-select-wrapper">
	<div id ="carf" class="selects row">
		<fieldset id="year_fs" class="col-md-4">
		
			<select id="year_c" name="year_c">
				<option>-Select Year-</option>
<?php
	//get all of the distinct years for cars
	$result = $db->getDistinctCarYears();
	
	//load up the selects with all of the vailable years
	while($row = mysql_fetch_array($result))
	{ 
		if ($car->year == $row["year"])
		{
   			echo "\t\t\t<option selected>".$row["year"]."</option>\n";
		}
		else
		{
			echo "\t\t\t<option>".$row["year"]."</option>\n";
		}
	}

?>
			</select>
		</fieldset>
		<fieldset id="make_fs" class="col-md-4">
			<select id="make" name="make" <?php if (!isset($car)) { ?>disabled="true" <?php }?>>
				<option>-Select Make-</option>
<?php 
	if (isset($car)) 
	{
		$mfrs = $db->getCarMfrByYear($car->year);
		while($row = mysql_fetch_array($mfrs))
		{ 
			if ( strtolower($car->mfr) != strtolower($row['mfr']) )
			{
		   		echo "\t\t\t<option>".ucwords($row['mfr'])."</option>\n";
			}
			else
			{
				echo "\t\t\t<option selected >".ucwords($row['mfr'])."</option>\n";
			}
		}
	}
?>
			</select>
		</fieldset>
		<fieldset id="model_fs" class="col-md-4">
			<select id="model" name="model" <?php if (!isset($car)) { ?>disabled="true" <?php }?>>
				<option>-Select Model-</option>
<?php 
	if (isset($car)) 
	{
		$models = $db->getCarMakeByYearAndMfr($car->year,$car->mfr);
		while($row = mysql_fetch_array($models))
		{ 
			if ( strtolower($car->make) != strtolower($row['car_line']) )
			{
		   		echo "\t\t\t<option>".ucwords($row['car_line'])."(".$car->fuelType.")"."</option>\n";
			}
			else
			{
				echo "\t\t\t<option selected >".ucwords($row['car_line'])."(".$car->fuelType.")"."</option>\n";
			}
		}
	}
?>
			</select>
		</fieldset>
	</div>
    </div>

	<div id="customcar"  class="fr-wrp fr-wrp-from container" style="display: none">
		<div class="row">
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-6"><label for="mpg">MPG</label></div>
					<div class="col-md-6"><input type="text" name="mpg" id="mpg" size="5"></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-6"><label for="gpt">Tank Size (Gallons) </label></div>
					<div class="col-md-6"><input type="text" name="gpt" id="gpt" size="5"></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-6"><label for="fuelType">Fuel Type</label></div>
					<div class="col-md-6">
						<select name="fuelType" id="fuelType">
							<option value="Regular">Regular</option>
							<option value="Premium">Premium</option>
							<option value="Diesel">Diesel</option>
						</select>
						<input type="hidden" name="custom" id="custom"  value="false">
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
	if (isset($car)) 
	{
   		echo '<input type="hidden" value ="'.$car->id.'" name="carId" id="carId">';
	}
	else
	{
		echo '<input type="hidden" name="carId" id="carId">';
	}
?>