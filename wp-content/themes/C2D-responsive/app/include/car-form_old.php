<?php
$db = new DBO();
if(isset($_COOKIE['carId']))
	$car = new Car($_COOKIE['carId']);
?>
	
	<h2 id="what_car">What Car Are You Driving?</h2>
	<a onclick="showCarsForm();" href="#" class="top_link tmp_hidden">Can't find your car?</a>
	<div class="selects">
		<fieldset id="year_fs">
		
			<select id="year_c" name="year_c">
				<option>-Select Year-</option>
<?php
	//get all of the distinct years for cars
	$result = $db->getDistinctCarYears();
	
	// var_dump($result);
	//load up the selects with all of the vailable years
	while($row = mysql_fetch_array($result))
	{ 
		if ($car->year == $row["year"])
		{
   			echo "\t\t\t<option selected>".$row["year"]."</option>\n";
		}
		else
		{
			echo "\t\t\t<option>".$row["year"]."</option>\n";
		}
	}

?>
			</select>
		</fieldset>
		<fieldset id="make_fs">
			<select id="make" name="make" <?php if (!isset($car)) { ?>disabled="true" <?php }?>>
				<option>-Select Make-</option>
<?php 
	if (isset($car)) 
	{
		$mfrs = $db->getCarMfrByYear($car->year);
		while($row = mysql_fetch_array($mfrs))
		{ 
			if ( strtolower($car->mfr) != strtolower($row['mfr']) )
			{
		   		echo "\t\t\t<option>".ucwords($row['mfr'])."</option>\n";
			}
			else
			{
				echo "\t\t\t<option selected >".ucwords($row['mfr'])."</option>\n";
			}
		}
	}
?>
			</select>
		</fieldset>
		<fieldset id="model_fs">
			<select id="model" name="model" <?php if (!isset($car)) { ?>disabled="true" <?php }?>>
				<option>-Select Model-</option>
<?php 
	if (isset($car)) 
	{
		$models = $db->getCarMakeByYearAndMfr($car->year,$car->mfr);
		while($row = mysql_fetch_array($models))
		{ 
			if ( strtolower($car->make) != strtolower($row['car_line']) )
			{
		   		echo "\t\t\t<option>".ucwords($row['car_line'])."(".$car->fuelType.")"."</option>\n";
			}
			else
			{
				echo "\t\t\t<option selected >".ucwords($row['car_line'])."(".$car->fuelType.")"."</option>\n";
			}
		}
	}
?>
			</select>
		</fieldset>
	</div>

<?php
	if (isset($car)) 
	{
   		echo '<input type="hidden" value ="'.$car->id.'" name="carId" id="carId">';
	}
	else
	{
		echo '<input type="hidden" name="carId" id="carId">';
	}
?>