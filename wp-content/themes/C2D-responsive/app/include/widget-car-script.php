<script type="text/javascript" charset="utf-8">
jQuery(document).ready(function($){
  $("select#year_c").change(function(){
    $.getJSON("http://beta.costtodrive.com/wp-content/themes/C2D/app/cars_ajax_v3.php",{year_c: $(this).val(), ajax: 'true'}, function(j){
      var options = '<option>- Make -</option>';
      for (var i = 0; i < j.length; i++) {
        if( j[i] ) {
          options += '<option value="' + j[i].mfr + '">' + j[i].mfr + '</option>';
        }
      }
      $("select#make").html(options);
      $("select#make").removeAttr('disabled');
    })
  })

  $("select#make").change(function(){
    carscache = new Array();

    $.getJSON("http://beta.costtodrive.com/wp-content/themes/C2D/app/cars_ajax_v2.php",{make: $(this).val(), year_c:$("select#year_c").val(), ajax: 'true'}, function(j){
      var options = '<option>- Model -</option>';
      for (var i = 0; i < j.length; i++) {
        if ( j[i] ) {
          carscache[i] = j[i];
          var fuel ="Reg";
          if (j[i].fueltype == "Premium") {
              fuel ="Pre";
          } else if (j[i].fueltype == "Diesel") {
              fuel = "Dies";
          }
          options += '<option value="' + j[i].mpg+","+ j[i].tank_size +","+j[i].car_line + '">' + j[i].car_line +'('+fuel+')</option>';
        }
      }
      $("select#model").html(options);
      $("select#model").removeAttr('disabled');
    })
  })

  $("select#model").change(function(){
      var i = this.selectedIndex - 1;
      //      alert(dump(carscache[i] ));
      $("#car_type").val('');
      $("#mpg").val(carscache[i].mpg);
      $("#gpt").val(carscache[i].tank_size);
      $("#fuel").val(carscache[i].fueltype);
      $("#city").val(carscache[i].city08);
      $("#highway").val(carscache[i].highway08);
      $("#cmb").val(carscache[i].cmb08);
      $("#carId").val(carscache[i].id);
      $("#c2d_submit").removeAttr('disabled');
  })
})

</script>
