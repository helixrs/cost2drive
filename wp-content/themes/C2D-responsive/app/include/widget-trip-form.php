<script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=places&sensor=false"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
	var input = document.getElementById('start');
	autocomplete = new google.maps.places.Autocomplete(input);
	input = document.getElementById('end');
	autocomplete2 = new google.maps.places.Autocomplete(input);
});
</script>
<img src="<?php print get_bloginfo('template_url'); ?>/images/structural/c2d_widget_logo.png" alt="Cost To Drive" title="Cost To Drive" />
<h2 id="where_to">Calculate Cost To Drive</h2>
<fieldset class="ft">
	<div class="fr-wrp fr-wrp-from">
		<label for="start">From</label>
		<input type="text" id="start" name="start" value="Enter Address, City, State or Zip" />
	</div>
	<div class="fr-wrp">
		<label for="end">To</label>
		<input id="end" class="<?php print (isset($_GET['to'])) ? 'populated' : '' ?>" type="text" name="end" value="<?php print (isset($_GET['to'])) ? str_replace('-', ' ', $_GET['to']) : 'Enter Address, City, State or Zip'; ?>" />
	</div>
</fieldset>
