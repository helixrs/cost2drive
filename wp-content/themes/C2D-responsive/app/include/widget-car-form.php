<?php
$db = new DBO();
?>
	<h3 id="what_car">Select Car</h3>
	<div id ="carf" class="selects">
		<fieldset id="year_fs">
		
			<select id="year_c" name="year_c">
				<option>- Year -</option>
<?php
	//get all of the distinct years for cars
	$result = $db->getDistinctCarYears();
	
	//load up the selects with all of the vailable years
	while($row = mysql_fetch_array($result))
	{ 
		echo "\t\t\t<option>".$row["year"]."</option>\n";
	}

?>
			</select>
		</fieldset>
		<fieldset id="make_fs">
			<select id="make" name="make" disabled="true">
				<option>- Make -</option>
			</select>
		</fieldset>
		<fieldset id="model_fs">
			<select id="model" name="model" disabled="true">
				<option>- Model -</option>
			</select>
		</fieldset>
	</div>
	


	<input type="hidden" name="carId" id="carId">

