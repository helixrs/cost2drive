<?php
include("../classes/object-db.php");
include("globals.php");


$year = $_GET['year_c'];
$make = $_GET['make'];
$model = $_GET['model'];
$callback =$_GET['callback'];
$db = new DBO();

if (isset($year) && (! isset($make))) 
{
	$result = $db->getCarMfrByYear($year);
	
} elseif (isset($make)){

	$result = $db->getCarMakeByYearAndMfr($year,$make);
} else {
  return;
} 

if(strlen($callback) >0) {
  echo "$callback (";
}
echo "[";

while ($row = mysql_fetch_array($result)) {
  $str ="{";
  $sz = sizeof($row);
  $i=0;
  foreach ($row as $k => $v) {
    $i++;
    if ($k) {
      $str .= "$k : '".ucwords(strtolower($v))."'";
    
      if ($i < $sz) {
	$str .=",";
      }
    }
    
  }
  $str .= "},";
  echo $str;
}

echo "]";

if (strlen($callback) > 0) {
 echo ");";
}
?>
