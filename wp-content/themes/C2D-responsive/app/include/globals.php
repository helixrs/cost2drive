<?php
session_start();

$TEST_MODE = false;


// PRODUCTION SETTINGS
$base_href = "http://www.costtodrive.com/dev";
$dbserver = "localhost:3306";
$dbuser = "root";
$dbpass = "";
$dbname = "c2d_data";


// TODO: Return false for Production
function testmode() {
	global $TEST_MODE;
    return $TEST_MODE;
}

// OVERRIDE PRODUCTION SETTINGS IF WE ARE IN TEST MODE
if (testmode()) {

	// TEST MODE SETTINGS
}   
    
	

	/* AP - Dynamically switches global variables based on which environment its called from */
	if ($_SERVER["SERVER_NAME"] == "localhost") {
	    $base_dir = "http://localhost:8888";
   		$base_dir_relative = "/";
   		$base_href = "http://localhost:8888/index.php";
	}




function getRequestURI() {
	return $_SERVER['REQUEST_URI'];
}

/**
 * Checks the post object for the specified index and returns the value
 * it finds or null if the index is not valid.
 * 
 * @param $postIndexName	the string name used as the index into the post object.
 * 
 * @return 	the value of the post object at the specified index if it was valid,
 * 			null if there was nothing at that index.
 */
function GetSafeVarFromPost($postIndexName)
{
 	if (true==isset($_REQUEST[$postIndexName]))
  		return $_REQUEST[$postIndexName];
   	else
   		return null;
}

?>