<script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=places&sensor=false"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
	var input = document.getElementById('start');
	autocomplete = new google.maps.places.Autocomplete(input);
	input = document.getElementById('end');
	autocomplete2 = new google.maps.places.Autocomplete(input);

	var counter = 1;
	var acindex = 1;
	var limit = 5;

	// add a stop
	$('.adds-a:not(.disabled), .adds-first-add-btn').live('click', function(e){
		var newStop = '<div  class="waypoint-wrp"><input type="text" class="waypoint" id ="waypoint' + acindex + '" name="waypoints[]">';
		newStop += '<span class="adds-a" title="Add a stop">+</span>';
		newStop += '<span class="adds-r" title="Remove this stop">-</span></div>';
		var firstBtn = $(e.currentTarget).hasClass('adds-first-btn')
		if(!firstBtn) {
			// one of generated add buttons was clicked
			$(this).parents('.waypoint-wrp').after(newStop);
		} else {
			// the first add button was clicked
			// add the new stop as the first in the .fr-wrp-adds and hide this button
			$('.fr-wrp-adds').prepend(newStop);
			$(this).hide();
		}
		input2 = document.getElementById('waypoint'+acindex);
		autocomplete = new google.maps.places.Autocomplete(input2);
		counter++;
		acindex++;
		if (counter == limit)  {
			$('.adds-a').addClass('disabled');
		}
	});

	// remove a stop
	$('.adds-r').live('click', function(){
		$(this).parents('.waypoint-wrp').remove();
		$('.adds-a').removeClass('disabled');
		counter--;
		if(counter == 1) {
			$('.adds-first-btn').show();
		}
	})
});
</script>

<h2 id="where_to">Where Are You Going?</h2>
<fieldset class="ft">
	<div class="fr-wrp fr-wrp-from form-group">
		<label for="from" class="col-sm-2">From</label>
			<div class="col-sm-10">
			<input type="text" id="start" name="start" <?php if(isset($_COOKIE['start'])) echo 'value="'.$_COOKIE['start'].'"'; ?> value="Enter Address, City, State or Zip" />
		</div>
	</div>
	<div class="first-add-button form-group">
		<span class="adds-a adds-first-btn"><span class="adds-a-plus">+</span>Add a stop</span>
	</div>
	<div class="fr-wrp fr-wrp-adds form-group"></div>
	<div class="fr-wrp form-group">
		<label for="to" class="col-sm-2">To</label>
		<div class="col-sm-10">
			<input id="end" type="text" name="end" value="<?php print $location_title; ?>" />
		</div>
	</div>
	<div class="fr-wrp fr-wrp-to form-group">
		<label for="to" class="wide-label col-sm-4">Passengers</label>
		<div class="col-sm-8">
			<select id="passengers" name="passengers">
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
				<option value="8">8</option>
				<option value="9">9</option>
				<option value="10">10</option>
			</select>
		</div>
	</div>
</fieldset>
