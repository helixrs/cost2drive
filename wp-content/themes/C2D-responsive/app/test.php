<?php
	include("include/globals.php"); 
	include("classes/object-car.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Cost To Drive</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="description" content="Cost to drive provides  insight into the cost of a driving trip including gas prices"/>
<meta name="keywords" content="cost to drive, driving, price, gas price, economize, trip, galculator "/>
<script src="../js/jquery-1.2.6.min.js"></script>
<script src="../js/jquery.inputplaceholder.js"></script>
<script src="../js/jquery.bgiframe.js"></script>
<script src="../js/ui.js"></script>
</head>
<body>
<div id="debug"></div>
 <form id="where_to_f"  action="trip.php" method="get">
 <?php include("include/car-form.php");?>
  <?php include("include/trip-form.php");?>
  <input type="submit" value="Submit" />
 </form>

 <?php include("include/car-script.php"); ?>
</body>
</html>
