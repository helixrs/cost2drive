directions = new GDirections();
var startP;
var endP;
var stationsWhere = 0;
var stationsStop = false;
var stationsTimeId;

GEvent.addListener(directions,"load", function() {

  poly=directions.getPolyline();
  
  var route = directions.getRoute(0);

  //var ht = getRouteDetails(route) + route.getSummaryHtml();
  meters = route.getDistance().meters;
  miles = meters2miles(meters);

  var duration_sec = route.getDuration().seconds / 3600;
  var route_seconds = route.getDuration().seconds;
  
  var duration_hrs = Math.floor(duration_sec);
  var duration_mins = Math.floor((route_seconds % 3600) / 60);

  // backwards compat mode
  if ( city && highway && cmb ) {
     var driveType = "City ";
     if (miles >15) {

         if (miles > 25) {
             driveType = "Highway ";
             avgMPG = highway;
          } else {
             driveType = "Combined ";
   	     avgMPG = cmb;
          }

     } else {
	avgMPG = city;
     }
     document.getElementById("avgMPG_details").innerHTML = "<span>Mileage per Gallon</span> "+driveType+ avgMPG;
  }

  
  if (miles > 200) {
       showFlights = true;
  }
  startP =poly.getVertex(0);
  endP = poly.getVertex(poly.getVertexCount()-1);

  var c2d = ((miles / avgMPG) * nationalPPG);


  var td = "<span>Total Distance</span> "+Math.ceil(miles);
  var tt = "<span>Driving Time</span> "+ duration_hrs+" hrs "+duration_mins+" mins";

  document.getElementById("t_distance").innerHTML = td;
  document.getElementById("t_time").innerHTML = tt;

  getGasStops();
});

GEvent.addListener(directions,"error", function() {
  //alert("Location(s) not recognised. Code: "+directions.getStatus().code);
  displayTripWithInfo("<font color='red' size='3'>Please check your locations, we were unable to identify one of them</font>");
});


function createC2DMap () {

  map.setCenter(poly.getVertex(0),15);
  map.addOverlay(new GMarker(poly.getVertex(0),G_START_ICON));
  map.addOverlay(new GMarker(poly.getVertex(poly.getVertexCount()-1),G_END_ICON));
  marker = new GMarker(poly.getVertex(0));
  map.addOverlay(marker);
  map.addOverlay(poly);
  bounds = poly.getBounds();
  map.setZoom(map.getBoundsZoomLevel(bounds));
  map.setCenter(bounds.getCenter());

  var sz = stops.length;
  for (var i = 0; i < sz; i++) {

	markers[i] = new GMarker(new GPoint(stops[i][0], stops[i][1]));
	markers[i].c2d_id = i;
        GEvent.addListener(markers[i], "click", function() {
	        getStation(this);
	});

	map.addOverlay(markers[i]);
  }

}

function getRouteDetails( route ) {

  var data ="";
  var steps = route.getNumSteps() +1;
  for (var i=0; i < steps; i++ ) {

    if (i < 4) {
      var step = route.getStep(i);
      data += step.getDescriptionHtml() +"<br/>";
    } else if (i == (steps -1)) {
      data += "....................<br/>";
      data += step.getDescriptionHtml() +"<br/>";
    }
  }

  return data;

}


function displayGasStops(obj) {

	var sz =obj.length;
	var info= "";
	var milesPF = Math.floor( avgMPG * galsPT);

	var trip_cost = 0;
	var origin =0;

	var milesToTravel = miles;
	var milesCovered = 0;
	var driven = 0;
	var avgPrice = 0;
	var totalGals = 0;
	
	for (var i =0 ; i < sz; i++) {

		var gas_price = Math.round(obj[i]["gas_price"] * 100) / 100;
		var gals_used;
		gas_price = gas_price.toFixed(2);
		var leg_cost;
		var gp_calc = gas_price;
		
		if (gas_price == 0) {
		  gas_price = "no data <sup>org</sup>";
		  if (origin == 0) {
			gas_price = "no data <sup>nat avg</sup>";
			gp_calc = national_avg * 1;
			
	          } else {
		  	gp_calc = origin;
		  }
		  avgPrice += (gp_calc *1);
		} else {
			avgPrice += (gp_calc *1);
			if (i == 0) {
				origin = gp_calc;
			}
		}


		if ( i == 0 ) {
	
			milesCovered = avgMPG * galsPT90;
			if (milesCovered > miles) {
				milesCovered = miles;
				gals_used = (miles / avgMPG);
				leg_cost = Math.round( gp_calc * gals_used * 100) / 100;
				gals_used = gals_used.toFixed(2);
			} else {
				leg_cost = Math.round(gp_calc * galsPT * 100 ) / 100;
				gals_used = galsPT;
			}
			
			
		} else if(i < (sz -1)) {
			leg_cost = Math.round(gp_calc * galsPT90 * 100 ) / 100;
			gals_used = galsPT90.toFixed(2);
			milesToTravel -= Math.floor((avgMPG * galsPT90));
			milesCovered = Math.floor((avgMPG * galsPT90)); 
		} else {
			milesToTravel -= Math.floor((avgMPG * galsPT90));
			milesCovered = milesToTravel; //Math.floor((avgMPG * galsPT90)); 

			var lastGals = (milesToTravel / avgMPG);
			leg_cost = Math.round(gp_calc * lastGals * 100 ) / 100;
			gals_used = lastGals.toFixed(2);
		}

		leg_cost = leg_cost.toFixed(2);

	       
		trip_cost = ((trip_cost *1) + (leg_cost *1));
		totalGals += (gals_used * 1);

		info+="<tr id='gas_t_p' >";
		info+= "<td><a href='#' onMouseOver='GEvent.trigger(markers["+i+"],\"click\")'>" +obj[i]["county"] + " Co., " + obj[i]["state"] +"</a></td>\n";
		info+= "<td>"+Math.floor(driven) +"</td>\n";
		info+= "<td>"+ gals_used+"</td>\n";
		info+= "<td>$"+ gas_price +"</td>\n";
		info+= "<td>$"+ leg_cost +"</td>\n";
		info+="</tr>";

		stations[i] = obj[i]["station"];
		driven += milesCovered;

	}

		info+="<tr id='gas_t_p'>";
                info+= "<td>" +endPoint +"</td>\n";
                info+= "<td>"+Math.floor(driven) +"</td>\n";
                info+= "<td> - </td>\n";
                info+= "<td> - </td>\n";
                info+= "<td> - </td>\n";
                info+="</tr>";

	avgPrice = (avgPrice *1) / sz;
	avgPrice = avgPrice.toFixed(2);

	$("#prices_table").append(info);
	trip_cost = trip_cost.toFixed(2);
	document.getElementById("cost").innerHTML = "$"+trip_cost; 
	document.getElementById("mini_cost").innerHTML = "<span>Total Cost</span> $"+trip_cost;
	document.getElementById("avgPrice").innerHTML = "<span>Average Gas Price</span> $"+avgPrice;
	document.getElementById("gasUsed").innerHTML = "<span>Fuel</span> "+ totalGals.toFixed(1)+" Gallons";
	
	var tc = "<a href='http://www.ucsusa.org/publications/greentips/whats-your-carb.html' target='_new'> "+
		 (totalGals * 24).toFixed(1)+" lbs</a>";
	
	document.getElementById("ycf_result").innerHTML = tc;
	
	// 04.03.29 djura
	// added Terra Pass link code
	tpFootprint = Math.round(totalGals * 0.048);
	if(tpFootprint == 0)
		tpFootprint = 1;
	tpPrice = tpFootprint * 5.95;
	tpHref = 'http://www.terrapass.com/Merchant2/merchant.mvc?Screen=PROD&Product_Code=TPX-CNSMRCO2LBS&Store_Code=TerraPass&vehicle_count=1&klbsco2='+tpFootprint+'&utm_source=c2d&utm_campaign=c2d';
	tpText = 'Buy Carbon Offsets ($'+tpPrice+')';
	$('#about_carbon a:eq(1)').attr('href', tpHref).attr('target', '_blank').html(tpText);
	// end of Terra Pass link code

	$('#waiting').hide();

	if (showFlights) {
	
     		getFlightDetails();

	}
	
	stationsTimeId = setInterval('displayGasInfo()', 2500);

//        var adManager = GAdsManager(map, "pub-3764264814254639"); // {maxAdsOnMap:3, minZoomLevel: map.getZoom()}); 
//	adManager.enable();

}

function displayGasInfo() {

	var sz = markers.length;
	if (stationsWhere < sz) {
	   GEvent.trigger(markers[stationsWhere],"click");
	   stationsWhere++;
	} else {
	  markers[sz - 1].closeInfoWindow();
	  stopDisplayInfo();
	}
}

function stopDisplayInfo() {
	clearInterval(stationsTimeId);
	
	bounds = poly.getBounds();
	map.setZoom(map.getBoundsZoomLevel(bounds));
	map.setCenter(bounds.getCenter());

}

function getStation(marker) {
	var i = marker.c2d_id;
	marker.openInfoWindowHtml( stations[i]);
}

function getGasStops() {
  galsPT90 =  (galsPT / 10) * 9;

  var distanceTraveled =0;
  var step =0;
  var d2s = galsPT;
  while (distanceTraveled < miles) {

      if (step >  0 ) {
	d2s = galsPT90;
        distanceTraveled += d2s * avgMPG;

     }

     if (distanceTraveled < miles) {

     	var p =  poly.GetPointAtDistance(miles2meters(distanceTraveled));

     	stops[step] = [p.x ,  p.y];

     	step ++;
     }

  }

  var req ="";
  for (var i =0; i< stops.length; i++) {
	req+="&stop"+i+"="+stops[i][0]+","+stops[i][1];
  }
  stReq = req;
 
  getStopDetails();
  createC2DMap();

}

function getStopDetails( ){
	args = stReq;
	//var src = "/apps/stops.php?"+args+"&jsonp=?";
	var src = "/apps/stops.php?product="+fuel+"&"+args+"&jsonp=?";

        
	var data;

	$.getJSON( src , function (json) {
				data = json;
				displayGasStops(json);
			});

	

}


function meters2miles (meters) {

  return (meters / 1000) / 1.609;
}


function miles2meters (miles) {

  return (miles * 1.609) * 1000;
}

function load(startPoint, endPoint) {
  if (GBrowserIsCompatible()) {
    map = new GMap2(document.getElementById("map"));

    directionsPanel = document.getElementById("route");
    directions.loadFromWaypoints([startPoint, endPoint],
    {getPolyline:true,getSteps:true});


  }
}



function getC2Dir(form) {

  var startPoint = form.startPoint.value;
  var endPoint = form.endPoint.value;
  avgMPG = form.mpg.value;
  nationalPPG = form.ppg.value;
  galsPT = form.galsPT.value;
  $('#waiting').show();
  if (typeof GMap2 != 'function') {
    loadC2DGMap();
  }
  load(startPoint, endPoint);
  $('#waiting').hide();

  
}



}
