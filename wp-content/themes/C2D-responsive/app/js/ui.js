jQuery(document).ready(function($){
	// show/hide tooltip
	$('#how').click(function(){
		$('#tooltip').slideDown('medium');
	});
	$('#tooltip .close').click(function(){
		$('#tooltip').slideUp('medium');
	});

	// show/hide edit trip
	$('#edit').click(function(){
		$('.edit_trip').slideDown('medium');
	});
	$('.edit_trip .close').click(function(){
		$('.edit_trip').slideUp('medium');
	});

	// show/hide feedback
	//$('#advert').click(function(){
	//	$('.feedback').slideDown('medium');
	//});
	$('.feedback .close').click(function(){
		$('.feedback').slideUp('medium');
	});

	// show/hide tech. details
	if ($("#details")) {
		$("#details").toggle(function(){
			$(this).text('Show details').css({backgroundImage: 'url(/wp-content/themes/C2D/images/bullet_black.gif)'});
			$('#tech_details').slideUp('medium');
		},function(){
			$(this).text('Hide details').css({backgroundImage: 'url(/wp-content/themes/C2D/images/bullet_down.gif)'});
			$('#tech_details').slideDown('medium');
		});
	}
	
	// default text in inputs
	$(":text").placehold({blurClass: "d_text"});
	
	// select overlap ie6 bug
	$('.feedback').bgiframe();
	
	// show/hide ycf_tooltip
	//$('#learn_more').click(function(){
//		$('#ycf_tooltip').slideDown('medium');
//	});
	$('#ycf_tooltip .close').click(function(){
		$('#ycf_tooltip').slideUp('medium');
	});

});
