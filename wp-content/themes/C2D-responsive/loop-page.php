<?php
/**
 * The loop that displays a page.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop-page.php.
 *
 * @package WordPress
 * @subpackage cost_to_drive
 * @since Cost to Drive 1.2
 */
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<?php if ($post->post_parent == '1223') : ?>
				<?php /* Popular Destionation */ ?>

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'costtodrive' ), 'after' => '</div>' ) ); ?>
						<?php edit_post_link( __( 'Edit', 'costtodrive' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-content -->

				<?php else: ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php if (in_array(get_the_ID(), array(3578, 3591, 3597, 3600, 3603))): ?>
						<?php /*<a class="top-banner" target="_blank" href="https://itunes.apple.com/app/id909952082" onclick="trackOutboundLink('https://itunes.apple.com/app/id909952082'); return false;">
							<img class="img-responsive" src="<?php  print bloginfo('template_url'); ?>/images/toll-top-banner.jpg" alt="">
						</a>*/ ?>
						<div class="tollsmart-banner-box-horiz">
							<img class="img-responsive" src="/wp-content/themes/C2D-responsive/images/tollsmart-banner-bgr-horiz.png" alt="">
							<div class="banner-buttons">
								<a href="https://itunes.apple.com/app/id909952082" onclick="trackOutboundLink('https://itunes.apple.com/app/id909952082'); return false;" class="app-store-btn">Available on the App Store</a>
								<a href="https://play.google.com/store/apps/details?id=com.tollsmart.leonidiogansen.tolls" onclick="trackOutboundLink('https://play.google.com/store/apps/details?id=com.tollsmart.leonidiogansen.tolls'); return false;" class="google-play-btn">Get it on Google play</a>
							</div>
						</div>
					<?php endif; ?>

					<?php if ( is_front_page() ) { ?>
						<h2 class="entry-title"><?php the_title(); ?></h2>
					<?php } else { ?>
						<h1 class="entry-title"><?php the_title(); ?></h1>
					<?php } ?>

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'costtodrive' ), 'after' => '</div>' ) ); ?>
						<?php edit_post_link( __( 'Edit', 'costtodrive' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-## -->
				<?php endif; ?>

				<?php // comments_template( '', true ); ?>

<?php endwhile; // end of the loop. ?>
