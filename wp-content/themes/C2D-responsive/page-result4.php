<?php
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
/**
 * The template for displaying results page.
 *
 * @package WordPress
 * @subpackage cost_to_drive
 * @since Cost to Drive 1.0
 */

 //error_reporting(E_ALL);

$query_string = $_SERVER['QUERY_STRING'];
// query string format:
// pagename=from&trip_info=..

// (I)
// trip_info=start-address/to/end-address/
// (II)
// trip_info=start-address/to/end-address/passengers/carId/~whatever or nothing~
// trip_info=start-address/to/end-address/2/15035/2008-Honda-Civic
// (III)
// trip_info=start-address/to/end-address/passengers/custom/mpg/gpt/fuel_type
// trip_info=start-address/to/end-address/passengers/custom/20/17/Regular

//  to the end of url goes: /via/Stop-Point/via/Stop-Point2/...

if (strpos($query_string, 'start=') === false) {

	//~$query_vars_initial = explode('/', $query_string);
	$query_vars_initial = explode('/', $_GET['trip_info']);


	// waypoints - Stop Points: Read them from the array and then remove them from an array
	$get_waypoints = '';
	$query_vars_bkp = $query_vars_initial;
	if (in_array('via', $query_vars_bkp)) {
		// url contains: /via/Stop-Point/via/Stop-Point2/...
		$get_waypoints = array();
		$next = false;
		foreach ($query_vars_bkp as $var_key=>$var_value) {
			if ($next) {
				$get_waypoints[] = $var_value;
				unset($query_vars_initial[$var_key]);
			}
			if ($var_value == 'via') {
				$next = true;
				unset($query_vars_initial[$var_key]);
			} else {
				$next = false;
			}
		}
	}
	$waypoints_bkp = $get_waypoints;
	if ($get_waypoints == '') {
		unset ($get_waypoints);
	}

	// reset array indexes, make final array $query_vars without Stop Points
	$query_vars = array();
	foreach ($query_vars_initial as $var_value) {
		$query_vars[] = $var_value;
	}



	$get_start = str_replace('-', '%20', $query_vars[0]);
	$get_end =   str_replace('-', '%20', $query_vars[2]);
	$get_start_readable = str_replace('-', ' ', $query_vars[0]);
	$get_end_readable =   str_replace('-', ' ', $query_vars[2]);

	if (count($query_vars) <= 5) {

		// default car - url format: /start-address/to/end-address/
		$get_custom = true;
		$get_mpg = 25;
		$get_car_id = -1;
		$get_fuel_type = 'Regular';
		$get_passengers = 1;
		$get_gpt = 17;

	} else {

		if (in_array('custom', $query_vars)) {

			// custom car info: /start-address/to/end-address/passengers/custom/mpg/gpt/fuel_type
			$get_custom = true;
			$get_passengers = (is_numeric($query_vars[3])) ? $query_vars[3] : 1;
			$custom_var_index = array_search('custom', $query_vars);
			$custom_var_index++;
			$get_mpg = (is_numeric($query_vars[$custom_var_index])) ? $query_vars[$custom_var_index] : 25;
			$custom_var_index++;
			$get_gpt = (is_numeric($query_vars[$custom_var_index])) ? $query_vars[$custom_var_index] : 17;
			$custom_var_index++;
			$get_fuel_type = ($query_vars[$custom_var_index] != '') ? $query_vars[$custom_var_index] : 'Regular';

		} else {

			// carId from url: /start-address/to/end-address/passengers/carId/~whatever or nothing~
			$get_custom = false;
			$get_passengers = (is_numeric($query_vars[3])) ? $query_vars[3] : 1;
			$get_car_id = (is_numeric($query_vars[4])) ? $query_vars[4] : 15395;
		}
	}

} else {
	$get_start = $_GET["start"];
	$get_end =   $_GET["end"];
	$get_start_readable = $_GET["start"];
	$get_end_readable =   $_GET["end"];
	$get_custom = ($_GET["custom"] == 'true') ? true : false;
	$get_mpg = $_GET["mpg"];
	$get_car_id = $_GET["carId"];
	$get_fuel_type = $_GET["fuelType"];
	$get_waypoints = $_GET["waypoints"];
	$get_passengers = $_GET["passengers"];
	$get_gpt = $_GET["gpt"];
}


	//seconds * minutes * hours * days + current time
	$inTwoMonths = 60 * 60 * 24 * 60 + time();
	setcookie('start', $get_start_readable, $inTwoMonths, "/", ".costtodrive.com");
	setcookie('end', $get_end_readable, $inTwoMonths, "/", ".costtodrive.com");
	setcookie('carId', $get_car_id, $inTwoMonths, "/", ".costtodrive.com");

	include("app/include/globals.php");
	include("app/classes/object-trip.php");
	include("app/classes/object-google-maps-test.php");
	include("app/classes/object-tripadvisor-destination.php");
	//Calculate 60 days in the futures
	if ($get_custom==false && isset($get_car_id) && $get_car_id!="")
	{
		$carId = $get_car_id;
	}
	else
	{
		if (isset($get_mpg) && $get_mpg!="")
		{
			$carId=-1;
		}
		else
		{
			$carId = 15395;
		}
	}
	$car = new Car($carId);
	if ($get_custom==true && isset($get_mpg) && $get_mpg!="")
	{
		$car->setCustom($get_fuel_type,$get_mpg,$get_gpt);
		$carName = "Custom Car";
	}
	else
	{
		$carName = $car->year." ".$car->mfr." ".$car->make;
	}

	$trip = new Trip($car,$get_start,$get_end,$get_waypoints,$get_passengers);
	$trip->calculateCost();
	//$trip->getKayak();
	$map = new C2DMap($trip);

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php bloginfo( 'name' ); ?> | From <?php echo $trip->start; ?> to <?php echo $trip->end; ?>, in <?php print ucwords($carName); ?></title>
<meta property="og:title" content="<?php bloginfo( 'name' ); ?> | From <?php echo $trip->start; ?> to <?php echo $trip->end; ?>, in <?php print ucwords($carName); ?>" />
<meta property="og:image" content="<?php print  bloginfo('template_url'); ?>/images/structural/og-logo.png" />
<meta property="og:url" content="http://<?php print  $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]; ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="image_src" href="/wp-content/themes/C2D/images/structural/logo.png" />
<?php
// load js files here
	if (!is_admin()) {
		wp_enqueue_script('jquery');
		wp_enqueue_script('js_fancybox', get_bloginfo('template_url') . '/jquery.fancybox-1.3.4.js', array('jquery'), '1.0');
		wp_enqueue_script('js_files', get_bloginfo('template_url') . '/main.js', array('jquery'), '1.0');
	}
?>
<?php
	wp_head();
?>
<script type="text/javascript">
	var end_location_name = "<?php print addslashes($trip->end); ?>";
	var end_point_lat = <?php print $trip->endPoint->lat; ?>;
	var end_point_lng = <?php print $trip->endPoint->lng; ?>;
</script>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script src="http://widgets.twimg.com/j/2/widget.js"></script>
<script type="text/javascript">stLight.options({publisher:'242135f6-62bd-47d6-93f5-b37899a6956c'});</script>
<?php
	$map->printGoogleJS();
	$map->addAddress($trip->start,$trip->startPoint);
	$map->addAddress($trip->end,$trip->endPoint);
	$map->polyline=$trip->polyline;

	$count_waypoints = count($trip->waypointsList);
	if ($count_waypoints > 0) {
		for ($i = 0; $i < $count_waypoints-1; $i++)	{
			$map->addAddress($trip->waypointsList[$i],$trip->waypointsListPoints[$i]);
		}
	}

	$map->showMap(map);
?>


<?php
	// Weather icons on the map

	$weather_points = array();
	$weather_points[] = $trip->startPoint;

	foreach ($map->trip->stops as $gas_stop) {
		$weather_points[] = (object)array('lat'=>$gas_stop['latitude'], 'lng'=>$gas_stop['longitude']);
	}

	$weather_points[] = $trip->endPoint;

	$weather_points_serialized =  json_encode($weather_points);
?>
<script type="text/javascript">

	// called from initialize()
	function refueling_points(map) {

		var markersArrayGas = new Array();
		<?php
		$i=0;
		while($i < count($map->trip->stops) ) {
		?>
			var myLatlng = new google.maps.LatLng(<?php print $map->trip->stops[$i]["latitude"]; ?>, <?php print $map->trip->stops[$i]["longitude"]; ?>);
			var marker<?php print $i; ?> = new google.maps.Marker({
			position: myLatlng,
			map: map,
			icon:"/wp-content/themes/C2D/images/gas.png",
			title:"<?php print $map->trip->stops[$i]["station"]; ?>"
			});

			var infoWindow = new google.maps.InfoWindow();

			google.maps.event.addListener(marker<?php print $i; ?>, "click", function () {
				infoWindow.setContent("<?php print $map->trip->stops[$i]["station"]; ?>");
				infoWindow.open(map,marker<?php print $i; ?>);
			});

			markersArrayGas.push(marker<?php print $i; ?>);

		<?php
			$i++;
		}
		?>

		for (var i = 0; i < markersArrayGas.length; i++ ) {
			markersArrayGas[i].setMap(map);
		}
	}

	// called from initialize()
	function weather_points(map, weather_response) {

		var markersArrayWeather = new Array();

		var response = JSON.parse(weather_response);
		for (var i=0; i < response.length; i++) {

			var myLatlng = new google.maps.LatLng(response[i].lat, response[i].lng);
			markersArrayWeather[i] = new google.maps.Marker({
				position: myLatlng,
				map: map,
				icon: response[i].icon,
				title:response[i].temp+'°F, '+response[i].desc
			});

			var infoWindow = new google.maps.InfoWindow();

			google.maps.event.addListener(markersArrayWeather[i], "click", function () {
				infoWindow.setContent(response[i].temp +'&deg;F<br />'+ response[i].desc);
				infoWindow.open(map,markersArrayWeather[i]);
			});

			markersArrayWeather.push(markersArrayWeather[i]);
		}

		for (var i = 0; i < markersArrayWeather.length; i++ ) {
			markersArrayWeather[i].setMap(map);
		}
	}




	// called on checkbox click
	function get_refueling() {

		if (!document.getElementById('weather-control').checked) {

			initialize(0, 0);
		} else {

			get_weather();
		}
	}
	// called on checkbox click
	function get_weather() {

		document.getElementById('map-loader').style.display = 'block';

		if (document.getElementById('weather-control').checked) {

			var xmlhttp;
			xmlhttp=new XMLHttpRequest();
			xmlhttp.onreadystatechange = function(){

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					//console.log(xmlhttp.responseText);
					document.getElementById('map-loader').style.display = 'none';

					var weatherResponseText = xmlhttp.responseText;

					if (! document.getElementById('attractions-control').checked) {
						initialize(weatherResponseText, 0);
					} else {
						//another ajax request (attractions)
					}
				}
			}
			xmlhttp.open("POST","/wp-content/themes/C2D/ajax/weather.php",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send('points=<?php print $weather_points_serialized; ?>');
		} else {

			if (! document.getElementById('attractions-control').checked) {

				initialize(0, 0);
				document.getElementById('map-loader').style.display = 'none';
			} else {
				// get attractions
			}
		}
	}
	// called on checkbox click
	function get_attractions(){
		// get attractions data and then call initialize(...)
	}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//function get_ajax_data_1() {
	//
	//	document.getElementById('map-loader').style.display = 'block';
	//
	//	if (document.getElementById('weather-control').checked && document.getElementById('attractions-control').checked) {
	//
	//	} else if (document.getElementById('weather-control').checked) {
	//
	//	} else if (document.getElementById('attractions-control').checked) {
	//
	//	}
	//}
	//
	//function get_ajax_data_2() {
	//
	//	document.getElementById('map-loader').style.display = 'block';
	//
	//	var weather_response;
	//	if (document.getElementById('weather-control').checked) {
	//
	//		// get ajax
	//		if (document.getElementById('attractions-control').checked) {
	//
	//		}
	//	}
	//}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


</script>


</head>

<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
	<div id="header">
		<div id="masthead">
			<div id="branding" role="banner">
				<div id="site-title">
					<span>
						<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="/wp-content/themes/C2D/images/structural/logo.png" alt="Cost To Drive" /></a>
					</span>
				</div>
				<div id="site-description"><?php bloginfo( 'description' ); ?></div>

				<?php
					// Check if this is a post or page, if it has a thumbnail, and if it's a big one
					if ( is_singular() && current_theme_supports( 'post-thumbnails' ) &&
							has_post_thumbnail( $post->ID ) &&
							( /* $src, $width, $height */ $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnail' ) ) &&
							$image[1] >= HEADER_IMAGE_WIDTH ) :
						// Houston, we have a new header image!
						echo get_the_post_thumbnail( $post->ID );
					elseif ( get_header_image() ) : ?>
						<img src="<?php header_image(); ?>" width="<?php echo HEADER_IMAGE_WIDTH; ?>" height="<?php echo HEADER_IMAGE_HEIGHT; ?>" alt="" />
					<?php endif; ?>
			</div><!-- #branding -->
			<div id="login-register">
				<?php if ( is_user_logged_in() ) {
					 global $current_user;

					wp_get_current_user();

					?>
					 <span>Welcome back, <?php echo $current_user->nickname; ?>!</span> <?php
				} else {
				?>  <!--  <a href="<?php //echo wp_login_url( get_permalink() ); ?>" title="Login">Login</a> or <?php //wp_register('', ''); ?> --> <?php
					}
				?>

			</div>
			<div id="access" role="navigation">
			  <?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
				<div class="skip-link screen-reader-text"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'costtodrive' ); ?>"><?php _e( 'Skip to content', 'costtodrive' ); ?></a></div>
				<?php /* Our navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
				<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
			</div><!-- #access -->
		</div><!-- #masthead -->
	</div><!-- #header -->

	<div id="main">

		<div id="container">
			<div id="content" role="main">
				<div class="wide-box page">
				<?php if (!is_null($trip->start) && !is_null($trip->end)) : ?>

					<div id="your_trip_info">
						<h2 id="your_trip"><span>Your Trip</span></h2>
						<a class="expand-et" href="#inline">Edit Trip</a>
						<a class="collapse-et" href="#">Cancel</a>

						<div class="trip_info">
							<p><?php echo $trip->start; ?> to <?php echo $trip->end; ?>, in <?php print ucwords($carName); ?></p>
						</div>
					</div>

					<div id="edit-trip-form">
						<!-- map breaks when this is enabled -->
						<form id="where_to_f"  action="http://<?php print $_SERVER['SERVER_NAME']; ?>/from" method="get">
							<div><?php include("app/include/trip-form-edit-trip.php");?></div>
							<div><?php  include("app/include/car-form.php");?></div>
							<input class="my_c2d_btn" type="image" src="<?php echo get_bloginfo('template_url'); ?>/images/structural/c2d-btn.gif" alt="Calculate my cost to drive" />
						</form>
						<div><?php include("app/include/car-script.php"); ?></div>
					</div> <!-- edit-trip-form -->

					<?php
						//$end_location = $trip->end;
						$end_location = $get_end_readable;

						if (!empty($end_location)) {
							$query = sprintf("SELECT geo_id FROM geos_id_cache WHERE geo_name='%s' AND geo_id<>0 LIMIT 1",
											mysql_real_escape_string($end_location));
							$row = $wpdb->get_row($query);
							$taid_is_ok = false;

							if ($row != null) {
								$taid = $row->geo_id;
								if (is_numeric($taid) && $taid > 0) {
									$taid_is_ok = true;
								}
								//print '<p>#from cache: '.$taid.'#</p>';
							} else {
								$contents = file_get_contents('https://www.google.com/search?q=tripadvisor+hotels+'.urlencode($end_location));
								$pattern = '/http:\/\/www\.tripadvisor(.*?)-g([0-9]+)/';
								preg_match($pattern, $contents, $matches);
								$taid = $matches[2];
								if (is_numeric($taid) && $taid > 0) {
									if ($wpdb->insert('geos_id_cache', array('geo_id' => $taid, 'geo_name' => $end_location), array('%d', '%s'))) {
										$taid_is_ok = true;
										//print '<p>#write into db#</p>';
									}
								}
								//print '<p>#live: '.$taid.'#</p>';
							}
						}
					?>

					<div id="your_cost_info">
						<div class="your_cost_info_content">
							<h2 id="your_cost"><span>Your Cost To Drive Is</span></h2>
							<span id="cost">$<?php echo number_format($trip->cost,2); ?></span>
							<?php
								if ($trip->passengers > 1 ) {
									echo '<div class="trip_info"><p>The cost per passenger: $'.$trip->getCostPerPassenger().'</p></div>';
								}
							?>
							<div class="cost-info cost-info-distance">
								<span>Total Distance:</span><span><?php echo number_format($trip->distance,2);?> miles </span>
							</div>
							<div class="cost-info cost-info-time">
								<span>Driving Time:</span><span><?php echo $trip->getDrivingTime();?></span>
							</div>
							<?php /* if ($trip->kayakPrice != 0) { ?>
							<div class="cost-info cost-info-kayak-price">
								<span><a target="_blank" href="<?php echo $trip->kayakUrl;?>">Fly for $<?php echo $trip->kayakPrice;?></a></span>
							</div>
							<?php } */?>
						</div>

						<?php if ($taid_is_ok) : ?>
						<?php $tripAdvisor = new TripAdvisorDestination($taid); ?>
						<div class="cost-info trip-advisor-short-info">
						  <?php if ($tripAdvisor->destPhotoURL != ''): ?>
							<a class="dest-photo" target="_blank" href="http://www.tripadvisor.com/Tourism-g<?php print $taid; ?>"><img src="<?php print $tripAdvisor->destPhotoURL; ?>" alt="" /></a>
						  <?php endif; ?>
							<a class="dest-desc" target="_blank" href="http://www.tripadvisor.com/Tourism-g<?php print $taid; ?>">Find top hotels, restaurants and attractions in <?php echo $trip->end; ?></a>
							<div class="powered-by"><span>Powered by</span><img class="ta-logo" src="/wp-content/themes/C2D/images/structural/tripadvisor_logo_api.gif" alt="tripadvisor"></div>
						</div>
					  <?php endif; ?>

					</div>

					<div>
						<p><?php
							if ($trip->getDrivingTime() <= 2) {
								print 'This is a trip under two hours, therefore we will serve recommended restaurants or attractions at destination here';
							} elseif ($trip->getDrivingTime() <= 12) {
								print 'This is a trip longer than two but shorter than 12 hours, therefore we will serve recommended restaurants or attractions along the way here, something like 4 or 6 hrs down the road';
							} else {
								print 'This is a long trip and very likely lodging will be required';
							}
						?></p>
					</div>
					<div class="map-controls">
						<label for="refueling-points-control"><input type="checkbox" checked="checked" name="refueling-points-control" id="refueling-points-control" onchange="get_refueling()" /> Refueling points</label>
					</div>
					<div class="map-controls">
						<label for="weather-control"><input type="checkbox" name="weather-control" id="weather-control" onchange="get_weather()" /> Weather</label>
					</div>
					<div class="map-controls">
						<label for="attractions-control"><input type="checkbox" name="attractions-control" id="attractions-control" onchange="get_attractions()" /> Attractions</label>
					</div>
					<div id="map-loader"><img src="http://<?php print $_SERVER['SERVER_NAME']; ?>/wp-content/themes/C2D/images/ajax-loader-circle.gif" alt="Loading..." /></div>
					<div id="map">
						<div class="map-positioner">
							<div id="map_canvas" style="width: 646px; height: 360px;"></div>
						</div>
					</div>

					<div id="complete_directions">
						<p>Get complete directions from:</p>
						<ul>
							<li><a target="_blank" href="<?php echo $trip->googleURL;?>">Google Maps</a></li>
						</ul>
					</div>

					<?php if ($taid_is_ok) : ?>
					<div class="destination-recommendations">
						<input type="hidden" name="taid-value" value="<?php print $taid; ?>" id="taid-value" />
						<h2 id="dest-recommend">New! <span>Destination Recommendations for <?php echo $trip->end; ?></span></h2>
						<div class="dest-recommend-content-wrap">
							<div class="dest-recommend-content">
								<img class="spiner" src="http://<?php print $_SERVER['SERVER_NAME']; ?>/wp-content/themes/C2D/images/ajax-loader.gif" alt="loading..." title="" />
							</div>
						</div>
					</div>
					<?php endif; ?>


					<div id="tech-details">
						<h2 id="tenical_details_heading"><span>Details</span></h2>
						<a class="expand" href="#">Expand</a>
						<a class="collapse" href="#">Collapse</a>

						<div class="tech-details-content">
							<div class="trip_info">
								<ul id="prices">
									<li id="gasUsed"><span>Fuel &rsaquo;</span> <?php echo number_format($trip->totalGallons,1); ?> Gallons</li>
									<li id="avgPrice"><span>Average Gas Price &rsaquo;</span> $<?php echo number_format(($trip->cost / $trip->totalGallons),2);?></li>
									<?php
										// $tolls = $trip->getTolls();
										//
										// if (count($tolls) > 0)
										// {
										// 	echo "<li>The toll roads include the following: <br>";
										// 	foreach($tolls as $toll)
										// 	{
										// 		print_r($toll);
										// 	}
										// 	echo "</li>";
										// }
									?>
								</ul>
							</div>
							<div id="tech_details">
								<table id="prices_table">
									<tbody>
										<tr id="prices_row">
											<th></th>
											<th>Miles Driven</th>
											<th>Gallons</th>
											<th>Gas Price</th>
											<th>Cost</th>
										</tr>
										<!-- <td><a onmouseover="GEvent.trigger(markers[0],&quot;click&quot;)" href="#">New York Co., NY</a></td>
										<td><a onmouseover="GEvent.trigger(markers[1],&quot;click&quot;)" href="#">Jefferson Co., PA</a></td> -->
										<?php
											foreach($trip->legs as $leg)
											{
												echo "<tr>\n";
												echo "<td>".$leg->location."</td>\n";
												echo "<td>".number_format($leg->distance,1)."</td>\n";
												echo "<td>".number_format($leg->gallons,1)."</td>\n";
												echo "<td>".number_format($leg->gasPrice,2)."</td>\n";
												echo "<td>".number_format($leg->legCost,2)."</td>\n";
												echo "</tr>\n";
											}
										?>
									</tbody>
								</table>
							</div>
							<div id="total">
								<ul>
									<li id="mini_cost"><span>Total Cost</span> $<?php echo number_format($trip->cost,2); ?></li>
								</ul>
							</div>
						</div><!-- /tech-details-content -->
					</div><!-- /details -->

					<div class="weather-box">
						<!-- ajax populated -->
					</div>

				<?php else : ?>

					<div id="your_trip_info">
						<h2 id="your_trip"><span>Error</span></h2>

						<div class="trip_info">
							<p>Start or Destination is not valid. Please <a class="expand-et-er" href="#">edit</a> your trip info.</p>
						</div>
					</div>
					<div id="edit-trip-form">
						<!-- map breaks when this is enabled -->
						<form id="where_to_f"  action="http://<?php print $_SERVER['SERVER_NAME']; ?>/from" method="get">
							<div><?php include("app/include/trip-form-edit-trip.php");?></div>
							<div><?php  include("app/include/car-form.php");?></div>
							<input class="my_c2d_btn" type="image" src="<?php echo get_bloginfo('template_url'); ?>/images/structural/c2d-btn.gif" alt="Calculate my cost to drive" />
						</form>
						<div><?php include("app/include/car-script.php"); ?></div>
					</div> <!-- edit-trip-form -->

				<?php endif; ?>
				</div><!-- /wide-box -->
			</div><!-- #content -->
		</div><!-- #container -->

		<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function($) {
				initialize(0, 0);
			});
		</script>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>