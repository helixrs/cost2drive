<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage cost_to_drive
 * @since Cost to Drive 1.0
 */

	header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
	include("app/include/globals.php");
	include("app/classes/object-car.php");

	get_header();
?>

		<div id="container">
			<div id="content" role="main">

				<div class="wide-box page">
					<form id="where_to_f"  action="http://<?php print $_SERVER['SERVER_NAME']; ?>/result2" method="get">
						<?php include("app/include/trip-form.php");?>
						<?php include("app/include/car-form.php");?>
						<input class="my_c2d_btn" type="image" src="<?php echo get_bloginfo('template_url'); ?>/images/structural/c2d-btn.gif" alt="Calculate my cost to drive" />
					</form>
					<?php include("app/include/car-script.php"); ?>
				</div><!-- /wide-box -->

			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
