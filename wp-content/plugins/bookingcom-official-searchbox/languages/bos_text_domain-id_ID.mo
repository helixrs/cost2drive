��    1      �  C   ,      8  !   9     [     _     e     i     p     ~     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �  
                            %  Y   C  
   �     �     �  	   �  <   �     �     �     �        I     �   M     �       -        H  �  O  #   F	     j	     n	     t	     x	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     

     
     #
     '
     0
     4
     <
  ^   \
     �
     �
     �
  	   �
  N   �
     -     1     5     9  W   =  �   �     S     d  +   v     �                         .               0                +       $   *      &            "             ,   '                    %      (      	   )              
              /   1      !                   #                         -        I don't have specific dates yet  Apr April Aug August Check-in date Check-out date Close Close calendar Dec December Feb February Fr Jan January Jul July Jun June Mar March May May  Mo Need a hotel? Next month Nov November Oct October Open calendar and pick a date Please check your dates, the check-out date appears to be earlier than the check-in date. Prev month Sa Sept September Sorry, we need at least part of the name to start searching. Su Th Tu We Your check-in date is in the past. Please check your dates and try again. Your check-out date is more than 30 nights after your check-in date. Bookings can only be made for a maximum period of 30 nights. Please enter alternative dates and try again. check-in date check-out date e.g. city, region, district or specific hotel search Project-Id-Version: Booking.com searchbox plugin 1.1
POT-Creation-Date: 2014-06-24 16:27+0100
PO-Revision-Date: 2014-06-24 16:27+0100
Last-Translator: 
Language-Team: Strategic Partnership at Booking.com <wp-plugins@booking.com>
Language: id_ID
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.5
X-Poedit-Basepath: .
Plural-Forms: nplurals=1; plural=0;
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 Saya belum punya tanggal yang pasti Apr April Agu Agustus Tanggal check-in Tanggal check-out Tutup Tutup kalender Des Desember Feb Februari Jum Jan Januari Jul Juli Jun Juni Mar Maret Mei Mei Sen Butuh hotel? Bulan depan Nov November Okt Oktober Buka kalender dan pilih tanggal Harap periksa tanggal Anda, tanggal check-out sepertinya lebih awal daripada tanggal check-in. Bulan sebelumnya Sab Sep September Maaf, kami memerlukan setidaknya sebagian dari nama tujuan untuk mulai mencari Min Kam Sel Rab Tanggal check-in Anda telah lewat. Silakan periksa kembali tanggal Anda lalu coba lagi. Tanggal check-out Anda melebihi 30 malam setelah tanggal check-in Anda. Pemesanan hanya bisa dilakukan untuk periode maksimal 30 malam. Silakan masukkan tanggal yang berbeda lalu coba lagi. tanggal check-in tanggal check-out mis. kota, daerah, distrik, atau nama hotel cari 