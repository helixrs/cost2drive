��    1      �  C   ,      8  !   9     [     _     e     i     p     ~     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �  
                            %  Y   C  
   �     �     �  	   �  <   �     �     �     �        I     �   M     �       -        H  ~  O  A   �	     
     
     
     
     %
     5
     J
     Q
     j
     n
     w
  	   {
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
       -   
  f   8     �     �     �     �  I   �                      b   "  �   �     t     �  =   �  	   �                         .               0                +       $   *      &            "             ,   '                    %      (      	   )              
              /   1      !                   #                         -        I don't have specific dates yet  Apr April Aug August Check-in date Check-out date Close Close calendar Dec December Feb February Fr Jan January Jul July Jun June Mar March May May  Mo Need a hotel? Next month Nov November Oct October Open calendar and pick a date Please check your dates, the check-out date appears to be earlier than the check-in date. Prev month Sa Sept September Sorry, we need at least part of the name to start searching. Su Th Tu We Your check-in date is in the past. Please check your dates and try again. Your check-out date is more than 30 nights after your check-in date. Bookings can only be made for a maximum period of 30 nights. Please enter alternative dates and try again. check-in date check-out date e.g. city, region, district or specific hotel search Project-Id-Version: Booking.com searchbox plugin 1.1\rbos_text_domain-de_DE.po\rbos_text_domain-fr_FR.mo\rbos_text_domain-fr_FR.po\rbos_text_domain-it_IT.mo\rbos_text_domain-it_IT.po
POT-Creation-Date: 2014-06-24 16:28+0100
PO-Revision-Date: 2014-06-24 16:28+0100
Last-Translator: 
Language-Team: Strategic Partnership at Booking.com <wp-plugins@booking.com>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.5
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 Ainda n&atilde;o sei as datas espec&iacute;ficas da minha estadia Abr Abril Ago Agosto Data de entrada Data de sa&iacute;da Fechar Fechar calend&aacute;rio Dez Dezembro Fev Fevereiro Sex. Jan Janeiro Jul Julho Jun Junho Mar Mar&ccedil;o Maio Mai Seg. Precisa de um hotel? M&ecirc;s seguinte Nov Novembro Out Outubro Abrir o calend&aacute;rio e escolher uma data Por favor, verifique as suas datas. A data de entrada &eacute; anterior &agrave; data de sa&iacute;da. M&ecirc;s anterior Sab. Set Setembro Desculpe, precisamos de pelo menos parte do nome para iniciar a pesquisa. Dom. Qui. Ter. Qua. A data de entrada selecionada j&aacute; passou. Por favor, verifique suas datas e tente novamente. Voc&ecirc; selecionou uma data de sa&iacute;da para mais de 30 di&aacute;rias ap&oacute;s a data de entrada. As reservas s&oacute; podem ser feitas por um per&iacute;odo m&aacute;ximo de 30 di&aacute;rias. Por favor, escolha outras datas. data de entrada data de sa&iacute;da Ex.: cidade, regi&atilde;o, bairro ou hotel espec&iacute;fico pesquisar 