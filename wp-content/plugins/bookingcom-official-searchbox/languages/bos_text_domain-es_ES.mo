��    1      �  C   ,      8  !   9     [     _     e     i     p     ~     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �  
                            %  Y   C  
   �     �     �  	   �  <   �     �     �     �        I     �   M     �       -        H  �  O  (   M	     v	     z	     �	     �	     �	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     
     
     
     4
     B
  	   F
     P
     T
  )   \
  P   �
     �
  
   �
     �
  
   �
  <   �
     ;     ?     C  
   G  J   R  m   �            2   ,     _                         .               0                +       $   *      &            "             ,   '                    %      (      	   )              
              /   1      !                   #                         -        I don't have specific dates yet  Apr April Aug August Check-in date Check-out date Close Close calendar Dec December Feb February Fr Jan January Jul July Jun June Mar March May May  Mo Need a hotel? Next month Nov November Oct October Open calendar and pick a date Please check your dates, the check-out date appears to be earlier than the check-in date. Prev month Sa Sept September Sorry, we need at least part of the name to start searching. Su Th Tu We Your check-in date is in the past. Please check your dates and try again. Your check-out date is more than 30 nights after your check-in date. Bookings can only be made for a maximum period of 30 nights. Please enter alternative dates and try again. check-in date check-out date e.g. city, region, district or specific hotel search Project-Id-Version: Booking.com searchbox plugin 1.1
POT-Creation-Date: 2014-06-24 16:27+0100
PO-Revision-Date: 2014-06-24 16:27+0100
Last-Translator: 
Language-Team: Strategic Partnership at Booking.com <wp-plugins@booking.com>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.5
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 Todav&iacute;a no he decidido las fechas abr abril ago agosto Fecha de entrada Fecha de salida Cerrar Cerrar calendario dic diciembre feb febrero vie ene enero jul julio jun junio mar marzo mayo may lun &iquest;Necesitas un hotel? Mes siguiente nov noviembre oct octubre Abre el calendario y selecciona una fecha Comprueba las fechas: el d&iacute;a de salida es igual o anterior al de entrada. Mes anterior s&aacute;b sep septiembre Necesitamos al menos parte del nombre para empezar a buscar. dom jue mar mi&eacute; La fecha de entrada ya ha pasado. Revisa las fechas y vuelve a intentarlo. La duraci&oacute;n de la estancia no puede ser superior a 30 noches. Revisa las fechas y vuelve a intentarlo. fecha de entrada fecha de salida Ej. ciudad, regi&oacute;n, zona o nombre del hotel buscar 