=== Theme per user ===
Contributors: PressLabs, olarmarius
Donate link: http://www.presslabs.com/
Tags: themes, theme, user, theme per user, redirect, presslabs 
Requires at least: 3.8.1
Tested up to: 3.8.1
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Load one Theme for a specific user. You must set the theme from User Profile then logout and login again in order to take effect.

== Description ==

Load one Theme for a specific user. You must set the theme from User Profile then logout and login again in order to take effect.

= Usage =
1. Select the theme you prefer from the `User Profile` page, then press Save button;
2. Logout;
3. Login.

== Installation ==

= Installation =
1. Upload `theme-per-user.zip` to the `/wp-content/plugins/` directory;
2. Extract the `theme-per-user.zip` archive into the `/wp-content/plugins/` directory;
3. Activate the plugin through the 'Plugins' menu in WordPress.


== Screenshots ==

1. Theme per user option

== Changelog ==

= 1.0 =
Start version on WP.

