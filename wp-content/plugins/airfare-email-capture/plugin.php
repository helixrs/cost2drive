<?php


/**
 * Plugin Name: Airfare Email Capture
 * Plugin URI:
 * Description:
 * Version: 1.0.0
 * Author:
 * Author URI:
 * License: GPL2
 */
class AWDEmailCapture
{

    /**
     * AWDEmailCapture constructor.
     */
    public function __construct()
    {
        // load_plugin_textdomain( 'airfare-email-capture', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );
        add_action('wp_enqueue_scripts', array(&$this, 'register_plugin_styles'));
        add_action('wp_enqueue_scripts', array(&$this, 'register_plugin_scripts'));
        add_action('wp_head', array(&$this, 'add_ajax_library'));
        add_action('wp_ajax_capture_email', array(&$this, 'capture_email'));
        // add_filter( 'the_content', array( &$this, 'add_modal' ) );
    }

    public function capture_email()
    {

    }

    public function register_plugin_styles()
    {
        wp_register_style('airfare-email-capture', plugins_url('ive-read-this/css/plugin.css'));
        wp_enqueue_style('airfare-email-capture');
    }

    public function register_plugin_scripts()
    {

        wp_register_script('airfare-email-capture', plugins_url('airfare-email-capture/js/plugin.js'), array('jquery'));
        wp_enqueue_script('airfare-email-capture');

    } // end register_plugin_scripts

    public function add_ajax_library()
    {

        $html = '<script type="text/javascript">';
        $html .= 'var ajaxurl = "' . admin_url('admin-ajax.php') . '"';
        $html .= '</script>';

        echo $html;

    }

    public function add_modal($content)
    {
        //ToDo:: if based on page type

        $html = '<div class="front-page-lightbox">';
        $html .= '<div class="modal fade in col-md-8 col-lg-4 col-lg-offset-4 col-sm-8 col-sm-offset-2 col-md-offset-2 col-xs-12 col-xs-offset-0" tabindex="-1" role="dialog">';
        $html .= '<div class="modal-dialog">';
        $html .= '<div class="modal-content">';
        $html .= '<div class="modal-header">';
        $html .= '<h4 class="modal-title text-center">Special Offer from Cost2Drive</h4>';
        $html .= '</div>';
        $html .= '<div class = "modal-body">';
        $html .= "<p class='step-1'>";
        $html .= "<i> TripPass &#8482; Roadside Assistance </i>by Urgent.ly provides you with On-Demand Roadside Assistanceon your next road trip for a low flat-rate of <strong>$9.99</strong>.</p>";
        $html .= "<p class='step2'>";
        $html .= '< i>Cost2Drive </i > has partnered with < i>TripPass &#8482; Roadside Assistance</i> by Urgent.ly to';
        $html .= 'provide you with On-Demand Roadside Assistance for your trip with one low flat-rate.<br><br>';
        $html .= "With travel insurance through TripPass&#8482;, you'll have valuable coverage that travels with you . Breakdowns, flat tire, out of gas, lock outs . These things happen . Insure your journey with < i>TripPass &#8482; Roadside Assistance</i> by Urgent.ly to get you back on the road quickly.<br ><br >";
        $html .= 'To take advantage of this offer, enter your dates of travel below and click < i>Proceed to Payment </i >.</p >';
        $html .= '<p class="step3">';
        $html .= 'Thank you for your interest in < i>TripPass &#8482; Roadside Assistance</i> by Urgent.ly. Unfortunately, this service is not yet';
        $html .= 'available in your region . Please check back later as we are continually expanding our coverage area.</p ></div>';
        $html .= '<div class="modal-footer">';
        $html .= '<div class="step-1-footer" >';
        $html .= ' <button class="btn learn-more green-btn" > Learn more </button >';
        $html .= ' <button class="btn cancel-step-1 cancel" > No thanks </button >';
        $html .= '</div >';
        $html .= '<div class="step-2-footer text-center" >';
        $html .= ' <div class="row" >';
        $html .= ' <div';
        $html .= ' class="col-md-2 col-sm-2 col-xs-2 col-sm-offset-2 col-md-offset-2 col-xs-offset-0 text-left" >';
        $html .= ' <i class="fa fa-calendar" aria-hidden = "true" ></i ></div >';
        $html .= ' <label for="datepicker" ></label >';
        $html .= ' <input type = "text" id = "datepicker" class="col-md-3 col-sm-3 col-xs-4"';
        $html .= ' value = "Start Trip" readonly = "true" >';
        $html .= ' <input type = "text" id = "datepicker2" class="col-md-3 col-sm-3 col-xs-4" value = "End Trip" readonly = "true" >';
        $html .= ' </div >';
        $html .= ' <div class="row" >';
        $html .= ' <div class="col-md-8 col-sm-8 col-sm-push-2 text-center col-md-push-2 col-xs-12 col-xs-push-0" >';
        $html .= ' <button class="btn payment-btn payment green-btn" > Proceed to payment</button >';
        $html .= '</div >';
        $html .= '</div >';
        $html .= '<button class="btn cancel-step-2 cancel" > Cancel</button >';
        $html .= '</div>';
        $html .= '<div class="step-3-footer">';
        $html .= '<button class="btn cancel-step-3 cancel" > Close</button>';
        $html .= '</div>';
        $html .= '<div class = "subtext" > (Your trip results will display after few seconds)</div>';
        $html .= '</div>';
        $html .= '</div >';
        $html .= '<!-- /.modal - content-->';
        $html .= '</div >';
        $html .= '<!-- /.modal - dialog-->';
        $html .= '</div >';
        $html .= '</div >';

        $content .= $html;


        return $content;
    }

} // end class

new AWDEmailCapture();
?>
