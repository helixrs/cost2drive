<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'c2d_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'oEy49DH?dznMx6y1vt(<8BtR2R*DPER~R.{H~2[-qq4seRQd/|lmx#w]]Kr_zE=$');
define('SECURE_AUTH_KEY',  '::b0CE;R(o+2nawWS[+bqQD+c<EuqJ6@%YMb#ceZ!k2[WlhfV5XN?>ggxqzkg80p');
define('LOGGED_IN_KEY',    'E6%vYgDGS%b>;fUmla@^7]]MbfC<UmP|=EU{%]k|U>dA<Yof%WP~S)m!cf|G|(2*');
define('NONCE_KEY',        'qcHDe.#NTGz_[&NNAO3NL4km2fj7e%Hu~{oy(E>]GVn5g=!=|cJu@!;9S6toa0F6');
define('AUTH_SALT',        'gD-]oY]6B`wu%I</UdDUhC<5A_7F+H@^4 ?.)+~F+J#4P{ur=Q5?b{W<u[1P_g-R');
define('SECURE_AUTH_SALT', 'Sw0I-9pVsYtk`Q-`0~S1h$NX?d~B2cyG6UPxG|+byo`{t5`r0e_&w) ZzIgV_eMt');
define('LOGGED_IN_SALT',   'pZwyjl{+-2m5 & +B?vLFioe|z^_p$<WCC;HEvw5GHgo-1sNodkNz( YCz-~5(VI');
define('NONCE_SALT',       '!U@(pT|X}wHWy}N)iRM-^u.@Z>JNbxZ4}@S<G-K{V2ElPqn+%R&-3= rm%S|@/-(');
define('DISALLOW_FILE_EDIT', true);

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/** Turn XML-RPC completely off*/
add_filter( 'xmlrpc_enabled', '__return_false' );
